<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
      <section class="ct" id="ct">
        <div class="ct--in">
          <h2 class="ct__ttl">Контакты</h2>
          <div class="ct__flex">
            <div class="ct__acrdw">
              <p class="ct__acrdw__ttl">Наши офисы</p>
              <div class="ct__acrd" id="ct__acrd">
                <h3 class="ct__acrd__ttl" data-map="1">Сочи, ул. Московская, д. 20</h3>
                <div class="ct__acrd__cnt">
                  <p class="ct__acrd__cnt__txt">Телефоны: 8(862) 271-11-11;<br> 8(988) 404-40-38</p>
                  <p class="ct__acrd__cnt__txt">Режим работы: 9.00 – 17.30</p>
                  <p class="ct__acrd__cnt__txt">Выходной: Воскресенье</p>
                </div>
                <h3 class="ct__acrd__ttl" data-map="2">Сочи, ул.Чехова, д. 21</h3>
                <div class="ct__acrd__cnt">
                  <p class="ct__acrd__cnt__txt">Телефоны: 8(862) 271-11-11;<br> 8(988) 404-40-38</p>
                  <p class="ct__acrd__cnt__txt">Режим работы: 9.00 – 17.30</p>
                  <p class="ct__acrd__cnt__txt">Выходной: Воскресенье</p>
                </div>
                <h3 class="ct__acrd__ttl" data-map="3">Дагомыс, ул. Гайдара, д. 2</h3>
                <div class="ct__acrd__cnt">
                  <p class="ct__acrd__cnt__txt">Телефоны: 8(862) 271-11-11;<br> 8(988) 404-40-38</p>
                  <p class="ct__acrd__cnt__txt">Режим работы: 9.00 – 17.30</p>
                  <p class="ct__acrd__cnt__txt">Выходной: Воскресенье</p>
                </div>
                <h3 class="ct__acrd__ttl" data-map="4">п. Лазаревское, ул. Павлова, д. 64 Б</h3>
                <div class="ct__acrd__cnt">
                  <p class="ct__acrd__cnt__txt">Телефоны: 8(862) 271-11-11;<br> 8(988) 404-40-38</p>
                  <p class="ct__acrd__cnt__txt">Режим работы: 9.00 – 17.30</p>
                  <p class="ct__acrd__cnt__txt">Выходной: Воскресенье</p>
                </div>
                <h3 class="ct__acrd__ttl" data-map="5">п. Хоста, ул. Шоссейная, д. 5 А</h3>
                <div class="ct__acrd__cnt">
                  <p class="ct__acrd__cnt__txt">Телефоны: 8(862) 271-11-11;<br> 8(988) 404-40-38</p>
                  <p class="ct__acrd__cnt__txt">Режим работы: 9.00 – 17.30</p>
                  <p class="ct__acrd__cnt__txt">Выходной: Воскресенье</p>
                </div>
              </div>
            </div>
            <div class="ct__map" id="ct__map"></div>
          </div>
        </div>
      </section>
      <section class="qq" id="qq">
        <div class="qq--in">
          <div class="qq__info">
            <h2 class="qq__ttl">Если есть вопросы? </h2>
            <p class="qq__sttl">Напишите нам</p>
          </div>
          <div class="qq__formw">
            <form class="qq__form form" id="form--3" action="javascript:void(0)" method="post" data-form="3">
              <p class="form__item form__item--name">
                <input class="form__input form__input--name form__input--valid" id="name--3" name="name" type="text" placeholder="Ваше имя">
              </p>
              <p class="form__item form__item--mail">
                <input class="form__input form__input--mail form__input--valid" id="mail--3" name="mail" type="text" placeholder="Ваш email">
              </p>
              <p class="form__item form__item--msg">
                <textarea class="form__input form__input--msg form__input--valid" id="msg--3" name="msg" placeholder="Ваше сообщение"></textarea>
              </p>
              <p class="form__item form__item--send">
                <input class="modal__formName" name="formName" type="hidden" value="Если есть вопросы? Напишите нам">
                <button class="form__send" type="submit">Отправить</button>
              </p>
            </form>
          </div>
        </div>
      </section>
<br />
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>