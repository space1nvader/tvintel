<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
<title>Реквизиты компании</title>
     <section class="req" id="req">
        <div class="req--in">
          <h2 class="req__ttl">Реквизиты</h2>
          <p class="req__sttl"> Общество с ограниченной ответственностью «РАДИСТ» <span class="nobr">(ООО «РАДИСТ»)</span></p>
          <div class="req__box">
            <ul class="req__its">
              <li class="req__it">
                <p class="req__it__ttl">ИНН:</p>
                <p class="req__it__val">2320023716 </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">КПП:</p>
                <p class="req__it__val">232001001 </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">ОКПО:</p>
                <p class="req__it__val">31362435 </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">ОКВЭД:</p>
                <p class="req__it__val">64.20.21 </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">Р/с:</p>
                <p class="req__it__val">40702810306300000704<br> в Филиале Банка ВТБ (ПАО)<br> в г. Ростове-на-Дону г. Ростов-на-Дону </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">К/с:</p>
                <p class="req__it__val">30101810300000000999 </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">БИК:</p>
                <p class="req__it__val">046015999 </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">Юридический адрес:</p>
                <p class="req__it__val">354000, г. Сочи, ул. Московская, д. 20 </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">Почтовый адрес:</p>
                <p class="req__it__val">354000, г. Сочи, Главпочтамт, а/я 284 </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">Телефон:</p>
                <p class="req__it__val">(8-862) 2710-100 2711-111 </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">Телефон / бухгалтерия:</p>
                <p class="req__it__val">(8-862) 2710-151 </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">ОГРН:</p>
                <p class="req__it__val">1022302941462 </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">Директор:</p>
                <p class="req__it__val">Сидорцов Роман Николаевич<br> действующий на основании Устава </p>
              </li>
              <li class="req__it">
                <p class="req__it__ttl">Главный бухгалтер:</p>
                <p class="req__it__val">Федоренко Наталья Юрьевна</p>
              </li>
            </ul>
          </div>
        </div>
      </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>