<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
    <section class="about" id="about">
        <div class="about--in">
            <h2 class="about__ttl">Инструкция по оплате банковскими картами (Visa, Master Card) и Яндекс.деньгами</h2>
            <div class="about__box">
                <div class="about__info">
                    <p class="about__txt">Для оплаты услуг пластиковой картой или Яндекс.деньгами требуется войти в личный кабинет по адресу <a href="https://client.tvintel.info">https://client.tvintel.info</a>.</p>
                    <p class="about__txt">Далее в левом меню выбираем пункт «Оплата услуг» -&gt; «Пополнить баланс».</p>
                    <br />
                    <p class="about__txt"><img src="/bitrix/templates/tvintel/img/image001.png" alt="image001" width="500"></p>
                    <p class="about__txt">На следующей странице выбираем платежную систему «Яндекс деньги».</p>
                    <p class="about__txt"><img src="/bitrix/templates/tvintel/img/image003.png" alt="image003" width="500"></p>
                    <p class="about__txt">Далее выбираем номер договора, по которому требуется произвести оплату.</p>
                    <p class="about__txt"><img src="/bitrix/templates/tvintel/img/image005.png" alt="image005" width="500"></p>
                    <p class="about__txt">В следующем окне выбираем способ платежа «Платеж с банковской карты» и указываем сумму к оплате. &nbsp;Для оплаты услуг при помощи Яндекс.денег и Webmoney выберите соответствующий пункт меню.</p>
                    <p class="about__txt"><img src="/bitrix/templates/tvintel/img/image007.png" alt="image007" width="500"></p>
                    <p class="about__txt">Затем вы будете перенаправлены на страницу Яндекс Кассы, где вам потребуется ввести реквизиты банковской карты и e-mail, на который придет оповещение о платеже.</p>
                    <p class="about__txt"><img src="/bitrix/templates/tvintel/img/image009.png" alt="image009" width="500"></p>
                    <p class="about__txt">&nbsp;</p>
                    <p class="about__txt"><strong>В зависимости от требований конкретного банка, выпустившего карту, может потребоваться дополнительное подтверждение платежа при помощи SMS, одноразового пароля или иного способа.<br></strong></p>

                </div>
            </div>
        </div>
    </section>

    <section class="about" id="sber_about">
        <div class="about--in">
            <h2 class="about__ttl">Инструкция по оплате через мобильное приложение Сбербанк-онлайн</h2>
            <div class="about__box">
                <div class="about__info">
                    <p class="about__txt">Авторизовываемя в мобильном риложении Сбербанк-Онлайн:<br><br>&nbsp;<img src="/bitrix/templates/tvintel/img/1.PNG" alt=""></p>
                    <p class="about__txt">Выбираем раздел "Интернет и ТВ":</p>
                    <p class="about__txt"><img src="/bitrix/templates/tvintel/img/2.PNG" alt=""><br><br>В строке поиска указываем "Твинтел":<br><br><img src="/bitrix/templates/tvintel/img/photo_2017-11-29_17-16-02.jpg" alt="photo_2017-11-29_17-16-02.jpg" width="416" height="736"></p>
                    <p class="about__txt">Выбираем счет для cписания и указываем номер договора:<br><br></p>
                    <p class="about__txt"><img src="/bitrix/templates/tvintel/img/photo_2017-11-29_18-07-25.png" alt="photo_2017-11-29_18-07-25.png" width="390" height="667"></p>
                    <p class="about__txt">&nbsp;</p>
                    <p class="about__txt">&nbsp;</p>
                    <p class="about__txt"><img src="/bitrix/templates/tvintel/img/photo_2017-11-29_18-07-29.png" alt="photo_2017-11-29_18-07-29.png" width="393" height="901"><br><br><br></p>
                    <p class="about__txt">Указываем номер договора, сумму платежа и нажимаем "Продолжить".<br><br>Обращаем внимание, что ввиду особенностей работы Сберанка платеж зачислаеся на счет в срок до трех рабочих дней.</p>
                    <p class="about__txt">&nbsp;</p>

                    <h3>Инструкция по оплате через Сбербанк Онлайн</h3>
                    <p class="about__txt">
                        Для оплаты услуг через Сбербанк-Онлайн открываем сайт Сбербанка (<a href="http://www.sberbank.ru/">http://www.sberbank.ru/</a>) и входим в систему Сбербанк Онлайн.<br><br><img src="/bitrix/templates/tvintel/img/pay/image001.jpg" alt="image001"><br><br>Вводим логин и пароль в системе Сбербанк-Онлайн.<br><br><img src="/bitrix/templates/tvintel/img/pay/image003.jpg" alt="image003"><br><br>Подтверждаем вход в систему при помощи SMS-пароля.<br><br><img src="/bitrix/templates/tvintel/img/pay/image005.jpg" alt="image005"><br><br>Далее переходим в раздел «Платежи и переводы», в строке поиска вводим «Радист» и нажимаем «Найти».<br><br><img src="/bitrix/templates/tvintel/img/pay/image7.jpg" alt="image7">После этого щелкаем по надписи «Радист».<br><br><img src="/bitrix/templates/tvintel/img/pay/image8.jpg" alt="image8"><br>Выбираем карту, с которой требуется произвести оплату, указываем номер договора и нажимаем «Продолжить».<br><br><img src="/bitrix/templates/tvintel/img/pay/image9.jpg" alt="image9"><br>На следующей странице указываем сумму к оплате и нажимаем «Продолжить».<br><br><img src="/bitrix/templates/tvintel/img/pay/image10.jpg" alt="image10"><span style="font-size: 11pt; line-height: 15.6933336257935px; font-family: Calibri, sans-serif;"><br>Проверяем правильность введенных данных (номер договора и сумма к оплате). Если все верно – вводим&nbsp;</span><span style="font-size: 11pt; line-height: 15.6933336257935px; font-family: Calibri, sans-serif;">SMS-</span><span style="font-size: 11pt; line-height: 15.6933336257935px; font-family: Calibri, sans-serif;">пароль и нажимаем «Подтвердить».</span><br><span style="font-size: 11pt; line-height: 107%; font-family: Calibri, sans-serif;"><br><img src="/bitrix/templates/tvintel/img/pay/image11.jpg" alt="image11"><br></span><span style="font-size: 11pt; line-height: 107%; font-family: Calibri, sans-serif;"><br><br></span><br><br>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="about" id="sber_about">
        <div class="about--in">
            <h2 class="about__ttl">Оплата через QIWI Терминалы</h2>
            <div class="about__box">
                <div class="about__info">
                    <p class="about__txt">Пополнить счет можно в любом QIWI Терминале по пути домой. QIWI Терминалы легко узнать по трем большим кнопкам на экране. Заплатить просто: достаточно нажать 3-5 кнопок и внести наличные. Деньги будут зачислены моментально. Карта расположения терминалов и контактная информация на <a href="http://www.qiwi.com">www.qiwi.com<br></a><br>Как пополнить счет в QIWI Терминале:<br><br>На главном экране выберите раздел «Оплата услуг»;<br><br><img src="/bitrix/templates/tvintel/img/qw1.jpg" alt="qw1"><br><br>Нажмите кнопку «Поиск по всем услугам»;<br><br><img src="/bitrix/templates/tvintel/img/qw2.jpg" alt="qw2"><br><br>Введите название компании или услуги в строку поиска и выберите нужный вариант;<br><br><img src="/bitrix/templates/tvintel/img/qw3.jpg" alt="qw3"><br><br>Укажите необходимые для платежа данные;Внесите наличные в купюроприемник;Сохраните квитанцию об оплате до зачисления платежа.</p>
                    <h2>Карта приема платежей QIWI Терминалов</h2>
                    <p class="about__txt">
                        <a href="https://qiwi.ru/replenish/map.action?lat=43.60174851451021&amp;lng=39.72512895&amp;zoom=13"><img src="/bitrix/templates/tvintel/img/mapq.jpg" alt="mapq" style="width: 100%;"></a>
                    </p>

                    <br />
                    <br />
                    
                    <h4 class="about__ttl">Адреса приема платежей QIWI Терминалов</h4>
                    <br /><br />
                    
                    <div>Адлерская ул, 4 <br>
                        Аллея Челтенхэма ул, д. 8 <br>
                        Аллея Челтенхэма ул, 4 <br>
                        Альпийская ул, 3А <br>
                        Апшеронская ул, 11 <br>
                        Армавирская ул, 150 <br>
                        Армавирская ул, 34 <br>
                        Армавирская ул, 56А <br>
                        Армавирская ул, 98 <br>
                        Армянская ул, 43 <br>
                        Армянская ул, 76/8 <br>
                        Батумское ш, 25 <br>
                        Батумское ш, 41 <br>
                        Батумское ш, 57а <br>
                        Бзугу ул, 6 <br>
                        Бытха ул, д.39 корп.В <br>
                        Бытха ул, 1 <br>
                        Бытха ул, 25 <br>
                        Бытха ул, 3/4 <br>
                        Бытха ул, 30 <br>
                        Бытха ул, 48А <br>
                        Бытха ул, 53 <br>
                        Бытха ул, 54 <br>
                        Виноградная ул, 122 <br>
                        Виноградная ул, 224/4 <br>
                        Виноградная ул, 272 <br>
                        Виноградная ул, 45 <br>
                        Виноградная ул, 46а <br>
                        Виноградная ул, 66 <br>
                        Виноградная ул, 85Б <br>
                        Виноградная ул, 93 <br>
                        Виноградный пер, 22 <br>
                        Волгоградская ул, 8/1 <br>
                        Воровского ул, 19 <br>
                        Воровского ул, 36 <br>
                        Воровского ул, 4 <br>
                        Ворошиловская ул, 20 <br>
                        Восточная ул, 8в <br>
                        Высокогорная ул, 54 <br>
                        Гагарина ул, д.59, стр.1 <br>
                        Гагарина ул, 15 <br>
                        Гагарина ул, 32 <br>
                        Гагарина ул, 48/1 <br>
                        Гагарина ул, 53А <br>
                        Гагарина ул, 63 <br>
                        Гагарина ул, 7б <br>
                        Гайдара ул, 11 <br>
                        Главная ул, 23/5 <br>
                        Горького пер, 14/1 <br>
                        Горького ул, 1 <br>
                        Горького ул, 22 <br>
                        Горького ул, 30/2 <br>
                        Горького ул, 53 <br>
                        Горького ул, 56 <br>
                        Госпитальная ул, 3 <br>
                        Дагомысская ул, 42 <br>
                        Дагомысская ул, 7/2 <br>
                        Дарвина ул, д.57а <br>
                        Дарвина ул, 101/1 <br>
                        Дарвина ул, 4 <br>
                        Дарвина ул, 82, кор.1 <br>
                        Дарвина ул, 93 <br>
                        Декабристов ул, 78Б <br>
                        Дивноморская ул, 17 <br>
                        Дмитриевой ул, 2а <br>
                        Донская ул, 100 <br>
                        Донская ул, 15, А <br>
                        Донская ул, 20 <br>
                        Донская ул, 28 <br>
                        Донская ул, 3/9 <br>
                        Донская ул, 36 <br>
                        Донская ул, 5, А <br>
                        Донская ул, 90 <br>
                        Донская ул, 94я <br>
                        Донской пер, 23 <br>
                        Единство ул, 21 <br>
                        Железнодорожная ул, 3 <br>
                        Загородная ул, 1/16 <br>
                        Калараш ул, 11 <br>
                        Калараш ул, 111 <br>
                        Калараш ул, 52 <br>
                        Калараш ул, 66 <br>
                        Калараш ул, 99 <br>
                        Калиновая ул, 33 <br>
                        Камо ул, д.3,корп.А <br>
                        Кольцевая ул, 16 <br>
                        Коммунаров ул, 1/4 <br>
                        Комсомольская ул, 1 <br>
                        Конституции СССР ул, д.18 <br>
                        Красноармейская ул, д.2, корп.1 <br>
                        Красноармейская ул, 19 <br>
                        Красноармейская ул, 41 <br>
                        Краснодонская ул, 18 <br>
                        Крымская ул, 25а <br>
                        Кубанская ул, 11 <br>
                        Курортный пр-кт, д.72, корп.7 <br>
                        Курортный пр-кт, 32 <br>
                        Курортный пр-кт, 40 <br>
                        Курортный пр-кт, 5 <br>
                        Курортный пр-кт, 58, кор.16 <br>
                        Курортный пр-кт, 73 <br>
                        Курортный пр-кт, 75 <br>
                        Курортный пр-кт, 92 <br>
                        Курортный пр-кт, 92/5 <br>
                        Лазарева ул, 11К <br>
                        Лазарева ул, 88А <br>
                        Ленина ул, 233/1 <br>
                        Ленина ул, 300 <br>
                        Лермонтова ул, 12 К 4 <br>
                        Лысая гора ул, 26 <br>
                        Лысая гора ул, 37 <br>
                        Львовская ул, 26а <br>
                        Львовская ул, 8/5 (СчА) <br>
                        Мацестинская ул, 7 <br>
                        Молодежная ул, 23 <br>
                        Навагинская ул, 14 <br>
                        Навагинская ул, 3/2 <br>
                        Невская ул, 14А <br>
                        Невская ул, 18 <br>
                        Невская ул, 46 <br>
                        Невская ул, 50A <br>
                        Несебрская ул, 6 <br>
                        Новая Заря ул, 3 <br>
                        Новороссийское шоссе ул, 12/11 <br>
                        Новороссийское шоссе ул, 17/1 <br>
                        Новороссийское шоссе ул, 2 <br>
                        Новороссийское шоссе ул, 2/6 <br>
                        Орджоникидзе ул, 24 <br>
                        Павлова ул, 1 <br>
                        Павлова ул, 2А <br>
                        Павлова ул, 25 <br>
                        Параллельная ул, 18 <br>
                        Парковая ул, 19 <br>
                        Партизанская ул, 15 <br>
                        Партизанская ул, 64 <br>
                        Пасечная ул, 20 <br>
                        Пионерская ул, 3/10 <br>
                        Пионерская ул, 91 <br>
                        Пирогова ул, д.10 корп. 1/7 <br>
                        Пирогова ул, 4 <br>
                        Пирогова ул, 4 корп А <br>
                        Пирогова ул, 40/1 <br>
                        Победы ул, 1 <br>
                        Победы ул, 101 <br>
                        Победы ул, 110А <br>
                        Победы ул, 153 <br>
                        Победы ул, 165 <br>
                        Победы ул, 170 <br>
                        Победы ул, 2 <br>
                        Победы ул, 264 <br>
                        Победы ул, 267 <br>
                        Победы ул, 31 <br>
                        Победы ул, 370А <br>
                        Победы ул, 67 <br>
                        Победы ул, 77 <br>
                        Победы ул, 85 <br>
                        Победы ул, 89 <br>
                        Подгорная ул, 33 <br>
                        Политехническая ул, 39 <br>
                        Поярко ул, 2 <br>
                        Приморская ул, 3А <br>
                        Приморская ул, 3/10 <br>
                        Промышленный пер, 4а <br>
                        Просвещения ул, 169а <br>
                        Речной пер, 16 <br>
                        Родниковая ул, 23 <br>
                        Роз ул, 14 <br>
                        Роз ул, 6 <br>
                        Роз ул, 67 <br>
                        Роз ул, 95 <br>
                        Ростовская ул, 6 <br>
                        Ростовская ул, 7 <br>
                        Северная ул, 12 <br>
                        Северная ул, 6 <br>
                        Советская ул, 40 <br>
                        Соколова ул, 1 <br>
                        Строительный пер, 2Д <br>
                        Строительный пер, 9 <br>
                        Сухумское шоссе ул, 19 <br>
                        Сухумское шоссе ул, 50/2 <br>
                        Теневой пер, 18 <br>
                        Тимирязева ул, 16 <br>
                        Тимирязева ул, 7 <br>
                        Титова ул, 4 <br>
                        Тоннельная ул, 2 <br>
                        Туапсинская ул, 11 <br>
                        Туапсинская ул, 13 <br>
                        Туапсинская ул, 5/33 <br>
                        Туапсинская ул, 9 <br>
                        Учительская ул, 19 <br>
                        Цветной б-р, 40 <br>
                        Целинная ул, 5 <br>
                        Центральная ул, 13 <br>
                        Чайковского ул, 19 <br>
                        Чайковского ул, 27/2 <br>
                        Чайковского ул, 7 <br>
                        Чайная ул, 24/1 <br>
                        Чебрикова ул, 36 <br>
                        Чебрикова ул, 5 <br>
                        Чебрикова ул, 7 <br>
                        Черноморская (Хостин) ул, 12 <br>
                        Чехова пер, 8 <br>
                        Чехова ул, 23 <br>
                        Чехова ул, 54 <br>
                        Шоссейная ул, 2б <br>
                        Юных Ленинцев ул, 10/2 МГ <br>
                        Юных Ленинцев ул, 5/1 <br>
                        Ялтинская ул, 11 <br>
                        Ялтинская ул, 14 <br>
                        Ясногорская ул, 4/2 <br>
                        Ясногорская ул, 4/2 <br>
                        Ясногорская ул, 6 <br>
                        Я.Фабрициуса ул, 3/7 <br>
                        20 Горно-Стрелковой Дивизии ул, 16 <br>
                        20 Горно-Стрелковой Дивизии ул, 24 <br>
                        50 лет СССР ул, 8/2</div>
                </div>
            </div>
        </div>
    </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>