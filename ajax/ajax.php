<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("user");
global $USER;

$url = 'https://helpdesk.tvintel.info/nstaff/ext/auth/';

$data = array (
    'login' => $_REQUEST['login'],
    'pass' => $_REQUEST['pass'],
);

$params = '';
foreach($data as $key=>$value)
    $params .= $key.'='.$value.'&';

$params = trim($params, '&');

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url); //Remote Location URL
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 7); //Timeout after 7 seconds
curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
curl_setopt($ch, CURLOPT_HEADER, 0);

//We add these 2 lines to create POST request
curl_setopt($ch, CURLOPT_POST, count($data)); //number of parameters sent
curl_setopt($ch, CURLOPT_POSTFIELDS, $params); //parameters data

$result = curl_exec($ch);
//print_r($result) ;
curl_close($ch);

$tucode = json_decode($result);
if($tucode->uid){
	$arAuthResult = $USER->Login($_REQUEST['login'], $_REQUEST['pass'], "Y");
	if(isset($arAuthResult['TYPE']) && $arAuthResult['TYPE'] == 'ERROR')
	{
		$arResult = $USER->Register($_REQUEST['login'], $tucode->name, "",$_REQUEST['pass'],$_REQUEST['pass'],$_REQUEST['login'].'e@email.ru');
		//ShowMessage($arResult);
		$fields = Array(
			"UF_UID" => $tucode->uid,
		);
		$USER->Update($USER->GetID(), $fields);
		$USER->Authorize($USER->GetID()); // авторизуем
		echo 1;
	}
	else
	{
		// массив для переменных, которые будут переданы с запросом
		$data = array(
			'login' => $_REQUEST['login'],
			'pass' => $_REQUEST['pass']
		);
		echo 1;
	}
}else {
	$arAuthResult = $USER->Login($_REQUEST['login'], $_REQUEST['pass'], "Y");
	if(isset($arAuthResult['TYPE']) && $arAuthResult['TYPE'] == 'ERROR')
	{
		
		echo '<p class="err">Пользователь не найден</p>';
	}
	else
	{
		echo 1;
	}
}