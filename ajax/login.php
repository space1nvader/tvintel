<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/helpers.php");
CModule::IncludeModule("user");
global $USER;

$url = 'https://helpdesk.tvintel.info/nstaff/ext/auth/';

$data = array (
    'login' => $_REQUEST['login'],
    'pass' => $_REQUEST['pass'],
);

$tucode = curlRequest($url, $data);

if($tucode->uid){
    $arAuthResult = $USER->Login($_REQUEST['login'], $_REQUEST['pass'], "Y");
    if(isset($arAuthResult['TYPE']) && $arAuthResult['TYPE'] == 'ERROR')
    {
        $arResult = $USER->Register($_REQUEST['login'], $tucode->name, "",$_REQUEST['pass'],$_REQUEST['pass'],$_REQUEST['login'].'e@email.ru');
        //ShowMessage($arResult);
        $fields = Array(
            "UF_UID" => $tucode->uid,
        );
        $USER->Update($USER->GetID(), $fields);
        $USER->Authorize($USER->GetID()); // авторизуем
        echo 1;
    }
    else
    {
        // массив для переменных, которые будут переданы с запросом
        $data = array(
            'login' => $_REQUEST['login'],
            'pass' => $_REQUEST['pass']
        );
        echo 1;
    }
}else {
    $arAuthResult = $USER->Login($_REQUEST['login'], $_REQUEST['pass'], "Y");
    if(isset($arAuthResult['TYPE']) && $arAuthResult['TYPE'] == 'ERROR')
    {
        
        echo '<p class="err">Пользователь не найден</p>';
    }
    else
    {
        echo 1;
    }
}