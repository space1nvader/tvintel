<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/helpers.php");
CModule::IncludeModule("user");
global $USER;

$url = 'https://helpdesk.tvintel.info/nstaff/ext/get_address_name';

$data = array (
    'street' => $_REQUEST['street'],
    'building' => $_REQUEST['building'],
);

$result = curlRequest($url, $data);

if($result['result'] == 'Ok'){
    
    if($result['message'] == 'Found') {
        printJsonAndReturn([
            'success' => true,
            'found' => true,
            $result['data']
        ]);
    }
    
    printJsonAndReturn([
        'success' => true,
        'found' => false,
    ]);
    
}

printJsonAndReturn([
    'success' => false,
    'message' => 'Не удалось подключиться, попробуйте позже'
]);