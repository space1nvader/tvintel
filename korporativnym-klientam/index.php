<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");
?><section class="tvpromo" id="tvpromo">
<div class="tvpromo--in">
	<div class="tvpromo__box">
		<div class="tvpromo__info" style="padding: 70px 0 70px 80px">
			<h2 class="tvpromo__ttl">Подход для каждого<br>
 <span class="nobr strong">индивидульный</span></h2>
			<p class="tvpromo__txt">
				 Безлимитный доступ по выделенным каналам связи, индивидуальный подход, гибкие ценовые и технические решения. Разертывание беспроводных WiFi систем позволит организовать рабочие места в кротчайшие сроки. Новейшие технологии и масштаб сети Твинтел-Юг объединят ваши офисы, а техническая поддержка 24/7 поможет с настройкой оборудования любой сложности.
			</p>
			<p class="tv__it__btw">
 <span class="prc__it__bt openModal" data-modal="bid" ttl="" speed="" price="" tid="61">
				Отправить заявку </span>
			</p>
		</div>
		<p class="tvpromo__pic">
 <img src="/bitrix/templates/tvintel_new/img/corppromo-pic.png" class="tvpromo__pic__img">
		</p>
	</div>
</div>
 </section><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>