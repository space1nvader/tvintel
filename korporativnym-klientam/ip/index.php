<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");
?><section class="tvpromo" id="tvpromo">
<div class="tvpromo--in">
	<div class="tvpromo__box">
		<div class="tvpromo__info" style="padding: 70px 0 178px 80px">
			<h2 class="tvpromo__ttl">Качественная<br>
 <span class="nobr strong">телефония</span></h2>
			<p class="tvpromo__txt">
				 Современная связь с передачей информации через Интернет без использования аналоговых сетей, позволяющая осуществлять дешевые телефонные звонки по всему миру. Виртуальная АТС, быстрые конференции, интеллектуальная переадресация, интеграция с мобильным телефоном и единый номерной план, упростят задачи управления бизнесом.

			</p>
		</div>
		<p class="tvpromo__pic">
 <img src="/bitrix/templates/tvintel_new/img/promotelcorp.png" class="tvpromo__pic__img" style="width: 91%;">
		</p>
	</div>
</div>
 </section> <section class="tv" id="tv">
<div class="inet--in">
	<h2 class="tv__ttl">Тарифы <span class="nobr">Телефония</span></h2>
	 <?$arrFilter = Array(
            "SECTION_ID" => '7'
        );?> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"tariff_detail",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "arrFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "tarif",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "9",
		"NOT_SHOW_ADDRESSCHECK_BLOCK" => true,
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "round",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"PRICE",1=>"PERIOD",2=>"SALE",3=>"DAY",4=>"NIGHT",5=>"ANAL",6=>"CIFRA",7=>"HD_CANAL",8=>"TELEPHONE",9=>"",),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
</div>
 </section><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>