<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/helpers.php");

$url = 'https://helpdesk.tvintel.info/nstaff/ext/upd_ticket_podkl';

$data = array(
    'tariff_id' => $_REQUEST['id'],
    'name' => $_REQUEST['name'],
    'phone' => $_REQUEST['tel'],
    'street' => $_REQUEST['addr1'],
    'house_number' => $_REQUEST['addr2'],
    'kv' => $_REQUEST['addr3'],
);

$result = curlRequest($url, $data);

if ($result['result'] == 'success') {
    
    echo "
			<p class='modal__thx__ttl'>Спасибо! Ваша <span class='nobr'>заявка принята.</span></p>
			<p class='modal__thx__txt'>Наш менеджер перезвонит <span class='nobr'>в течение <span class='strong'>8 минут</span></span></p>";
    exit;
}

echo "
			<p class='modal__thx__ttl'>Возникла ошибка!</p>
			<p class='modal__thx__txt'>Попробуйте <span class='nobr'>перезагрузить страницу</span></p>";
exit;
?>