// @___codekit-prepend 'base/jquery-1.8.3.js';
// @codekit-prepend 'base/jquery-1.12.4.min.js';
// @___codekit-prepend 'base/jquery.validate.min.js';
// @codekit-prepend 'base/scrollTo.js';
// @___codekit-prepend 'base/form.validation.js';
// @___codekit-prepend 'base/forms.js';
// @codekit-prepend 'base/maskedinput.js';
// @codekit-prepend 'base/utm.js';
// @codekit-prepend 'base/swiper.js';
// @codekit-prepend 'base/lightbox.js';

// @codekit-prepend 'ext/countUp.js';
// @___codekit-prepend 'ext/countdown.js';

// @codekit-prepend 'ext/jquery-ui.min.js';
// @___codekit-prepend 'ext/jquery.ui.touch-punch.js';
$(document).ready(function(){
  // ajax tests
  // v3
  // $('.form__input--file').change(function(){
  //   var files = this.files
  //   console.log(files)
  //   console.log(files.length)
  //   if(files.length < $(this).data('length')){
  //     $(this).val('')
  //     // $('.bid__sttl').html('dsf')
  //     $('.bid__sttl').html($(this).val())
  //   }
  // })
  // $('#file--2').change(function(){
  //     var files = this.files; //это массив файлов
  //     var form = new FormData();
  //     for(var i=0;i<files.length;i++){
  //       // form.append("file_"+i,files[i]);
  //       form.append("file["+i+"]", files[i]);
  //     }
  //     // $('.bid__form__test1').html(files[0])
  //     // $('.bid__form__test2').html(form)
  //     // console.log(files)
  //     // console.log(form)
  // })
  var files
  // console.log(files)
  // if(typeof files !== "undefined" && files.length){
  //   console.log(files)
  //   // for(var i = 0; i < files.length; i++){
  //   //   data.append("file[" + i + "]", files[i]);
  //   // }
  // }
  $('#file--2').change(function(){
    var $this = $(this)
    // var $files = $(this).get(0).files
    files = $(this).get(0).files
    var len = files.length
    var $form = $(this).parents('.form')
    $('.form__lbl__eq').html(len)
    if(len === 0){
      $form.removeClass('withFiles')
    }else{
      if(len > 8){
        $form.removeClass('withFiles')
        // $('.bid__ttl').html(len)
        $this.val('')
        files = undefined
        $('.form__files').empty()
        $this.after('<span class="error">Максимум 8 файлов</span>');
        // $this.nextAll('.error').delay(1500).fadeOut('slow');
        setTimeout(function(){
          $this.removeClass('invalid').nextAll('.error').remove()
        }, 2100);
      }else{
        $form.addClass('withFiles')
        $('.form__files').empty()
        for(var i = 0; i < files.length; i++){
          // data.append("file[" + i + "]", files[i]);
          // files[i].name
          $('.form__files').append('<li class="form__file"><p class="form__file__remove"></p><p class="form__file__name">' + files[i].name + '</p></li>')
        }
      }
    }
    console.log(files)
  })
  // $('.form__file__remove').bind('click', function(){
  //   // delete $files[0]
  //   // $('.bid__ttl').html(index)
  //   console.log($files)
  // })
  $(document).on('click', '.form__file__remove', function(){
    var index = $(this).parent('.form__file').index()
    var filesTemp = Array.from(files)
    $(this).parent('.form__file').remove()
    // Array.prototype.push.apply($files2, $files)
    filesTemp.splice(index, 1)
    files = filesTemp
    // // $files[index].splice(index, 1)
    // // delete $files['0']
    // // $files['0'] = undefined
    // // $files[0] = undefined
    // // delete $files[index]
    // console.log(delete $files['0'])
    // console.log(delete $files2['color'])
    // // for (var prop in $files) {
    // //   console.log("obj." + prop + " = " + $files[prop])
    // // }
    // console.log(files)
    // console.log(filesTemp)
  })
  // $('.bid__ttl').on('click', function(){
  //   if($(this).hasClass('noTerm')){
  //     $('.bid__ttl').html('click')
  //   }
  // })
  // $('#nav').remove()
  // $('#head').remove()
  $('.form').on('submit', function(e){
    e.preventDefault()
    var form = $(this)
    var formIndex = $(this).attr('data-form')
    var data = new FormData();  // Для отправки файлов понадобится объект FormData. Подробнее про него можно прочитать в документации - https://developer.mozilla.org/en-US/docs/Web/API/FormData

    // проверка согласия с условиями
    // $('.bid__ttl').html('asd')
    if(form.hasClass('noTerm')){
      form.addClass('noTermOnClick')
      setTimeout(function(){
        form.removeClass('noTermOnClick')
      }, 1500)
      form.find('.form__input--valid').each(function(){
        if($(this).val() == ''){
          if(!$(this).addClass('invalid').nextAll('.error').length){
            var $this = $(this)
            $this.after('<span class="error">Обязательно для заполнения</span>');
            setTimeout(function(){
              $this.removeClass('invalid').nextAll('.error').remove()
            }, 2100);
          }
          return false;
        }else{
          $(this).removeClass('invalid').nextAll('.error').remove();
        }
      })
    }else{
      form.find('.form__input--valid').each(function(){
        if($(this).val() == ''){
          // if($(this).addClass('invalid').nextAll('.error').notExists()){
          if(!$(this).addClass('invalid').nextAll('.error').length){
            var $this = $(this)
            // $(this).after('<div class="error">Введите телефон</div>');
            // $(this).after('<span class="error">Обязательно для заполнения</span>');
            // $(this).nextAll('.error').delay(1500).fadeOut('slow');
            $this.after('<span class="error">Обязательно для заполнения</span>');
            // $this.nextAll('.error').delay(1500).fadeOut('slow');
            setTimeout(function(){
              $this.removeClass('invalid').nextAll('.error').remove()
            }, 2100);
            // setTimeout(function(){
            //   // form.find('.form__input--tel').next('.error').remove();
            //   $(this).removeClass('invalid').next('.error').remove();
            // }, 2100);
          }
          return false;
        }else{
          $(this).removeClass('invalid').nextAll('.error').remove();
        }
      })
      if(!$(this).find('.invalid').length){
        // Сбор данных из обычных полей
        form.find(':input[name]').not('[type="file"]').each(function(){
            var field = $(this);
            data.append(field.attr('name'), field.val());
        });

        // Сбор данных о файле (будет немного отличаться для нескольких файлов)
        var filesField = form.find('input[type="file"]');
        // if(filesField.length){
        //   var fileName = filesField.attr('name');
        //   // var file = filesField.prop('files')[0];
        //   // data.append(fileName, file);
        //   // var files = filesField.files
        //   var files = filesField.prop('files')
        //   for(var i = 0; i < files.length; i++){
        //     data.append("file[" + i + "]", files[i]);
        //   }
        // }
        // if(files.length){
        // if(typeof files !== "undefined" && files.length){
        if(files && files.length){
          for(var i = 0; i < files.length; i++){
            data.append("file[" + i + "]", files[i]);
          }
        }

        // Отправка данных
        var url = './php/send.php';

        $.ajax({
          url: url,
          type: 'POST',
          data: data,
          contentType: false,
          cache: false, 
          processData:false, 
          success: function(response) {
            // $('.modal--thx .modal__thx__res').html(response);
            $('.modal__thx__res').html(response);
            setTimeout(function(){
              // $('body').addClass('bodyModal blur');
              // $('html').css({'overflow':'hidden','padding-right':swidth});
              // $('#modal--success').addClass('active');
              $('.modal').removeClass('show')
              $('.modal--thx').addClass('show')
            },300);
            setTimeout(function(){
              // $.closeModal();
              $('.modal--thx').removeClass('show')
              $('.form__input').val('');
            },3700);
            // $('#form--' + formIndex).find('.form__send').removeClass('sending').prop('disabled', false)
            // window.location.href = 'img/SuperDecorColors.pdf';
            // $('.modal--call__download').trigger('click')
          },error:function(xhr,str){alert('Возникла ошибка!')}
        })
      }
    }
  })
  // v2
  // $('#form--2').on('submit', function(e){
  //   e.preventDefault()
  //   var form = $(this);
  //   var data = new FormData();  // Для отправки файлов понадобится объект FormData. Подробнее про него можно прочитать в документации - https://developer.mozilla.org/en-US/docs/Web/API/FormData

  //   // Сбор данных из обычных полей
  //   form.find(':input[name]').not('[type="file"]').each(function(){
  //       var field = $(this);
  //       data.append(field.attr('name'), field.val());
  //   });

  //   // Сбор данных о файле (будет немного отличаться для нескольких файлов)
  //   var filesField = form.find('input[type="file"]');
  //   var fileName = filesField.attr('name');
  //   var file = filesField.prop('files')[0];
  //   // var file = filesField.prop('files');
  //   data.append(fileName, file) ;

  //   // Отправка данных
  //   var url = './php/send.php';

  //   $.ajax({
  //     url: url,
  //     type: 'POST',
  //     data: data,
  //     contentType: false,
  //     cache: false, 
  //     processData:false, 
  //     success: function(response) {
  //         console.log(response)
  //     }
  //   })
  // }) 
  // v1
  // $('#form--2').submit(function(){
  //   $.ajax({
  //     type: 'POST',
  //     url: './php/send.php',
  //     data: $(this).serialize()
  //   }).done(function(){
  //     $(this).find('input').val('');
  //     alert('Спасибо за заявку! Скоро мы с вами свяжемся.');
  //     $('#form--2').trigger('reset');
  //     // $(this).trigger('reset');
  //   });
  //   return false;
  // });















  // base
  var winW = $(window).width(),
      winH = $(window).height()

  // для macos добавляем класс 'macos' в html
  if (navigator.appVersion.indexOf("Mac")!=-1) $('html').addClass('macos');
  if (navigator.userAgent.match(/Android|webOS|iPhone|iPod|Blackberry/i)) $('html').addClass('macos');

  // маска ввода телефона
  $('.form__input--tel').mask('+7(999) 999-99-99')

  // on resize
  // видеть размер экрана при дизайне
  $(function(){
    var dev = 0
    if(dev){
      var devWinSizeIc = '<span class="ic ic0"></span>'
      $('body').append('<p class="devWinSize">' + devWinSizeIc + winW + 'px • ' + winH + 'px</p>')
      $(window).on('resize', function(){
        winW = $(window).width()
        winH = $(window).height()
        // header & home
        // $('.home').css('padding-top', $('.header').outerHeight() + 'px')
        devWinSizeIc = '<span class="ic ic0"></span>'
        if(winW >= 480){
          devWinSizeIc = '<span class="ic ic1"></span>'
          if(winW >= 768){
            devWinSizeIc = '<span class="ic ic2"></span>'
            if(winW >= 1024){
              devWinSizeIc = '<span class="ic ic3"></span>'
              if(winW >= 1240){
                devWinSizeIc = '<span class="ic ic4"></span>'
                if(winW >= 1440){
                  devWinSizeIc = '<span class="ic ic5"></span>'
                }
              }
            }
          }
        }
        $('body>.devWinSize').html(devWinSizeIc + winW + 'px • ' + winH + 'px')
      })
      $(window).resize()
    }
  })

  // scrollTo
  $('.scrollTo').on('click',function(){
    var id = '#home'
    var offsetTop = 0
    var time = 1000
    if($(this).attr('href'))
      id = $(this).attr('href')
    if($(this).data('offset'))
      offsetTop = $(this).data('offset')
    if($(this).data('time'))
      time = $(this).data('time')
    $('html, body').animate({scrollTop:$(id).offset().top + offsetTop}, time)
    event.preventDefault()
  })

  // form term
  if($('.formw')){
    $(function(){
      $('.form__term').each(function(e){
        var $form = $(this).parent('.form')
        $(this).on('click',function(e){
          if(!$(e.target).hasClass('form__term__link'))
            if($form.hasClass('noTerm')){
              $form.removeClass('noTerm')
              $form.children('.form__send').prop('disabled', false)
            }else{
              $form.addClass('noTerm')
              $form.children('.form__send').prop('disabled', true)
            }
        })
      })
      // // $('.form.noTerm .form__send').on('click', function(e){
      // $('.form.noTerm').on('click', function(e){
      //   // $(this).parent('.form').addClass('noTermOnClick')
      //   $(this).addClass('noTermOnClick')
      //   setTimeout(function(){
      //     // $(this).parent('.form').removeClass('noTermOnClick')
      //     $(this).removeClass('noTermOnClick')
      //   }, 1000)
      //   // $(this).parent('.form').children('.form__term__ic').add
      // })
    })
  }

  // nav
  if($('#nav')){
    $(function(){
      $('.head__openNav').on('click',function(){
        if($('body').hasClass('withNav')){
          $('body').removeClass('withNav')
        }else{
          $('body').addClass('withNav')
        }
      })
      $('.head__kind').on('click',function(){
        if($('body').hasClass('withNav')){
          $('body').removeClass('withNav')
        }else{
          $('body').addClass('withNav')
        }
      })
      $('.nav__closeNav').on('click',function(){
        $('body').removeClass('withNav')
      })
      $('.nav__item__link').on('click',function(){
        $('body').removeClass('withNav')
      })
      $(window).on('resize', function(){
        winW = $(window).width()
        if(winW >= 1024){
          $('body').removeClass('withNav')
        }
      })
    })
  }

  // lazy load img
  lazyImg = setTimeout(function(){
    if($('img.lazy').length){
      $('img.lazy').each(function(){
        $(this).attr('src', $(this).attr('data-src')).removeClass('lazy')
      })
    }
  }, 500)

  // before scroll (if js)
  // $('.tech__item').addClass('anim')
  // $('.dvc__pics').addClass('anim')
  // on scroll
  $(window).on('scroll', function(){
    var winS = $(window).scrollTop()
    if(winS > 0){
      $('body').addClass('scroll')
    }else{
      $('body').removeClass('scroll')
    }
    // if($('.lib__pic').length){
    //   $(function(){
    //     var b = $('.lib__pic__img')
    //     var bTop = b.offset().top
    //     var k = winH * .9 / 45
    //     var angle = (winH * .9) * (1 - (winS + winH - bTop) / (winH * .9)) / k
    //     if(winS + winH > bTop){
    //       if(winS + winH * .1 < bTop){
    //         // $('body>.devWinSize').html(angle)
    //         b.css({'transform': 'rotateX(-' + angle + 'deg)'})
    //       }else{
    //         // $('body>.devWinSize').html((winH / 2) * (1 - (winH / 2) / (winH / 2)) / k)
    //         // $('body>.devWinSize').html(angle)
    //         b.css({'transform': 'rotateX(0deg)'})
    //       }
    //     }else{
    //       // $('body>.devWinSize').html((winH / 2) * (1 - 1 / (winH / 2)) / k)
    //       // $('body>.devWinSize').html(angle)
    //       b.css({'transform': 'rotateX(-45deg)'})
    //     }
    //   })
    // }
    // if($('.dvc__pics').length){
    //   $(function(){
    //     var b = $('.dvc__pics')
    //     var bT = b.offset().top
    //     var bH = b.height()
    //     if(winS + winH > bT + bH){
    //       b.removeClass('anim')
    //     }
    //   })
    // }
    // if($('img.lazy').length){
    //   $('img.lazy').each(function(){
    //     if($(this).offset().top < winH + winS + 300){
    //       $(this).attr('src', $(this).attr('data-src')).removeClass('lazy')
    //     }
    //     // if($(this).attr('src') == ''){
    //     //   if($(this).offset().top < winH + winS + 300){
    //     //     $(this).attr('src', $(this).attr('data-src'))
    //     //   }
    //     // }
    //   })
    // }
    // if($('.lazybg').length){
    //   $('.lazybg').each(function(){
    //     if($(this).offset().top < winH + winS + 300){
    //       $(this).removeClass('lazybg')
    //     }
    //   })
    // }
    // if($('.works').length){
    //   var box = $('.works')
    //   if(box.offset().top < winH + winS - 300){
    //     $('.works img').each(function(){
    //       if($(this).attr('src') == ''){
    //         $(this).attr('src', $(this).attr('data-src'))
    //       }
    //     })
    //   }
    // }
    // if($('.stf').length){
    //   var box = $('.stf')
    //   if(box.offset().top < winH + winS - 300){
    //     $('.stf img').each(function(){
    //       if($(this).attr('src') == ''){
    //         $(this).attr('src', $(this).attr('data-src'))
    //       }
    //     })
    //   }
    // }
    // if($('.we__items').length){
    //   var $this = $('.we__items')
    //   var offset = -100
    //   if($this.offset().top < winH + winS + offset){
    //     if(!$('.we__items').hasClass('done')){
    //       var weItemVal1 = parseInt($('#we__fact__val--1').text(), 10)
    //       var weItemVal2 = parseInt($('#we__fact__val--2').text(), 10)
    //       var weItemVal3 = parseInt($('#we__fact__val--3').text(), 10)
    //       var weItemVal4 = parseFloat($('#we__fact__val--4').text())
    //       var weItem1 = new CountUp('we__fact__val--1', 0, weItemVal1, 0, 3, {useEasing: false, separator: ''})
    //       var weItem2 = new CountUp('we__fact__val--2', 0, weItemVal2, 0, 3, {useEasing: false, separator: ''})
    //       var weItem3 = new CountUp('we__fact__val--3', 0, weItemVal3, 0, 3, {useEasing: false, separator: ''})
    //       var weItem4 = new CountUp('we__fact__val--4', 0, weItemVal4, 1, 3, {useEasing: false, separator: ''})
    //       weItem1.start()
    //       weItem2.start()
    //       weItem3.start()
    //       weItem4.start()
    //       $('.we__items').addClass('done')
    //     }
    //   }
    // }
  })



  // blocks
  // // nav
  // if($('#nav').length){
  //   $(function(){
  //     $('.nav__item').on('mouseenter', function(){
  //       var item = $(this).index()
  //       // $('.nav__item').removeClass('active')
  //       $(this).addClass('active').siblings().removeClass('active')
  //       $('.nav__pic').eq(item).addClass('active').siblings().removeClass('active')
  //     })
  //     // $('.we__item__val').html(0)
  //     // $(window).on('scroll', function(){
  //     //   var winS = $(window).scrollTop()
  //     //   var offset = -100
  //     //   $('.we__item__val').each(function(){
  //     //     if($(this).offset().top < winH + winS + offset){
  //     //       if(!$(this).hasClass('done')){
  //     //         var val = parseInt($(this).data('val'), 10)
  //     //         var id = $(this).attr('id')
  //     //         var item = new CountUp(id, 0, val, 0, 2, {useEasing: false, separator: ' '})
  //     //         item.start()
  //     //         $(this).addClass('done')
  //     //       }
  //     //     }
  //     //   })
  //     // })
  //   })
  // }
  // // we
  // if($('#we').length){
  //   $(function(){
  //     $('.we__item__val').html(0)
  //     $(window).on('scroll', function(){
  //       var winS = $(window).scrollTop()
  //       var offset = -100
  //       $('.we__item__val').each(function(){
  //         if($(this).offset().top < winH + winS + offset){
  //           if(!$(this).hasClass('done')){
  //             var val = parseInt($(this).data('val'), 10)
  //             var id = $(this).attr('id')
  //             var item = new CountUp(id, 0, val, 0, 2, {useEasing: false, separator: ' '})
  //             item.start()
  //             $(this).addClass('done')
  //           }
  //         }
  //       })
  //     })
  //   })
  // }
  // head
  if($('#head').length){
    $(function(){
      // $('.head__user').each(function(){
      //   $(this).css({
      //     'min-width': $(this).width() + 10 + 'px'
      //   })
      // })
      // $('.head__user').on('click', function(){
      //   $(this).addClass('active').siblings().removeClass('active')
      //   // $('.abt__cnt').eq(item).addClass('active').siblings().removeClass('active')
      // })
      // $.get('http://ipinfo.io', function(response){
      //   if((response.country == 'AZ')||(response.country == 'AM')||(response.country == 'BY')||(response.country == 'KZ')||(response.country == 'KG')||(response.country == 'MD')||(response.country == 'RU')||(response.country == 'TJ')||(response.country == 'TM')||(response.country == 'UZ')||(response.country == 'UA')){
      //     // $('.ip').html('Rus')
      //     $('.head__lang--ru').addClass('active').siblings().removeClass('active')
      //     // window.location.href = 'index-rus.html'
      //   }else{
      //     $('.head__lang--eng').addClass('active').siblings().removeClass('active')
      //   }
      // }, 'jsonp')

      // $.get("http://ipinfo.io", function(response) {
      //   if(response.country == "US") {
      //     $(".ip").html("Welcome, American!");
      //   }
      //   else if(response.country == "GB") {
      //     $(".ip").html("Welcome, Brit!");
      //   }
      //   else {
      //     $(".ip").html("Hello!");
      //   }
      // }, "jsonp");


      // // var navs = []
      // // $('.head__item__link').each(function(){
      // //   navs.push($(this).attr('href'))
      // // })
      // // $('.home__ttl').html(navs)
      // $(window).on('scroll', function(){
      //   var winS = $(window).scrollTop()
      //   $('.head__item__link').each(function(){
      //     // navs.push($(this).attr('href'))
      //     var id = $(this).attr('href')
      //     // if((winS + winH / 2 < $(id).offset().top + $(id).outerHeight())){
      //     if((winS + winH / 2 > $(id).offset().top) && (winS + winH / 2 < $(id).offset().top + $(id).outerHeight())){
      //       $(this).addClass('active').siblings().removeClass('active')
      //     }else{
      //       $(this).removeClass('active')
      //     }
      //     // $($(this).attr('href')).
      //   })
      //   // var offset = -200
      //   // $('.bid__sttl').each(function(){
      //   //   if($(this).offset().top < winH + winS + offset){
      //   //     $(this).removeClass('anim')
      //   //   }
      //   // })
      // })
      // // var marksTimer = setTimeout(function(){
      // //   $('.home__mark').each(function(){
      // //     $(this).removeClass('anim')
      // //   })
      // // }, 500)
      // // $('.home__mark').on('click', function(){
      // //   $(this).toggleClass('active').siblings().removeClass('active')
      // // })
    })
  }
  // nav
  if($('#nav').length){
    $(function(){
      $('.nav__login').on('click', function(){
        $('.nav__loginw').toggleClass('full')
      })
    })
  }
  // ct
  if($('#ct').length){
    $(function(){
      $('.ct__acrd').accordion({
        heightStyle: 'content',
        collapsible: true,
      })
    })
  }
  // home
  // if($('#home').length){
  //   $(function(){
  //     $('.home__vid').get(0).play()
  //   })
  // }
  // cmp
  // if($('#cmp').length){
  //   $(function(){
  //     $('.cmp__item__vid').addClass('play').each(function(){
  //       $(this).get(0).play()
  //     })
  //     // $(window).on('scroll', function(){
  //     //   var winS = $(window).scrollTop()
  //     //   if(winS + winH > $('.cmp__items').offset().top){
  //     //     $('.cmp__item__vid').addClass('play').each(function(){
  //     //       $(this).get(0).play()
  //     //     })
  //     //   }
  //     // })
  //   })
  // }
  // // abt
  // if($('#abt').length){
  //   $(function(){
  //     $('.abt__nav').on('click', function(){
  //       var item = $(this).index()
  //       $(this).addClass('active').siblings().removeClass('active')
  //       $('.abt__cnt').eq(item).addClass('active').siblings().removeClass('active')
  //     })
  //   })
  // }
  // // std
  // if($('#std').length){
  //   $(function(){
  //     $('.std__nav').on('click', function(){
  //       var item = $(this).index()
  //       $(this).addClass('active').siblings().removeClass('active')
  //       $('.std__cnt').eq(item).addClass('active').siblings().removeClass('active')
  //     })
  //   })
  // }
  // svc
  // if($('#svc').length){
  //   $(function(){
  //     $('.svc__nav').on('click', function(){
  //       var item = $(this).index()
  //       $(this).addClass('active').siblings().removeClass('active')
  //       $('.svc__tab').eq(item).addClass('active').siblings().removeClass('active')
  //     })
  //   })
  // }
  // bid
  // if($('#bid').length){
  //   $(function(){
  //     $('.bid__sttl').addClass('anim')
  //     $(window).on('scroll', function(){
  //       var winS = $(window).scrollTop()
  //       var offset = -200
  //       $('.bid__sttl').each(function(){
  //         if($(this).offset().top < winH + winS + offset){
  //           $(this).removeClass('anim')
  //         }
  //       })
  //     })
  //   })
  // }
  // work
  // if($('#work').length){
  //   $(function(){
  //     var workSlider = new Swiper('.work__slider--container', {
  //       // slidesPerView: 'auto',
  //       slidesPerView: 1,
  //       slidesPerColumn: 1,
  //       watchSlidesProgress: true,
  //       watchSlidesVisibility: true,
  //       spaceBetween: 200,
  //       speed: 500,
  //       // centeredSlides: true,
  //       loop: true,
  //       loopAdditionalSlides: 4,
  //       // loopedSlides: 5, //looped slides should be the same
  //       navigation: {
  //         prevEl: '.work__arr--prev',
  //         nextEl: '.work__arr--next',
  //       },
  //       pagination: {
  //         el: '.work__pags',
  //         bulletElement: 'p',
  //         bulletClass: 'work__pag',
  //         bulletActiveClass: 'active',
  //         // type: 'custom',
  //         clickable: true
  //       },
  //       observer: true,
  //       observeParents: true,
  //       grabCursor: false,
  //       // thumbs: {
  //       //   swiper: navSlider
  //       // }
  //     })
  //     // navSlider.on('slideChange', function(){
  //     //   // $('.work__ttl').html('asdsa')
  //     //   // itemSlider.slideTo(navSlider.realIndex)
  //     //   itemSlider.slideTo(navSlider.activeIndex)
  //     // })
  //     // $('.work__item__tmb').on('click', function(){
  //     //   var item = $(this).index()
  //     //   $(this).addClass('active').siblings().removeClass('active')
  //     //   $(this).parents('.work__item').find('.work__item__pic').eq(item).addClass('active').siblings().removeClass('active')
  //     //   // $('.work__ttl').html(item - 1)
  //     // })
  //   })
  // }
  // bid
  // if($('.bid').length){
  //   $(function(){
  //     $('.bid .form__send').addClass('anim')
  //     $(window).on('scroll', function(){
  //       var winS = $(window).scrollTop()
  //       // var b = $('.bid .form__item--send')
  //       var offset = -100
  //       $('.bid .form__send').each(function(){
  //         if($(this).offset().top < winH + winS + offset){
  //           $(this).removeClass('anim')
  //           // if(!$(this).hasClass('done')){
  //           //   var val = parseInt($(this).data('val'), 10)
  //           //   var id = $(this).attr('id')
  //           //   var item = new CountUp(id, 0, val, 0, 2, {useEasing: false, separator: ' '})
  //           //   item.start()
  //           //   $(this).addClass('done')
  //           // }
  //         }
  //       })
  //     })
  //   })
  // }

  // tech
  // if($('#tech').length){
  //   $(function(){
  //     var stfSlider = new Swiper('.tech__slider--container', {
  //       // slidesPerView: 'auto',
  //       slidesPerView: 2,
  //       slidesPerColumn: 1,
  //       watchSlidesProgress: true,
  //       watchSlidesVisibility: true,
  //       spaceBetween: 40,
  //       speed: 500,
  //       // centeredSlides: true,
  //       loop: true,
  //       loopAdditionalSlides: 4,
  //       // loopedSlides: 5, //looped slides should be the same
  //       // autoplay: {
  //       //   delay: 3000,
  //       // },
  //       navigation: {
  //         prevEl: '.tech__arr--prev',
  //         nextEl: '.tech__arr--next',
  //       },
  //       // pagination: {
  //       //   el: '.crt__pags',
  //       //   bulletElement: 'p',
  //       //   bulletClass: 'crt__pag',
  //       //   bulletActiveClass: 'active',
  //       //   // type: 'custom',
  //       //   clickable: true
  //       // },
  //       observer: true,
  //       observeParents: true,
  //       slideToClickedSlide: true,
  //       grabCursor: false,
  //       breakpoints:{
  //         // when window width is <= 480px
  //         479: {
  //           slidesPerView: 1,
  //           // spaceBetween: 0,
  //         },
  //         767: {
  //           slidesPerView: 2,
  //           // spaceBetween: 24,
  //         },
  //         979: {
  //           slidesPerView: 1,
  //           // spaceBetween: 30,
  //         }
  //       }
  //     })
  //   })
  // }

  // stf
  // if($('#stf').length){
  //   $(function(){
  //     var stfSlider = new Swiper('.stf__slider--container', {
  //       // slidesPerView: 'auto',
  //       slidesPerView: 4,
  //       slidesPerColumn: 1,
  //       watchSlidesProgress: true,
  //       watchSlidesVisibility: true,
  //       spaceBetween: 40,
  //       speed: 500,
  //       // centeredSlides: true,
  //       loop: true,
  //       loopAdditionalSlides: 4,
  //       // loopedSlides: 5, //looped slides should be the same
  //       // autoplay: {
  //       //   delay: 3000,
  //       // },
  //       navigation: {
  //         prevEl: '.stf__arr--prev',
  //         nextEl: '.stf__arr--next',
  //       },
  //       // pagination: {
  //       //   el: '.crt__pags',
  //       //   bulletElement: 'p',
  //       //   bulletClass: 'crt__pag',
  //       //   bulletActiveClass: 'active',
  //       //   // type: 'custom',
  //       //   clickable: true
  //       // },
  //       observer: true,
  //       observeParents: true,
  //       slideToClickedSlide: true,
  //       grabCursor: false,
  //       breakpoints:{
  //         // when window width is <= 480px
  //         479: {
  //           slidesPerView: 1,
  //           spaceBetween: 16,
  //         },
  //         767: {
  //           slidesPerView: 2,
  //           spaceBetween: 24,
  //         },
  //         979: {
  //           slidesPerView: 3,
  //           spaceBetween: 30,
  //         }
  //       }
  //     })
  //   })
  // }

  // crt
  if($('#crt').length){
    $(function(){
      var crtSlider = new Swiper('.crt__slider--container', {
        // slidesPerView: 'auto',
        slidesPerView: 3,
        slidesPerColumn: 1,
        watchSlidesProgress: true,
        watchSlidesVisibility: true,
        spaceBetween: 30,
        speed: 500,
        // centeredSlides: true,
        loop: true,
        loopAdditionalSlides: 4,
        // loopedSlides: 5, //looped slides should be the same
        // autoplay: {
        //   delay: 3000,
        // },
        navigation: {
          prevEl: '.crt__arr--prev',
          nextEl: '.crt__arr--next',
        },
        pagination: {
          el: '.crt__pags',
          bulletElement: 'p',
          bulletClass: 'crt__pag',
          bulletActiveClass: 'active',
          // type: 'custom',
          clickable: true
        },
        observer: true,
        observeParents: true,
        slideToClickedSlide: true,
        grabCursor: false,
        breakpoints:{
          // when window width is <= 480px
          479: {
            slidesPerView: 1,
            // spaceBetween: 16,
          },
          767: {
            slidesPerView: 2,
            spaceBetween: 16,
          },
          // 979: {
          //   slidesPerView: 4,
          //   spaceBetween: 40,
          // }
        }
      })
    })
  }




  // setTimeout(function(){
  //   $('.home__pic').removeClass('anim')
  // }, 100)
  // if($('#home').length){
  //   var pic = $('.home__pic')
  //   var img = $('.home__pic__img')
  //   pic.addClass('anim')
  //   if(winW >= 768){
  //     img.attr('src', img.attr('data-src'))
  //     img.on('load', function(){
  //       pic.removeClass('anim')
  //     })
  //     // var img = document.getElementById('home__pic__img')
  //     // img.onload = function(){
  //     //   $('.home__pic').removeClass('anim')
  //     // }
  //     // $('.home__pic__img').load(function(){
  //     //   setTimeout(function(){
  //     //     $('.home__pic').removeClass('anim')
  //     //   }, 100)
  //     // })
  //   }
  // }
  // if($('#new').length){
  //   $(function(){
  //     var slider = new Swiper('.new__slider--container',{
  //       // slidesPerView: 'auto',
  //       slidesPerView: 1,
  //       slidesPerColumn: 1,
  //       watchSlidesProgress: true,
  //       watchSlidesVisibility: true,
  //       spaceBetween: 0,
  //       speed: 500,
  //       // effect: 'fade',
  //       // fadeEffect: {
  //       //   crossFade: true
  //       // },
  //       // centeredSlides: true,
  //       // loop: true,
  //       // loopAdditionalSlides: 4,
  //       // autoplay: {
  //       //   delay: 3000,
  //       // },
  //       navigation: {
  //         prevEl: '.new__slider__arr--prev',
  //         nextEl: '.new__slider__arr--next',
  //       },
  //       pagination: {
  //         el: '.new__slider__pags',
  //         bulletClass: 'new__slider__pag',
  //         bulletActiveClass: 'active',
  //         // type: 'custom',
  //         clickable: true
  //       },
  //       observer: true,
  //       observeParents: true,
  //       // slideToClickedSlide: true,
  //       grabCursor: false,
  //       // simulateTouch: false,
  //       // preventClicks: false,
  //       // preventClicksPropagation: false,
  //     })
  //     // var boxs = document.getElementById('price__boxs')
  //     // var boxsH = boxs.offsetHeight
  //     // $('.price__nav').on('click', function(){
  //     //   var nav = $(this).data('nav')
  //     //   var box = document.getElementById('price__box--' + nav)
  //     //   var to = box.offsetTop - boxs.offsetTop;
  //     //   $(this).addClass('active').siblings().removeClass('active')
  //     //   scrollTo(boxs, to, 800);
  //     // })
  //     // $('#price__boxs').on('scroll', function(){
  //     //   var boxsS = $(this).scrollTop()
  //     //   boxTops = []
  //     //   $('.price__box').each(function(){
  //     //     boxTops.push($(this).position().top)
  //     //   })
  //     //   for(var i = 0; i < boxTops.length; i++){
  //     //     if((boxTops[i] < boxsH / 2)){
  //     //       // $('.price__note__txt').html('i=' + (i + 1))
  //     //       // $('.price__note__txt').html('boxsH=' + boxsH)
  //     //       $('.price__nav--' + (i + 1)).addClass('active').siblings().removeClass('active')
  //     //     }
  //     //   }
  //     // })
  //   })
  // }
  // if($('#tv').length){
  //   $(function(){
  //     $('.tv__item').addClass('hide')
  //     $('.tv__item').slice(0, 8).removeClass('hide')
  //     $('.tv__items__more').on('click', function(){
  //       // $('.tv__item').removeClass('hide')
  //       $('.tv__item.hide').slice(0, 8).removeClass('hide')
  //       if(!$('.tv__item.hide').length){
  //         $('.tv__itemsw').addClass('done')
  //       }
  //       // $('.tv__item').not('.hide').slice(0, 4).removeClass('hide')
  //       // $('.tv__items').add
  //     })
  //   })
  // }



  // // svc
  // if($('#svc').length){
  //   $(function(){
  //     $('.svc__tr--tip').on('click', function(e){
  //       $(this).toggleClass('hover')
  //     })
  //     // if(winW < 768){
  //     //   $('body').on('click', function(e){
  //     //     if($('.svc__tr--tip.hover').length){
  //     //       if(!$(e.target).hasClass('svc__tr--tip') && !$(e.target).hasClass('hover')){
  //     //       // if(!$(e.target).hasClass('svc__tr__tip')){
  //     //         $('.svc__tr--tip.hover').removeClass('hover')
  //     //       }
  //     //     }
  //     //   })
  //     //   $('.svc__tr--tip').on('click', function(e){
  //     //     // if(!$(e.target).hasClass('svc__tr__tip')){
  //     //     if(!$(e.target).hasClass('svc__tr__tip__close')){
  //     //       $(this).addClass('hover')
  //     //     }
  //     //   })
  //     //   $('.svc__tr__tip__close').on('click', function(){
  //     //     $('.svc__tr--tip').removeClass('hover')
  //     //   })
  //     //   $(window).on('resize', function(){
  //     //     if(winW >= 768){
  //     //       $('.svc__tr--tip').removeClass('hover')
  //     //     }
  //     //   })
  //     // }
  //   })
  // }


  // if($('#works').length){
  //   $(function(){
  //     var eq = 4
  //     $('.works__item').each(function(index){
  //       $(this).addClass('fit works__item--' + $(this).data('type'))
  //       // $('.works__ttl').html($('.works__ttl').html() + index)
  //       if(index < eq){
  //         $(this).addClass('show')
  //       }
  //     })
  //     $('.works__nav').on('click', function(){
  //       var nav = $(this).data('nav')
  //       $('.works').removeClass('noMore')
  //       $(this).addClass('active').siblings().removeClass('active')
  //       $('.works__item').removeClass('fit show')
  //       if(nav == 0){
  //         $('.works__item').addClass('fit')
  //       }else{
  //         $('.works__item--' + nav).addClass('fit')
  //       }
  //       if($('.works__item.fit').not('.show').length <= eq){
  //         $('.works').addClass('noMore')
  //       }
  //       $('.works__item.fit').slice(0, eq).addClass('show')
  //     })
  //     $('.works__more').on('click', function(){
  //       if($('.works__item.fit').not('.show').length <= eq){
  //         $('.works').addClass('noMore')
  //       }
  //       $('.works__item.fit').not('.show').slice(0, eq).addClass('show')
  //     })
  //   })
  // }

  // if($('#price').length){
  //   $(function(){
  //     var slider = new Swiper('.price__slider--container',{
  //       // slidesPerView: 'auto',
  //       slidesPerView: 1,
  //       slidesPerColumn: 1,
  //       watchSlidesProgress: true,
  //       watchSlidesVisibility: true,
  //       spaceBetween: 30,
  //       speed: 500,
  //       // effect: 'fade',
  //       // fadeEffect: {
  //       //   crossFade: true
  //       // },
  //       // centeredSlides: true,
  //       loop: true,
  //       loopAdditionalSlides: 4,
  //       autoplay: {
  //         delay: 3000,
  //       },
  //       navigation: {
  //         prevEl: '.price__slide__arr--prev',
  //         nextEl: '.price__slide__arr--next',
  //       },
  //       // pagination: {
  //       //   el: '.stf__slider__pags',
  //       //   // type: 'custom',
  //       // },
  //       observer: true,
  //       observeParents: true,
  //       // slideToClickedSlide: true,
  //       grabCursor: false,
  //       // simulateTouch: false,
  //       // preventClicks: false,
  //       // preventClicksPropagation: false,
  //     })
  //     var boxs = document.getElementById('price__boxs')
  //     var boxsH = boxs.offsetHeight
  //     $('.price__nav').on('click', function(){
  //       var nav = $(this).data('nav')
  //       var box = document.getElementById('price__box--' + nav)
  //       var to = box.offsetTop - boxs.offsetTop;
  //       $(this).addClass('active').siblings().removeClass('active')
  //       scrollTo(boxs, to, 800);
  //     })
  //     $('#price__boxs').on('scroll', function(){
  //       var boxsS = $(this).scrollTop()
  //       boxTops = []
  //       $('.price__box').each(function(){
  //         boxTops.push($(this).position().top)
  //       })
  //       for(var i = 0; i < boxTops.length; i++){
  //         if((boxTops[i] < boxsH / 2)){
  //           // $('.price__note__txt').html('i=' + (i + 1))
  //           // $('.price__note__txt').html('boxsH=' + boxsH)
  //           $('.price__nav--' + (i + 1)).addClass('active').siblings().removeClass('active')
  //         }
  //       }
  //     })
  //   })
  // }

  // if($('#stf')){
  //   $(function(){
  //     var slider = new Swiper('.stf__slider--container',{
  //       // slidesPerView: 'auto',
  //       slidesPerView: 3,
  //       slidesPerColumn: 1,
  //       watchSlidesProgress: true,
  //       watchSlidesVisibility: true,
  //       spaceBetween: 30,
  //       speed: 500,
  //       // effect: 'fade',
  //       // fadeEffect: {
  //       //   crossFade: true
  //       // },
  //       // centeredSlides: true,
  //       loop: true,
  //       loopAdditionalSlides: 4,
  //       autoplay: {
  //         delay: 3000,
  //       },
  //       navigation: {
  //         prevEl: '.stf__slider__nav--prev',
  //         nextEl: '.stf__slider__nav--next',
  //       },
  //       // pagination: {
  //       //   el: '.stf__slider__pags',
  //       //   // type: 'custom',
  //       // },
  //       observer: true,
  //       observeParents: true,
  //       // slideToClickedSlide: true,
  //       grabCursor: false,
  //       // simulateTouch: false,
  //       // preventClicks: false,
  //       // preventClicksPropagation: false,
  //       breakpoints: {
  //         // // when window width is <= 480px
  //         479: {
  //           slidesPerView: 1,
  //         },
  //         // when window width is <= 640px
  //         // 640: {
  //         //   slidesPerView: 2,
  //         //   spaceBetween: 20
  //         // },
  //         767: {
  //           slidesPerView: 2,
  //           spaceBetween: 30
  //         }
  //       }
  //     })
  //   })
  // }

  // if($('.modal--work')){
  //   $(function(){
  //     $('.modal__work__pic').on('click', function(){
  //       var pic = $(this).data('pic')
  //       $('.modal__work__bigpic__item').eq(pic - 1).addClass('active').siblings().removeClass('active')
  //     })
  //     // $('.modal__work__pic__arr').on('click', function(){
  //     //   var cur = $('.modal__work__bigpic__item.active').index()
  //     //   var all = $('.modal__work__bigpic__item').length
  //     //   var pic = 1
  //     //   if($(this).hasClass('modal__work__pic__arr--prev')){
  //     //     if(cur == 0){
  //     //       pic = all - 1
  //     //     }else{
  //     //       pic = cur - 1
  //     //     }
  //     //   }else{
  //     //     if(cur == all){
  //     //       pic = 1
  //     //     }else{
  //     //       pic = cur + 1
  //     //     }
  //     //   }
  //     //   $('.modal__work__bigpic__item').eq(pic).addClass('active').siblings().removeClass('active')
  //     // })
  //   })
  // }




  // // cat
  // if($('#cat')){
  //   $(function(){
  //     // var type = 0
  //     // var stype = 0
  //     var startLength = 6
  //     var moreLength = 6
  //     $('.cat__item').slice(0, startLength).addClass('show')
  //     $('.cat__item').each(function(){
  //       $(this).addClass('type--' + $(this).data('type'))
  //       $(this).addClass('stype--' + $(this).data('stype'))
  //     })

  //     $('.cat__more').on('click', function(){
  //       $('.cat__item').not('.show').slice(0, moreLength).addClass('show')
  //       if($('.cat__item').not('.show').length == 0){
  //         $('.cat').addClass('noMore')
  //       }
  //     })
  //   })
  // }


  // // we
  // if($('#stf')){
  //   $(function(){
  //     var slider = new Swiper('.stf__slider--container',{
  //       // slidesPerView: 'auto',
  //       slidesPerView: 3,
  //       slidesPerColumn: 1,
  //       watchSlidesProgress: true,
  //       watchSlidesVisibility: true,
  //       spaceBetween: 30,
  //       speed: 500,
  //       // effect: 'fade',
  //       // fadeEffect: {
  //       //   crossFade: true
  //       // },
  //       // centeredSlides: true,
  //       loop: true,
  //       loopAdditionalSlides: 4,
  //       autoplay: {
  //         delay: 3000,
  //       },
  //       navigation: {
  //         prevEl: '.stf__slider__nav--prev',
  //         nextEl: '.stf__slider__nav--next',
  //       },
  //       pagination: {
  //         el: '.stf__slider__pags',
  //         // type: 'custom',
  //       },
  //       observer: true,
  //       observeParents: true,
  //       // slideToClickedSlide: true,
  //       grabCursor: false,
  //       // simulateTouch: false,
  //       // preventClicks: false,
  //       // preventClicksPropagation: false,
  //       breakpoints: {
  //         // // when window width is <= 480px
  //         479: {
  //           slidesPerView: 1,
  //         },
  //         // when window width is <= 640px
  //         // 640: {
  //         //   slidesPerView: 2,
  //         //   spaceBetween: 20
  //         // },
  //         1023: {
  //           slidesPerView: 2,
  //           spaceBetween: 30
  //         }
  //       }
  //     })
  //     var picH = $('.stf__slide__pic').first().height()
  //     var navH = $('.stf__slider__nav').first().height()
  //     // $('.edge2__ttl').html(($('.edge2__slide__pic').first().height() - $('.edge2__slider__nav').first().height()) / 2)
  //     $('.stf__slider__nav').css({
  //       'top': (picH - navH) / 2 + 'px',
  //       'bottom': 'auto'
  //     })
  //     // $(window).on('resize', function(){
  //     //   picH = $('.edge2__slide__pic').first().height()
  //     //   navH = $('.edge2__slider__nav').first().height()
  //     //   $('.edge2__slider__nav').css({
  //     //     'top': (picH - navH) / 2 + 'px'
  //     //   })
  //     // })
  //     var resizeTimer;
  //     $(window).on('resize', function(e) {
  //       clearTimeout(resizeTimer);
  //       resizeTimer = setTimeout(function() {
  //         picH = $('.stf__slide__pic').first().height()
  //         navH = $('.stf__slider__nav').first().height()
  //         $('.stf__slider__nav').css({
  //           'top': (picH - navH) / 2 + 'px'
  //         })
  //       }, 250);
  //     });
  //     $(window).resize()
  //   })
  // }




  // // we
  // if($('#we')){
  //   $(function(){
  //     var slider = new Swiper('.we__slider--container',{
  //       // slidesPerView: 'auto',
  //       slidesPerView: 1,
  //       slidesPerColumn: 1,
  //       watchSlidesProgress: true,
  //       watchSlidesVisibility: true,
  //       spaceBetween: 50,
  //       speed: 500,
  //       effect: 'fade',
  //       fadeEffect: {
  //         crossFade: true
  //       },
  //       // centeredSlides: true,
  //       loop: true,
  //       loopAdditionalSlides: 4,
  //       autoplay: {
  //         delay: 3000,
  //       },
  //       // navigation: {
  //       //   prevEl: '.we__nav--prev',
  //       //   nextEl: '.we__nav--next',
  //       // },
  //       pagination: {
  //         el: '.we__slider__pag',
  //         // type: 'custom',
  //       },
  //       observer: true,
  //       observeParents: true,
  //       // slideToClickedSlide: true,
  //       grabCursor: false,
  //       // simulateTouch: false,
  //       // preventClicks: false,
  //       // preventClicksPropagation: false,
  //       // breakpoints: {
  //       //   // // when window width is <= 480px
  //       //   480: {
  //       //     slidesPerView: 1,
  //       //   },
  //       //   // when window width is <= 640px
  //       //   768: {
  //       //     slidesPerView: 2,
  //       //     spaceBetween: 20
  //       //   },
  //       //   // 1020: {
  //       //   //   slidesPerView: 3,
  //       //   //   spaceBetween: 10
  //       //   // }
  //       // }
  //     })
  //   })
  // }





  // // rev
  // if($('#rev')){
  //   $(function(){
  //     var slider = new Swiper('.rev__slider--container',{
  //       // slidesPerView: 'auto',
  //       slidesPerView: 1,
  //       slidesPerColumn: 1,
  //       watchSlidesProgress: true,
  //       watchSlidesVisibility: true,
  //       spaceBetween: 1000,
  //       speed: 500,
  //       // effect: 'fade',
  //       // fadeEffect: {
  //       //   crossFade: true
  //       // },
  //       // centeredSlides: true,
  //       // loop: true,
  //       // loopAdditionalSlides: 2,
  //       // autoplay: {
  //       //   delay: 3000,
  //       // },
  //       navigation: {
  //         prevEl: '.rev__slider__nav--prev',
  //         nextEl: '.rev__slider__nav--next',
  //       },
  //       pagination: {
  //         el: '.rev__slider__pags',
  //         // type: 'custom',
  //       },
  //       observer: true,
  //       observeParents: true,
  //       // grabCursor: false,
  //       // simulateTouch: false,
  //       // preventClicks: false,
  //       // preventClicksPropagation: false,
  //       // breakpoints: {
  //       //   // // when window width is <= 480px
  //       //   // 480: {
  //       //   //   slidesPerView: 1,
  //       //   // },
  //       //   // when window width is <= 640px
  //       //   767: {
  //       //     slidesPerView: 1,
  //       //     // spaceBetween: 20
  //       //   },
  //       //   // 1020: {
  //       //   //   slidesPerView: 3,
  //       //   //   spaceBetween: 10
  //       //   // }
  //       // }
  //     })
  //   })
  // }






  // // modal cat
  // if($('#modal--cat')){
  //   $(function(){
  //     $('.modal__cat__nav').on('click', function(){
  //       var tab = $(this).data('tab') - 1
  //       $(this).addClass('active').siblings().removeClass('active')
  //       $('.modal__cat__tab').eq(tab).addClass('active').siblings().removeClass('active')
  //     })
  //     $('.modal__cat__pics__nav').on('click', function(){
  //       var item = $(this).data('item') - 1
  //       $(this).addClass('active').siblings().removeClass('active')
  //       $('.modal__cat__pic').eq(item).addClass('active').siblings().removeClass('active')
  //     })
  //     $('.modal__cat__pics__arr').on('click', function(){
  //       var item = $(this).parent('.modal__cat__itemw').find('.active').data('item')
  //       var items = $(this).parent('.modal__cat__itemw').find('.modal__cat__pics__nav').length
  //       if(item == items)
  //         item = 0
  //       $('.modal__cat__pics__nav').eq(item).addClass('active').siblings().removeClass('active')
  //       $('.modal__cat__pic').eq(item).addClass('active').siblings().removeClass('active')
  //     })
  //   })
  // }













  // modal
  $(function(){
    // $.modalClose = function(e){
    //   var $th = e.currentTarget
    //   var $modal = $th.closest('.modal')
    //   $modal.classList.remove('show')
    // }
    // $.openModal = function (name){
    //   var $modals = document.querySelectorAll('.modal')
    //   var $modal = document.querySelector('.modal--' + name)
    //   $modals.forEach(function(obj){
    //     obj.classList.remove('show')
    //   })
    //   $modal.classList.add('show')
    // }
    $('.modal__bg').on('click',function(){
      $(this).parents('.modal').removeClass('show')
    })
    $('.modal__close').on('click',function(){
      $(this).parents('.modal').removeClass('show')
    })
    $('.openModal').on('click',function(){
      var modal = $(this).attr('data-modal')
      $('.modal').removeClass('show')
      $('.modal--' + modal).addClass('show')
      if(modal == 'call'){
        var ttl = $(this).attr('ttl'),
            subtitle = $(this).attr('subtitle'),
            subject = $(this).attr('subject'),
            send = $(this).attr('send')
        if(!ttl)
          // title = 'Заказать звонок'
          ttl = 'Оставить заявку'
          // title = 'Оставьте свои контакты и мы свяжемся <span class=nobr>с вами в течение 10 минут</span>'
        if(!subtitle)
          // subtitle = 'Оставьте заявку и администратор<br class=desk> свяжется с вами <span class=nobr>в ближайшее время</span>'
          // subtitle = 'Оставьте заявку, и наш эксперт ответит на все<br class=desk> интересующие <span class=nobr>Вас вопросы</span>'
        if(!subject)
          subject = 'Оставить заявку'
        if(!send)
          send = 'Отправить'
        $('.modal__call__ttl').html(ttl)
        $('.modal__call__sttl').html(subtitle)
        $('.modal--call__formName').html(subject)
        $('.modal--call .form__send').html(send)
      }
    })
  })



});



$(window).on('load', function(){
  $('body').addClass('load')
})