//@codekit-prepend 'base/ymap.min2.js';
//@codekit-prepend 'ext/ping.js';
$(document).ready(function(){
  // base
  var winW = $(window).width(),
      winH = $(window).height();

  // contact
  $(function(){
    function isSiteOnline(url,callback){
      // try to load favicon
      var timer = setTimeout(function(){
        // timeout after 5 seconds
        callback(false);
      },5000)
      var img = document.createElement("img");
      img.onload = function() {
        clearTimeout(timer);
        callback(true);
      }
      img.onerror = function() {
        clearTimeout(timer);
        callback(false);
      }
      img.src = url+"/favicon.ico";
    }
    // isSiteOnline("http://fb.com",function(found){
    isSiteOnline("http://ya.ru",function(found){
      if(found){
        ymaps.ready(init);
        var map,
            mapView,
            markIndex,
            mark1,
            mark2,
            mark3,
            mark4,
            mark5,
            markView;
        function init(){
          // markIndex = new ymaps.Placemark([55.734973, 37.571013],{
          //   // iconCaption: '',
          //   // balloonHeader: '',
          //   balloonContent: 'Твинтел • Телевидение, интернет, телефония'
          // },{
          //   // preset:'islands#dotIcon',
          //   // iconColor:'#e96620',

          //   // balloonLayout: MyBalloonLayout,
          //   // balloonContentLayout: MyBalloonContentLayout,

          //   iconLayout: 'default#image',
          //   iconImageHref: 'img/ct-map-ic.svg',
          //   iconImageSize: [50, 60],
          //   iconImageOffset: [-25, -60]
          // })
          var markC1 = [43.593517, 39.724751]
          var markC2 = [43.620665, 39.722792]
          var markC3 = [43.660324, 39.657961]
          var markC4 = [43.915376, 39.340424]
          var markC5 = [43.518457, 39.856848]
          var markCs = [markC1, markC2, markC3, markC4, markC5]
          mark1 = new ymaps.Placemark(markCs[0],{balloonContent: 'Твинтел • Телевидение, интернет, телефония'},{preset:'islands#dotIcon',iconColor:'#1E98FF'})
          mark2 = new ymaps.Placemark(markCs[1],{balloonContent: 'Твинтел • Телевидение, интернет, телефония'},{preset:'islands#dotIcon',iconColor:'#1E98FF'})
          mark3 = new ymaps.Placemark(markCs[2],{balloonContent: 'Твинтел • Телевидение, интернет, телефония'},{preset:'islands#dotIcon',iconColor:'#1E98FF'})
          mark4 = new ymaps.Placemark(markCs[3],{balloonContent: 'Твинтел • Телевидение, интернет, телефония'},{preset:'islands#dotIcon',iconColor:'#1E98FF'})
          mark5 = new ymaps.Placemark(markCs[4],{balloonContent: 'Твинтел • Телевидение, интернет, телефония'},{preset:'islands#dotIcon',iconColor:'#1E98FF'})
          // mark1 = new ymaps.Placemark([43.593517, 39.724751],{balloonContent: 'Твинтел • Телевидение, интернет, телефония'},{preset:'islands#dotIcon',iconColor:'#1E98FF'})
          // mark2 = new ymaps.Placemark([43.620665, 39.722792],{balloonContent: 'Твинтел • Телевидение, интернет, телефония'},{preset:'islands#dotIcon',iconColor:'#1E98FF'})
          // mark3 = new ymaps.Placemark([43.660324, 39.657961],{balloonContent: 'Твинтел • Телевидение, интернет, телефония'},{preset:'islands#dotIcon',iconColor:'#1E98FF'})
          // mark4 = new ymaps.Placemark([43.915376, 39.340424],{balloonContent: 'Твинтел • Телевидение, интернет, телефония'},{preset:'islands#dotIcon',iconColor:'#1E98FF'})
          // mark5 = new ymaps.Placemark([43.518457, 39.856848],{balloonContent: 'Твинтел • Телевидение, интернет, телефония'},{preset:'islands#dotIcon',iconColor:'#1E98FF'})
          markView = new ymaps.Placemark([55.734973, 37.571013],{
            balloonContent: 'Твинтел • Телевидение, интернет, телефония'
          },{
            preset:'islands#dotIcon',
            iconColor:'#ff3333',

            // balloonLayout: MyBalloonLayout,
            // balloonContentLayout: MyBalloonContentLayout,

            // iconLayout: 'default#image',
            // iconImageHref: 'img/ct-map-ic.svg',
            // iconImageSize: [50, 60],
            // iconImageOffset: [-25, -60]
          })
          if($('#ct__map')){
            map = new ymaps.Map('ct__map', {
              center: [43.729107, 39.602528],
              zoom: 10,
              controls: []
            })
            map.behaviors.disable('scrollZoom');
            // map.behaviors.disable('drag');
            map.controls.add('zoomControl', {size: 'small', position:{bottom: 40, right: 20}});
            // map.geoObjects.add(markIndex);
            map.geoObjects.add(mark1);
            map.geoObjects.add(mark2);
            map.geoObjects.add(mark3);
            map.geoObjects.add(mark4);
            map.geoObjects.add(mark5);
            $('.ct__acrd__ttl').on('click', function(){
              var mark = $(this).data('map') - 1
              map.setCenter(markCs[mark])
              map.setZoom(14)
            })
          }
          if($('#view__map')){
            mapView = new ymaps.Map('view__map', {
              center: [55.734973, 37.571013],
              zoom: 16,
              controls: []
            })
            mapView.behaviors.disable('scrollZoom');
            mapView.geoObjects.add(markView);
          }
          // $(window).on('resize',function(){
          //   var winW = $(window).width();
          //   map.setCenter([55.734973, 37.571013])
          //   // map.setZoom(14)
          //   // if(winW >= 480){
          //   //   map.setCenter([55.766, 37.588])
          //     if(winW >= 768){
          //       map.setCenter([55.734973, 37.567013])
          //       // if(winW >= 1024){
          //       //   map.setCenter([55.7595, 37.747])
          //       //   // map.setZoom(15)
          //       // }
          //     }
          //   // }
          // })
          // $(window).resize();
        }
      }else{
      }
    })
  })
});