<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Главная страница»");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Главная страница");
?>

    <div class="fade_content">
        <div class="nav">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 offset_col"></div>
                    <? $APPLICATION->IncludeComponent("bitrix:menu", "bootstrap_v4", Array(
                        "ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
                        "MAX_LEVEL" => "1",    // Уровень вложенности меню
                        "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "MENU_CACHE_TYPE" => "A",    // Тип кеширования
                        "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                        "MENU_CACHE_USE_GROUPS" => "N",    // Учитывать права доступа
                        "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                    ),
                        false
                    ); ?>
                </div>
            </div>
            <div class="assideBlock">
                <div class="assideItems">
                    <a href="http://tvintel.speedtestcustom.com/" target="_blank">
                        <div class="assideItem">
                            <span>Тест скорости</span>
                            <img src="/bitrix/templates/tvintel/img/Group_24.png" alt="test"
                                 style="width: 45px;margin-top: -11px;margin-left: -20px;">
                        </div>
                    </a>
                    <a href="/web-camera/" target="_blank">
                        <div class="assideItem item2">
                            <span>Сочи онлайн</span>
                            <img src="/bitrix/templates/tvintel/img/Group_23.png" alt="test"
                                 style="width: 45px;margin-top: -11px;margin-left: -20px;">
                        </div>
                    </a>
                </div>
            </div>
            <div class="lkPopup">
                <a href="javascript:void(0);" title="" class="lk th"><?
global $USER;
if ($USER->IsAuthorized()) {
    ?>
    <?

    $userid = $USER->GetID();
    $rsUser = CUser::GetByID($userid);
    $arUser = $rsUser->Fetch();
    ?>
    <?
    $name1 = explode(" ", $arUser["NAME"]);
    $name2 = $name1[1];
    $name3 = $name1[2];
    ?>
    <?=$name1[0]?> <?=mb_substr($name2, 0, 1);?>.<?=mb_substr($name3, 0, 1);?>.
    <? } else { ?>
    Личный кабинет
<? };
                    ?>
    </a>
                <div class="loginPopUp">
                    <div class="login-title step1">
                        Добро пожаловать
                    </div>
                    <?
                    global $USER;
                    if ($USER->IsAuthorized()) {
                        ?>

                        <!-- dropleft -->
                        <div class="dropdown ">111
                            <a class=" user-auth" href="/user.php" role="button" id="dropdownMenuLink">
                                <?

                                $userid = $USER->GetID();
                                $rsUser = CUser::GetByID($userid);
                                $arUser = $rsUser->Fetch();
                                echo $arUser["NAME"];
                                ?>
                            </a>

                            <span class="adress-user"><?= $arUser["UF_ADDRESS"]; ?></span>
                            <div class="d-flex justify-content-between align-items-end">
                                <div class="col-auto m-0 p-0">
                                    <ul>
                                        <li><p>Состояние счета:</p><span><?= $arUser["UF_MONEY"]; ?></span></li>
                                        <li><p>Тариф: </p><span style="color: <? echo $_SESSION['tariff_color']; ?>;"><?= $arUser["UF_TARIF"]; ?></span></li>
                                        <li><p>Статус:</p><span><?= $arUser["UF_STATUS"]; ?></span></li>
                                        <li><p>Абонентская плата:</p><span><?= $arUser["UF_ABONPLATA"]; ?></span></li>
                                    </ul>
                                </div>
                                <div class="col-auto p-2">
                                    <a href="<? echo $APPLICATION->GetCurPageParam("logout=yes", array(
                                        "login",
                                        "logout",
                                        "register",
                                        "forgot_password",
                                        "change_password")); ?>" class="boxed_btn">Выйти</a>


                                </div>
                            </div>
                        </div>
                    <? } else {
                        ?>
                        <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "login", Array(
                                "REGISTER_URL" => "register.php",
                                "FORGOT_PASSWORD_URL" => "",
                                "PROFILE_URL" => "profile.php",
                                "SHOW_ERRORS" => "Y"

                            )
                        ); ?>
                    <? };
                    ?>


                </div>
            </div>

            <style>
                .assideItems a {
                    display: block;
                }

                .item2 {
                    margin-top: 10px;
                }

                .assideItems div {
                    width: 45px;
                    height: 45px;
                    border-radius: 50px;
                    padding: 11px;
                    float: right;
                    transition: 0.5s;
                    margin-right: 15px;
                    background: linear-gradient(45deg, #364df0, #7f24dc);
                    padding-left: 20px;
                }

                .assideItems img {
                    background: linear-gradient(45deg, #364df0, #7f24dc);
                    border-radius: 50px;
                }

                .assideItems div.active {
                    width: 215px;
                    transition: 0.5s;
                }

                .assideItems div span {
                    position: absolute;
                    width: 200px;
                    opacity: 0;
                    margin-top: 20px;
                    margin-left: -65px;
                    font-size: 14px;
                    transition: .5s;
                }
                .assideItems .assideItem:hover span {
                    opacity: 1;
                    margin-top: 40px;
                }
                .assideItems div.active span {
                    opacity: 1;
                    transition-delay: 0.5s;
                }

                .go_connect {
                    transition: 1.5s;
                    /*transition-delay: 1.5s;*/
                    position: fixed;
                    left: auto;
                    top: auto;

                }

                .go_connect {

                    margin: 0;

                }

                .mast__title {
                    margin: 0 auto;
                    position: relative;
                }

                .go_connect {

                    width: calc(100% - 60px);
                    text-align: center;

                }

                .go_connect.swap {
                    transform: scale(0.3);
                }

                @media (max-width: 1368px) {
                    .logo_col {
                        width: 35%;
                        margin-top: 0;
                    }
                }

                @media (max-width: 1303px) {
                    .logo_col {
                        margin-top: -20px;
                    }
                }

                @media (max-width: 961px) {
                    .go_connect.swap .mast__title {

                        margin: 0;
                        position: relative;
                        width: 500px;

                    }

                    .mast__title {
                        margin-left: 0px !important;
                    }
                }
            </style>
            <script>
                // $('.assideItem').hover(
                //     function () {
                //         $(this).addClass('active');
                //     }, function () {
                //         $(this).removeClass('active');
                //     }
                // );
                $(document).ready(function () {
                    if ($(window).width() >= 961) {
                        setTimeout((function () {
                            $(".go_connect").animate({
                                left: "-115px", // ширина элемента
                                top: "200px", // высота элемента
                                width: "920px"
                            });
                        }), 5000);
                        setTimeout((function () {
                            $('.go_connect').addClass('swap');
                        }), 5400);
                    } else {
                        setTimeout((function () {
                            $(".go_connect").animate({
                                right: "25%", // ширина элемента
                                top: "120px", // высота элемента
                                width: "100%"
                            });
                        }), 5000);
                        setTimeout((function () {
                            $('.go_connect').addClass('swap');
                        }), 5400);
                    }

                });
            </script>
            <a href="tel:+78622711111" title="" target="_blank" class="lk">
                +7 (862) 2711-111
            </a>
        </div>
        <div class="video_wr" style="background-image:url(bg/videoplayback.jpg)">
            <video autoplay="autoplay" loop="loop" preload="auto" playsinline="" muted="" autobuffer=""
                   poster="bg/videoplayback.jpg" class="main_video js-main-video"
                   id="video">
                <source src="video/home-page.mp4" type="video/mp4">
                <source src="video/home-page.webm" type="video/ogg; codecs=theora, vorbis">
            </video>
            <div class="noise"></div>
        </div>
        <div class="wrapper header">
            <div class="main_content">
                <div class="container" style="margin-left: 0;">
                    <div class="row">
                        <div class="col-xs-3 logo_col">
                            <a href="/" title="Главная" class="logo">

                                <span style="position: absolute;width: 59px;height: 58px;border: transparent;border-radius: 10px;overflow: hidden;margin-top: 23px;">
                                    <img src="/bitrix/templates/tvintel/img/tv-logo.gif" alt=""
                                         style="width: 67px;margin-top: -2px;position: absolute;z-index: 9;margin-left: -5px;max-width: 100px;">
                                </span>

                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/logo.png" alt="">
                            </a>
                            <div class="slogan">
                                <!-- С нами мир ближе! -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 content_col">
                            <!--<span class="rbg"></span>-->
                            <div class="go_connect">
                                <div class="mast__title js-spanize">
                                    С нами мир ближе
                                </div>
                                <div class="shift"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3"></div>
                        <div class="col-xs-9 cat_col" style="opacity: 0.88;">
                            <div class="categories">
<!--                                <div class="choose">Выберите <br>подходящее предложение</div>-->
                                <div class="row">
                                    <div class="col-xs-4 cat_wr">
                                        <a href="/for-apartment/" class="cat_item"><div class="frame"></div><img src="/bitrix/templates/main/images/flat.png" alt=""><span>Для <br>квартиры</span></a>
                                    </div>
                                    <div class="col-xs-4 cat_wr">
                                        <a href="/for_dom/" class="cat_item"><div class="frame"></div><img src="/bitrix/templates/main/images/house.png" alt=""><span>Для <br>дома</span></a>
                                    </div>
                                    <div class="col-xs-4 cat_wr">
                                        <a href="/for_biz/" class="cat_item"><div class="frame"></div><img src="/bitrix/templates/main/images/office.png" alt=""><span>Для <br>бизнеса</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>