<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");
?><section class="tvpromo" id="tvpromo">
    <div class="tvpromo--in">
        <div class="tvpromo__box">
            <div class="tvpromo__info">
                <h2 class="tvpromo__ttl">Пришло время<br>
                    <span class="nobr strong">супертелевидения</span></h2>
                <p class="tvpromo__txt">
                    Цифровое телевидение Твинтел Юг, передаваемое по волоконно-оптическим каналам связи позволяет абонентам наслаждаться четкостью изображения любимых каналов и не требует установки дополнительного оборудования. Быстрая настройка телевизора, программа передач и выгодные условия подключения Вас приятно удивят.
                </p>
            </div>
            <p class="tvpromo__pic">
                <img src="/bitrix/templates/tvintel_new/img/tvpromo-pic.png"
                     class="tvpromo__pic__img"
                     style="width: 84%; margin: 0 10px 0 89px"
                />
            </p>
        </div>
    </div>
</section> <section class="tv" id="tv">
    <div class="inet--in">
        <h2 class="tv__ttl" style="margin-top: 40px">Тарифы <span class="nobr">Кабельное Телевидение</span></h2>
        <!--            <ul class="prc__navs tabs-links">--> <!--                <li class="prc__nav active" id="nav0" onclick="tab(1);">--> <!--                    <p class="prc__nav__ic prc__nav__ic--1"></p>--> <!--                    <p class="prc__nav__txt">Для квартиры</p>--> <!--                </li>--> <!--                <li class="prc__nav" id="nav1" onclick="tab(2);">--> <!--                    <p class="prc__nav__ic prc__nav__ic--2"></p>--> <!--                    <p class="prc__nav__txt">Для частного дома</p>--> <!--                </li>--> <!--            </ul>-->
        <div class="prc__itsw" id="tab1">
            <ul class="prc__its">
                <?$arrFilter = Array(
                    "SECTION_ID" => '21'
                );?> <?$APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "tariff_detail",
                    Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array(0=>"",1=>"",),
                        "FILTER_NAME" => "arrFilter",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "2",
                        "IBLOCK_TYPE" => "tarif",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "9",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "round",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array(0=>"PRICE",1=>"PERIOD",2=>"SALE",3=>"DAY",4=>"NIGHT",5=>"ANAL",6=>"CIFRA",7=>"HD_CANAL",8=>"TELEPHONE",9=>"",),
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N"
                    )
                );?>
            </ul>
            <h2 class="tv__ttl">Тарифы <span class="nobr">IP TV!</span></h2>
            <ul class="prc__its">
                <?$arrFilter = Array(
                    "SECTION_ID" => '20'
                );?> <?$APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "tariff_detail",
                    Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array(0=>"",1=>"",),
                        "FILTER_NAME" => "arrFilter",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "2",
                        "IBLOCK_TYPE" => "tarif",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "9",
                        "NOT_SHOW_ADDRESSCHECK_BLOCK" => true,
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "round",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array(0=>"PRICE",1=>"PERIOD",2=>"SALE",3=>"DAY",4=>"NIGHT",5=>"ANAL",6=>"CIFRA",7=>"HD_CANAL",8=>"TELEPHONE",9=>"",),
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "Y",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "DESC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N"
                    )
                );?>
            </ul>
        </div>
        <!--            <div class="prc__itsw" id="tab2">--> <!----> <!--                <ul class="prc__its">--> <!--            --> <!--                    --><?//$arrFilter = Array(
        //                        "SECTION_ID" => '19'
        //                    );?> <!--                    --><?//$APPLICATION->IncludeComponent("bitrix:news.list", "tariff_detail", Array(
        //                        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
        //                        "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
        //                        "AJAX_MODE" => "N",	// Включить режим AJAX
        //                        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
        //                        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
        //                        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
        //                        "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
        //                        "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
        //                        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
        //                        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
        //                        "CACHE_TYPE" => "A",	// Тип кеширования
        //                        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
        //                        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        //                        "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
        //                        "DISPLAY_DATE" => "Y",
        //                        "DISPLAY_NAME" => "Y",
        //                        "DISPLAY_PICTURE" => "Y",
        //                        "DISPLAY_PREVIEW_TEXT" => "Y",
        //                        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
        //                        "FIELD_CODE" => array(	// Поля
        //                            0 => "",
        //                            1 => "",
        //                        ),
        //                        "FILTER_NAME" => "arrFilter",	// Фильтр
        //                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
        //                        "IBLOCK_ID" => "2",	// Код информационного блока
        //                        "IBLOCK_TYPE" => "tarif",	// Тип информационного блока (используется только для проверки)
        //                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
        //                        "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
        //                        "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
        //                        "NEWS_COUNT" => "9",	// Количество новостей на странице
        //                        "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
        //                        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
        //                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
        //                        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
        //                        "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
        //                        "PAGER_TEMPLATE" => "round",	// Шаблон постраничной навигации
        //                        "PAGER_TITLE" => "Новости",	// Название категорий
        //                        "PARENT_SECTION" => "",	// ID раздела
        //                        "PARENT_SECTION_CODE" => "",	// Код раздела
        //                        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
        //                        "PROPERTY_CODE" => array(	// Свойства
        //                            0 => "PRICE",
        //                            1 => "PERIOD",
        //                            2 => "SALE",
        //                            3 => "DAY",
        //                            4 => "NIGHT",
        //                            5 => "ANAL",
        //                            6 => "CIFRA",
        //                            7 => "HD_CANAL",
        //                            8 => "TELEPHONE",
        //                            9 => "",
        //                        ),
        //                        "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
        //                        "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
        //                        "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
        //                        "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
        //                        "SET_STATUS_404" => "N",	// Устанавливать статус 404
        //                        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
        //                        "SHOW_404" => "N",	// Показ специальной страницы
        //                        "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
        //                        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
        //                        "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
        //                        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
        //                        "STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
        //                    ),
        //                        false
        //                    );?> <!----> <!--                </ul>--> <!----> <!--                <h2 class="tv__ttl">Тарифы <span class="nobr">IP TV!</span></h2>--> <!--                --> <!--                <ul class="prc__its">--> <!--        --> <!--                    --><?//$arrFilter = Array(
        //                        "SECTION_ID" => '16'
        //                    );?> <!--                    --><?//$APPLICATION->IncludeComponent("bitrix:news.list", "tariff_detail", Array(
        //                        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
        //                        "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
        //                        "AJAX_MODE" => "N",	// Включить режим AJAX
        //                        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
        //                        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
        //                        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
        //                        "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
        //                        "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
        //                        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
        //                        "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
        //                        "CACHE_TYPE" => "A",	// Тип кеширования
        //                        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
        //                        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        //                        "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
        //                        "DISPLAY_DATE" => "Y",
        //                        "DISPLAY_NAME" => "Y",
        //                        "DISPLAY_PICTURE" => "Y",
        //                        "DISPLAY_PREVIEW_TEXT" => "Y",
        //                        "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
        //                        "FIELD_CODE" => array(	// Поля
        //                            0 => "",
        //                            1 => "",
        //                        ),
        //                        "FILTER_NAME" => "arrFilter",	// Фильтр
        //                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
        //                        "IBLOCK_ID" => "2",	// Код информационного блока
        //                        "IBLOCK_TYPE" => "tarif",	// Тип информационного блока (используется только для проверки)
        //                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
        //                        "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
        //                        "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
        //                        "NEWS_COUNT" => "9",	// Количество новостей на странице
        //                        "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
        //                        "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
        //                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
        //                        "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
        //                        "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
        //                        "PAGER_TEMPLATE" => "round",	// Шаблон постраничной навигации
        //                        "PAGER_TITLE" => "Новости",	// Название категорий
        //                        "PARENT_SECTION" => "",	// ID раздела
        //                        "PARENT_SECTION_CODE" => "",	// Код раздела
        //                        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
        //                        "PROPERTY_CODE" => array(	// Свойства
        //                            0 => "PRICE",
        //                            1 => "PERIOD",
        //                            2 => "SALE",
        //                            3 => "DAY",
        //                            4 => "NIGHT",
        //                            5 => "ANAL",
        //                            6 => "CIFRA",
        //                            7 => "HD_CANAL",
        //                            8 => "TELEPHONE",
        //                            9 => "",
        //                        ),
        //                        "SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
        //                        "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
        //                        "SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
        //                        "SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
        //                        "SET_STATUS_404" => "N",	// Устанавливать статус 404
        //                        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
        //                        "SHOW_404" => "N",	// Показ специальной страницы
        //                        "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
        //                        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
        //                        "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
        //                        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
        //                        "STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
        //                        "NOT_SHOW_ADDRESSCHECK_BLOCK" => true,
        //                    ),
        //                        false
        //                    );?> <!----> <!--                </ul>--> <!--            </div>-->
    </div>
    </section><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>