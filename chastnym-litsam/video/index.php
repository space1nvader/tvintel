<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");
?><section class="videopromo" id="videopromo">
<div class="videopromo--in">
	<div class="videopromo__box">
		<div class="videopromo__info">
			<h2 class="videopromo__ttl"> Облачное <br>
 <span class="nobr strong">видеонаблюдение</span> </h2>
			<p class="videopromo__txt">
				 Позаботиться о безопасности и удаленно контролировать ситуацию вам поможет облачный сервис видеонаблюдения. Полный комплекс услуг проектирования системы с нуля, ее обслуживание или внедрение в существующую инфраструктуру с минимальной модернизацией. Простота использования — это лишь часть преимуществ мультисервисной сети Твинтел Юг.
			</p>
		</div>
		<p class="videopromo__pic">
 <img src="/bitrix/templates/tvintel_new/img/videopromo-pic.png" class="videopromo__pic__img">
		</p>
	</div>
</div>
 </section> <section class="inet" id="inet">
<div class="inet--in">
	<h2 class="tv__ttl">Тарифы <span class="nobr">Видеонаблюдение</span></h2>
	 <!--        <ul class="prc__navs tabs-links">--> <!--            <li class="prc__nav active" id="nav0" onclick="tab(1);">--> <!--                <p class="prc__nav__ic prc__nav__ic--1"></p>--> <!--                <p class="prc__nav__txt">Для квартиры</p>--> <!--            </li>--> <!--            <li class="prc__nav" id="nav1" onclick="tab(2);">--> <!--                <p class="prc__nav__ic prc__nav__ic--2"></p>--> <!--                <p class="prc__nav__txt">Для частного дома</p>--> <!--            </li>--> <!--        </ul>-->
	<div class="prc__itsw" id="tab1">
		<ul class="prc__its">
			 <?$arrFilter = Array(
                    "SECTION_ID" => '6'
                );?> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"tariff_detail",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "arrFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "tarif",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "9",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "round",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"PRICE",1=>"PERIOD",2=>"SALE",3=>"DAY",4=>"NIGHT",5=>"ANAL",6=>"CIFRA",7=>"HD_CANAL",8=>"TELEPHONE",9=>"",),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
		</ul>
	</div>
</div>
 </section><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>