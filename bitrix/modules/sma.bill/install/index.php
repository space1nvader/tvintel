<?
IncludeModuleLangFile(__FILE__);

Class sma_bill extends CModule
{
    const MODULE_ID = 'sma.bill';
    var $MODULE_ID = 'sma.bill';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    //var $strError = '';

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . "/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = GetMessage("SMART_B.BILL_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("SMART_B.BILL_MODULE_DESC");
        $this->PARTNER_NAME = GetMessage("SMART_B.BILL_PARTNER_NAME");
        $this->PARTNER_URI = GetMessage("SMART_B.BILL_PARTNER_URI");
    }

    function InstallDB($arParams = array())
    {
		RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CSmaBill', 'OnBuildGlobalMenu');
        return true;
    }

    function UnInstallDB($arParams = array())
    {
		UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'CSmaBill', 'OnBuildGlobalMenu');
        return true;
    }

    function InstallEvents()
    {
        CAgent::AddAgent( "CSmaBill::CheckDates;", "sma.bill", "N", 86400, "", "Y");
		//CAgent::AddAgent( "CSmaBill::CheckDates();", "sma.bill", "Y", 86400, "", "Y");
        return true;
    }

    function UnInstallEvents()
    {
        CAgent::RemoveAgent("CSmaBill::CheckDates();", "sma.bill");
        return true;
    }

    function InstallFiles($arParams = array())
    {
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/sma.bill/install/components/sma/bill', $_SERVER['DOCUMENT_ROOT'] . "/bitrix/components/sma/bill", true, true);
        return true;
    }

    function UnInstallFiles()
    {

        return true;
    }

    function DoInstall()
    {
        global $APPLICATION;
        $this->InstallFiles();
		$this->InstallDB();
		$this->InstallEvents();
        RegisterModule(self::MODULE_ID);
    }

    function DoUninstall()
    {
        global $APPLICATION;
        UnRegisterModule(self::MODULE_ID);
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();
    }
}

?>
