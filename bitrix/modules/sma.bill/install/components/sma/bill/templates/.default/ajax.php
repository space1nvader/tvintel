<?
ini_set('display_errors', '0');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
IncludeModuleLangFile(__FILE__);
CModule::IncludeModule("sma.bill");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");

function get_action_price($a)
{

    $a = trim($a);

    $price_of_action = 0;

    if (strlen($a) > 0) {
        if ($a == "top") {
            $price_of_action = COption::GetOptionString("bill", "BILL_LINK_TOP_PRICE");
        } elseif ($a == "fix") {
            $price_of_action = COption::GetOptionString("bill", "BILL_LINK_FIX_PRICE");
        } elseif ($a == "mark") {
            $price_of_action = COption::GetOptionString("bill", "BILL_LINK_MARK_PRICE");
        }
    }

    return $price_of_action;

}

function money_withdraw($sum)
{
    $sum = intval($sum);
    $operation_result = false;
    if ($sum > 0) {
        if (!is_object($USER)) $USER = new CUser;
        $aUserAccount = CSaleUserAccount::GetByUserID($USER->GetID(), "RUB");
        if ($aUserAccount["CURRENT_BUDGET"] >= $sum) {
            if (CSaleUserAccount::Withdraw($USER->GetID(), $sum, "RUB")) {
                $operation_result = true;
            }
        }
    }
    return $operation_result;
}

function process_action($action, $element_id)
{

    $RESULT = false;

    if ($action == "top") {

        $element = new CIBlockElement;
        $aElementFields = Array(
            "ACTIVE_FROM" => ConvertTimestamp(time(), "FULL")
        );
        $element->Update($element_id, $aElementFields);

        $RESULT = GetMessage('BILL_OK');

    }

    if ($action == "fix") {

        // пробуем найти св-во
        $BILL_LINK_FIX_PROP = COption::GetOptionString("bill", "BILL_LINK_FIX_PROP");
        $qElement = CIBlockElement::GetByID($element_id);
        if ($aElement = $qElement->GetNext()) {

            $TARGET_PROPERTY_ID = false;

            $qProperty = CIBlockProperty::GetList(Array(), Array("IBLOCK_ID" => $aElement["IBLOCK_ID"], "CODE" => $BILL_LINK_FIX_PROP, "PROPERTY_TYPE" => "L"));
            if ($aProperty = $qProperty->GetNext()) {

                // нашли св-во, ищем значение
                $qEnumValue = CIBlockProperty::GetPropertyEnum($aProperty["ID"]);
                if ($aEnumValue = $qEnumValue->GetNext()) {
                    $TARGET_PROPERTY_ID = $aEnumValue["ID"];
                }

            } else {

                // не нашли св-во, создаем
                $property = new CIBlockProperty;
                $aPropertyFields = Array(
                    "NAME" => GetMessage('BILL_PRIKREPIT'),
                    "ACTIVE" => "Y",
                    "SORT" => "500",
                    "CODE" => $BILL_LINK_FIX_PROP,
                    "PROPERTY_TYPE" => "L",
                    "IBLOCK_ID" => $aElement["IBLOCK_ID"]
                );
                $aPropertyFields["VALUES"][0] = Array(
                    "VALUE" => GetMessage('BILL_DA'),
                    "DEF" => "N",
                    "SORT" => "10"
                );
                $TARGET_PROPERTY_ID = $property->Add($aPropertyFields);

            }

            // если на выходе есть id св-ва
            if ($TARGET_PROPERTY_ID) {

                CIBlockElement::SetPropertyValues($element_id, $aElement["IBLOCK_ID"], $TARGET_PROPERTY_ID, $BILL_LINK_FIX_PROP);

                $RESULT = GetMessage('BILL_OK');

                // запишем время на которое элемент был закреплен
                $BILL_LINK_FIXDATE_PROP = COption::GetOptionString("bill", "BILL_LINK_FIXDATE_PROP"); // КОД св-ва
                $BILL_LINK_FIX_TIME = COption::GetOptionString("bill", "BILL_LINK_FIX_TIME"); // Время закрепления
                $datetime = AddToTimeStamp(Array("DD" => $BILL_LINK_FIX_TIME), time());
                $datetime = ConvertTimeStamp($datetime, "SHORT");
                $qProperty = CIBlockProperty::GetList(Array(), Array("IBLOCK_ID" => $aElement["IBLOCK_ID"], "CODE" => $BILL_LINK_FIXDATE_PROP, "PROPERTY_TYPE" => "S"));
                if ($aProperty = $qProperty->GetNext()) {

                    CIBlockElement::SetPropertyValues($element_id, $aElement["IBLOCK_ID"], $datetime, $aProperty["CODE"]);

                } else {

                    // создаем св-во
                    $property = new CIBlockProperty;
                    $aPropertyFields = Array(
                        "NAME" => GetMessage('BILL_DATE_K'),
                        "ACTIVE" => "Y",
                        "SORT" => "500",
                        "CODE" => $BILL_LINK_FIXDATE_PROP,
                        "PROPERTY_TYPE" => "S",
                        "IBLOCK_ID" => $aElement["IBLOCK_ID"],
                        "USER_TYPE" => "DateTime"
                    );
                    $property->Add($aPropertyFields);
                    CIBlockElement::SetPropertyValues($element_id, $aElement["IBLOCK_ID"], $datetime, $BILL_LINK_FIXDATE_PROP);

                }

            }

        } else {

            //$response = GetMessage("BILL.ELEMENT_NOT_FOUND");
            $response = GetMessage('BILL_NO_ELEMENT');

        }

    }

    if ($action == "mark") {

        // пробуем найти св-во
        $BILL_LINK_MARK_PROP = COption::GetOptionString("bill", "BILL_LINK_MARK_PROP");
        $qElement = CIBlockElement::GetByID($element_id);
        if ($aElement = $qElement->GetNext()) {

            $TARGET_PROPERTY_ID = false;

            $qProperty = CIBlockProperty::GetList(Array(), Array("IBLOCK_ID" => $aElement["IBLOCK_ID"], "CODE" => $BILL_LINK_MARK_PROP, "PROPERTY_TYPE" => "L"));
            if ($aProperty = $qProperty->GetNext()) {

                // нашли св-во, ищем значение
                $qEnumValue = CIBlockProperty::GetPropertyEnum($aProperty["ID"]);
                if ($aEnumValue = $qEnumValue->GetNext()) {
                    $TARGET_PROPERTY_ID = $aEnumValue["ID"];
                }

            } else {

                // не нашли св-во, создаем
                $property = new CIBlockProperty;
                $aPropertyFields = Array(
                    "NAME" => GetMessage('BILL_VIDELIT'),
                    "ACTIVE" => "Y",
                    "SORT" => "500",
                    "CODE" => $BILL_LINK_MARK_PROP,
                    "PROPERTY_TYPE" => "L",
                    "IBLOCK_ID" => $aElement["IBLOCK_ID"]
                );
                $aPropertyFields["VALUES"][0] = Array(
                    "VALUE" => GetMessage('BILL_DA'),
                    "DEF" => "N",
                    "SORT" => "10"
                );
                $TARGET_PROPERTY_ID = $property->Add($aPropertyFields);

            }

            // если на выходе есть id св-ва
            if ($TARGET_PROPERTY_ID) {

                CIBlockElement::SetPropertyValues($element_id, $aElement["IBLOCK_ID"], $TARGET_PROPERTY_ID, $BILL_LINK_MARK_PROP);

                $RESULT = GetMessage('BILL_OK');

            }

        } else {

            //$response = GetMessage("BILL.ELEMENT_NOT_FOUND");
            $RESULT = GetMessage('BILL_NO_ELEMENT');

        }

    }

    return $RESULT;

}

if ($_GET["get_price"] == "Y" && $_GET["action"]) {
    echo get_action_price($_GET["action"]);
}

if ($_GET["process"] == "Y" && $_GET["action"] && $_GET["ELEMENT_ID"]) {

    $RESULT = false;

    // Проверим авторизацию
    if (!is_object($USER)) $USER = new CUser;
    if (!$USER->IsAuthorized()) {

        $RESULT = GetMessage('BILL_USER_IS_NOT_AUTHORIZED');

    } else {

        // Пробуем списать средства
        $price = get_action_price($_GET["action"]);
        if (money_withdraw($price)) {

            $RESULT = process_action($_GET["action"], $_GET["ELEMENT_ID"]);

        } else {

            $RESULT = GetMessage('BILL_INSUFFICIENT_FUNDS');

        }

    }

    echo $RESULT;

}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>