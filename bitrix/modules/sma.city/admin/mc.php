<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');
}

IncludeModuleLangFile(__FILE__);
$moduleID = 'sma.city';

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $GLOBALS['APPLICATION']->SetTitle('Центр управления');
}

$RIGHT = $GLOBALS['APPLICATION']->GetGroupRight($moduleID);
if ($RIGHT >= 'R') {
    if (CModule::IncludeModule($moduleID)) {
        ?>
        <script id="bx24_form_inline" data-skip-moving="true">
            (function (w, d, u, b) {
                w['Bitrix24FormObject'] = b;
                w[b] = w[b] || function () {
                        arguments[0].ref = u;
                        (w[b].forms = w[b].forms || []).push(arguments[0])
                    };
                if (w[b]['forms']) return;
                var s = d.createElement('script');
                s.async = 1;
                s.src = u + '?' + (1 * new Date());
                var h = d.getElementsByTagName('script')[0];
                h.parentNode.insertBefore(s, h);
            })(window, document, 'https://smart-b.bitrix24.ru/bitrix/js/crm/form_loader.js', 'b24form');

            b24form({"id": "2", "lang": "ru", "sec": "7bf4ly", "type": "inline"});
        </script>
            <?
    } else {
        CAdminMessage::ShowMessage(GetMessage('ASPRO_NEXT_MODULE_NOT_INCLUDED'));
    }
} else {
    CAdminMessage::ShowMessage(GetMessage('ASPRO_NEXT_NO_RIGHTS_FOR_VIEWING'));
}
?>
<?
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
}
?>