<?
AddEventHandler('main', 'OnBuildGlobalMenu', 'addMenuItem');
function addMenuItem(&$aGlobalMenu, &$aModuleMenu)
{
    global $USER;

    if ($USER->IsAdmin()) {
        IncludeModuleLangFile(__FILE__);
        $moduleID = 'sma.city';
        $GLOBALS['APPLICATION']->SetAdditionalCss("/bitrix/css/" . $moduleID . "/menu.css");
        $aGlobalMenu['global_menu_sma'] = [
            'menu_id' => 'sma',
            'text' => 'Smart-B',
            'title' => 'Smart-B: Региональный портал',
            'url' => 'settingss.php?lang=ru',
            'sort' => 100,
            'items_id' => 'global_menu_sma',
            'help_section' => 'custom',
            'items' => [
                [
                    'parent_menu' => 'global_menu_sma',
                    'sort' => 10,
                    'url' => 'sma_city_mc.php',
                    'text' => 'Центр управления',
                    'title' => 'Центр управления',
                    'icon' => 'imi_control_center',
                    'page_icon' => 'imi_control_center',
                    'items_id' => 'menu_sma_mc',
                ],
                [
                    'parent_menu' => 'global_menu_sma',
                    'sort' => 20,
                    'url' => 'sma_bill_options.php',
                    'text' => 'Модуль биллинг',
                    'title' => 'Модуль биллинг',
                    'icon' => 'imi_sale',
                    'page_icon' => 'imi_sale',
                    'items_id' => 'menu_sma_bill',
                ],
                
            ],
        ];

    }
}

?>