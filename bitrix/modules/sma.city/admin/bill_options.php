<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');

global $APPLICATION;
IncludeModuleLangFile(__FILE__);

//$moduleClass = "CNext";
$moduleID = "sma.city";
\Bitrix\Main\Loader::includeModule($moduleID);

use \Bitrix\Main\Config\Option,
    \Bitrix\Main\Localization\Loc;

$arAllOptions = Array(
    "service_price" => Array(
        Array("BILL_LINK_TOP_PRICE", "Стоимость поднятия элемента", Array("text", "50")),
        Array("BILL_LINK_FIX_PRICE", "Стоимость закрепления элемента", Array("text", "50")),
        Array("BILL_LINK_MARK_PRICE", "Стоимость выделения элемента", Array("text", "50")),
    ),
    "element_props" => Array(
        Array("BILL_LINK_FIX_PROP", "Свойство элемента для закрепления", Array("text", "50")),
        Array("BILL_LINK_FIXDATE_PROP", "Свойство элемента для даты срока закрепления", Array("text", "50")),
        Array("BILL_LINK_MARK_PROP", "Свойство элемента для выделения", Array("text", "50")),
    ),
    "additional_settings" => Array(
        Array("BILL_LINK_FIX_TIME", "Время на которое закрепляем элемент (суток)", Array("text", "50"))
    )
);

$aTabs = array(
    array("DIV" => "edit1", "TAB" => "Настройки", "ICON" => "search_settings", "TITLE" => "Настройка параметров модуля")
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && check_bitrix_sessid()) {
    if (strlen($RestoreDefaults) > 0) {
        COption::RemoveOption("bill");
    } else {
        foreach ($arAllOptions as $option_key => $arOptions) {
            foreach ($arOptions as $arOption) {
                $name = $arOption[0];
                $val = trim($_POST[$name]);
                if ($arOption[2][0] == "checkbox" && $val != "Y")
                    $val = "N";
                COption::SetOptionString("bill", $name, $val, $arOption[1]);
            }
        }
    }
    if (strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0)
        LocalRedirect($_REQUEST["back_url_settings"]);
    else
        LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
}

$tabControl->Begin();
?>
<form method="post"
      action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<?= LANGUAGE_ID ?>">
    <?
    $tabControl->BeginNextTab();
    foreach ($arAllOptions as $option_key => $arOptions):
        ?>

        <tr class="heading">
            <td colspan="2"><? echo GetMessage($option_key) ?></td>
        </tr>

        <?
        foreach ($arOptions as $arOption):
            $val = COption::GetOptionString("bill", $arOption[0]);
            $type = $arOption[2];
            ?>
            <tr>
                <td valign="top" width="50%">
                    <? if ($type[0] == "checkbox"):?>
                        <? echo "<label for=\"" . htmlspecialchars($arOption[0]) . "\">" . $arOption[1] . "</label>"; ?>:
                    <? else:?>
                        <? echo $arOption[1]; ?>:
                    <? endif; ?>
                </td>
                <td valign="top" width="50%">
                    <? if ($type[0] == "checkbox"):?>
                        <input type="checkbox" name="<? echo htmlspecialchars($arOption[0]) ?>"
                               id="<? echo htmlspecialchars($arOption[0]) ?>"
                               value="Y"<? if ($val == "Y") echo " checked"; ?>>
                    <? elseif ($type[0] == "text"):?>
                        <input type="text" size="<? echo $type[1] ?>" maxlength="255"
                               value="<? echo htmlspecialchars($val) ?>"
                               name="<? echo htmlspecialchars($arOption[0]) ?>">
                    <? elseif ($type[0] == "select"):?>
                        <select name="<? echo htmlspecialchars($arOption[0]) ?>">
                            <? foreach ($type[1] as $key => $value):?>
                                <option <? if ($val == $key) echo " selected"; ?>
                                    value="<?= $key ?>"><?= $value ?></option>
                            <? endforeach; ?>
                        </select>
                    <? elseif ($type[0] == "textarea"):?>
                        <textarea rows="<? echo $type[1] ?>" cols="<? echo $type[2] ?>"
                                  name="<? echo htmlspecialchars($arOption[0]) ?>"><? echo htmlspecialchars($val) ?></textarea>
                    <? endif ?>
                </td>
            </tr>
        <? endforeach ?>
    <? endforeach ?>
    <? $tabControl->Buttons(); ?>
    <input type="submit" name="Update" value="<?= GetMessage("MAIN_SAVE") ?>"
           title="<?= GetMessage("MAIN_OPT_SAVE_TITLE") ?>">
    <input type="submit" name="Apply" value="<?= GetMessage("MAIN_OPT_APPLY") ?>"
           title="<?= GetMessage("MAIN_OPT_APPLY_TITLE") ?>">
    <? if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
        <input type="button" name="Cancel" value="<?= GetMessage("MAIN_OPT_CANCEL") ?>"
               title="<?= GetMessage("MAIN_OPT_CANCEL_TITLE") ?>"
               onclick="window.location='<? echo htmlspecialchars(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
        <input type="hidden" name="back_url_settings" value="<?= htmlspecialchars($_REQUEST["back_url_settings"]) ?>">
    <? endif ?>
    <input type="submit" name="RestoreDefaults" title="<? echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
           OnClick="return confirm('<? echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
           value="<? echo GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
    <?= bitrix_sessid_post(); ?>
    <? $tabControl->End(); ?>
</form>