<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!defined("WIZARD_SITE_ID") || !defined("WIZARD_SITE_DIR"))
    return;

function ___writeToAreasFile($path, $text)
{

    $fd = @fopen($path, "wb");
    if (!$fd)
        return false;

    if (false === fwrite($fd, $text)) {
        fclose($fd);
        return false;
    }

    fclose($fd);

    if (defined("BX_FILE_PERMISSIONS")) {
        @chmod($path, BX_FILE_PERMISSIONS);
    }
}

if (COption::GetOptionString("main", "upload_dir") == "") {
    COption::SetOptionString("main", "upload_dir", "upload");
}

//if(COption::GetOptionString("eshop", "wizard_installed", "N", WIZARD_SITE_ID) == "N" || WIZARD_INSTALL_DEMO_DATA)
//{
if (file_exists(
    WIZARD_ABSOLUTE_PATH . "/site/public/" . LANGUAGE_ID . "/"
)) {
    CopyDirFiles(
        WIZARD_ABSOLUTE_PATH . "/site/public/" . LANGUAGE_ID . "/",
        WIZARD_SITE_PATH,
        $rewrite = true,
        $recursive = true,
        $delete_after_copy = false
    );
}
//}

$wizard =& $this->GetWizard();


WizardServices::ReplaceMacrosRecursive(
    WIZARD_SITE_PATH, Array("SITE_DIR" => WIZARD_SITE_DIR)
);
CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH . "/.section.php",
    array("SITE_DESCRIPTION" => htmlspecialcharsbx(
        $wizard->GetVar("siteMetaDescription")
    ))
);
CWizardUtil::ReplaceMacros(
    WIZARD_SITE_PATH . "/.section.php",
    array("SITE_KEYWORDS" => htmlspecialcharsbx(
        $wizard->GetVar("siteMetaKeywords")
    ))
);

copy(
    WIZARD_THEME_ABSOLUTE_PATH . "/favicon.ico",
    WIZARD_SITE_PATH . "favicon.ico"
);


$arUrlRewrite = array();
if (file_exists(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php")) {
    include(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php");
}

$arNewUrlRewrite = array(
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "social/my/catalog/#",
        "RULE" => "",
        "ID" => "bitrix:iblock.element.add.form",
        "PATH" => WIZARD_SITE_DIR . "social/my/catalog/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "social/my/critik/#",
        "RULE" => "",
        "ID" => "bitrix:iblock.element.add.form",
        "PATH" => WIZARD_SITE_DIR
            . "social/my/board/index.php/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "social/my/auto/#",
        "RULE" => "",
        "ID" => "bitrix:iblock.element.add.form",
        "PATH" => WIZARD_SITE_DIR . "social/my/auto/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "information/catalog/map/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR
            . "information/catalog/map/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "social/my/\"}#",
        "RULE" => "",
        "ID" => "bitrix:iblock.element.add.list",
        "PATH" => WIZARD_SITE_DIR . "social/my/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "information/catalog/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "information/catalog/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "afisha/fotootchet/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "afisha/fotootchet/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "social/my/critik/#",
        "RULE" => "",
        "ID" => "bitrix:iblock.element.add.form",
        "PATH" => WIZARD_SITE_DIR . "social/my/critik/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "auto/test-drive/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "auto/test-drive/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "afisha/critic/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "afisha/critic/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "social/photo/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "social/photo/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "job/vacancy/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "job/vacancy/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "job/resume/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "job/resume/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "auto/board/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "auto/board/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "realty/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "realty/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "afisha/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "afisha/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "forum/#",
        "RULE" => "",
        "ID" => "bitrix:forum",
        "PATH" => WIZARD_SITE_DIR . "forum/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "board/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "board/index.php",
    ),
    array(
        "CONDITION" => "#^" . WIZARD_SITE_DIR . "news/#",
        "RULE" => "",
        "ID" => "bitrix:news",
        "PATH" => WIZARD_SITE_DIR . "news/index.php",
    ),
);

foreach ($arNewUrlRewrite as $arUrl) {
    if (!in_array($arUrl, $arUrlRewrite)) {
        CUrlRewriter::Add($arUrl);
    }
}
?>