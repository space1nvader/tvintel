<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Я кинокритик");
?><?
$APPLICATION->IncludeComponent(
    "bitrix:main.include", ".default", array(
    "AREA_FILE_SHOW" => "file",
    "PATH" => SITE_DIR . "include/kinocritic.php",
    "EDIT_TEMPLATE" => ""
), false
);
?><br>
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12 shadow-right">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:news", "kinocritic", array(
                "IBLOCK_TYPE" => "afisha_gorportal",
                "IBLOCK_ID" => "#KRITIK_IBLOCK_ID#",
                "NEWS_COUNT" => "20",
                "USE_SEARCH" => "N",
                "USE_RSS" => "N",
                "USE_RATING" => "Y",
                "USE_CATEGORIES" => "N",
                "USE_REVIEW" => "Y",
                "USE_FILTER" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "CHECK_DATES" => "Y",
                "SEF_MODE" => "Y",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "SET_STATUS_404" => "Y",
                "SET_TITLE" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "ADD_ELEMENT_CHAIN" => "N",
                "USE_PERMISSIONS" => "N",
                "PREVIEW_TRUNCATE_LEN" => "100",
                "LIST_ACTIVE_DATE_FORMAT" => "j F Y",
                "LIST_FIELD_CODE" => array(
                    0 => "CREATED_BY",
                    1 => "",
                ),
                "LIST_PROPERTY_CODE" => array(
                    0 => "FILM",
                    1 => "",
                ),
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "DISPLAY_NAME" => "Y",
                "META_KEYWORDS" => "-",
                "META_DESCRIPTION" => "-",
                "BROWSER_TITLE" => "NAME",
                "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                "DETAIL_FIELD_CODE" => array(
                    0 => "CREATED_BY",
                    1 => "",
                ),
                "DETAIL_PROPERTY_CODE" => array(
                    0 => "FILM",
                    1 => "",
                ),
                "DETAIL_DISPLAY_TOP_PAGER" => "N",
                "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                "DETAIL_PAGER_TITLE" => "Страница",
                "DETAIL_PAGER_TEMPLATE" => "",
                "DETAIL_PAGER_SHOW_ALL" => "N",
                "PAGER_TEMPLATE" => "infinity",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "USE_SHARE" => "Y",
                "AJAX_OPTION_ADDITIONAL" => "",
                "MAX_VOTE" => "5",
                "VOTE_NAMES" => array(
                    0 => "1",
                    1 => "2",
                    2 => "3",
                    3 => "4",
                    4 => "5",
                    5 => "",
                ),
                "MESSAGES_PER_PAGE" => "10",
                "USE_CAPTCHA" => "Y",
                "REVIEW_AJAX_POST" => "Y",
                "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
                "FORUM_ID" => "#FORUM_ID#",
                "URL_TEMPLATES_READ" => "",
                "SHOW_LINK_TO_FORUM" => "Y",
                "SEF_FOLDER" => "#SITE_DIR#afisha/critic/",
                "SHARE_HIDE" => "N",
                "SHARE_TEMPLATE" => "",
                "SHARE_HANDLERS" => array(
                    0 => "twitter",
                    1 => "lj",
                    2 => "mailru",
                    3 => "facebook",
                    4 => "delicious",
                    5 => "vk",
                ),
                "SHARE_SHORTEN_URL_LOGIN" => "",
                "SHARE_SHORTEN_URL_KEY" => "",
                "SEF_URL_TEMPLATES" => array(
                    "news" => "",
                    "section" => "",
                    "detail" => "#ELEMENT_ID#/",
                )
            ), false
            );
            ?>
        </div>
        <div class="col-md-3 col-sm-3 hidden-xs">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:main.include", "include", array(
                "AREA_FILE_SHOW" => "sect",
                "EDIT_TEMPLATE" => "",
                "AREA_FILE_SUFFIX" => "inc",
                "AREA_FILE_RECURSIVE" => "Y"
            ), false
            );
            ?>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>