<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Добавить рецензию");
?>
    <div class="row">
        <div
            class="<? if ($USER->IsAuthorized()): ?>col-md-9 col-sm-9 col-xs-12<? else: ?>col-md-12 col-sm-12 col-xs-12<? endif ?>">
            <? $APPLICATION->IncludeComponent(
                "bitrix:iblock.element.add.form",
                "form_critik",
                array(
                    "IBLOCK_TYPE" => "afisha_gorportal",
                    "IBLOCK_ID" => "#KRITIK_IBLOCK_ID#",
                    "STATUS_NEW" => "N",
                    "LIST_URL" => SITE_DIR . "social/my/critik/",
                    "USE_CAPTCHA" => "Y",
                    "USER_MESSAGE_EDIT" => "Успешно обновлено",
                    "USER_MESSAGE_ADD" => "Успешно добавлено",
                    "DEFAULT_INPUT_SIZE" => "30",
                    "RESIZE_IMAGES" => "Y",
                    "PROPERTY_CODES" => array(
                        0 => "NAME",
                        1 => "DETAIL_TEXT",
                    ),
                    "PROPERTY_CODES_REQUIRED" => array(
                        0 => "NAME",
                        1 => "DETAIL_TEXT",
                    ),
                    "GROUPS" => array(
                        0 => "2",
                    ),
                    "STATUS" => "ANY",
                    "ELEMENT_ASSOC" => "CREATED_BY",
                    "MAX_USER_ENTRIES" => "1000",
                    "MAX_LEVELS" => "1",
                    "LEVEL_LAST" => "Y",
                    "MAX_FILE_SIZE" => "0",
                    "PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
                    "DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
                    "SEF_MODE" => "Y",
                    "SEF_FOLDER" => "/social/my/critik/",
                    "CUSTOM_TITLE_NAME" => "Заголовок",
                    "CUSTOM_TITLE_TAGS" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                    "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                    "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                    "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                    "CUSTOM_TITLE_DETAIL_TEXT" => "Ваша рецензия",
                    "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                    "COMPONENT_TEMPLATE" => "form_critik"
                ),
                false
            ); ?>
        </div>
        <? if ($USER->IsAuthorized()): ?>
            <div class="col-md-3 col-sm-3 hidden-xs">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/my_cabinet.php"
                    )
                ); ?>
            </div>
        <? endif ?>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>