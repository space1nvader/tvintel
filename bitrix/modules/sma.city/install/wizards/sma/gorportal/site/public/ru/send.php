<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
$mail = COption::GetOptionString("main", "email_from");
if (!isset($_POST['message']) or empty($_POST['message'])) {
    $error4 = "<div class='alert alert-danger'>Сообщение не заполнено</div>";
} else $error4 = NULL;

if (!isset($_POST['link']) or empty($_POST['link'])) {
    $error2 = "<div class='alert alert-danger'>Нет ссылки</div>";
} else $error2 = NULL;

if (!isset($_POST['subject']) or empty($_POST['subject'])) {
    $error3 = "<div class='alert alert-danger'>Тема не заполнена</div>";
} else $error3 = NULL;
$mess = $_POST['message'];
if (empty($error4) and empty($error3)) {
    $subject = $_POST['subject'];
    $from = $mail;
    $headers = "Content-type: text/html; charset=\"utf-8\"\r\n";
    $headers .= "From: <" . $from . ">\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Date: " . date('D, d M Y h:i:s O') . "\r\n";
    $message = "Сообщение:" . $mess . "<br>Ссылка на страницу с ошибкой: " . $_POST['link'];
    if (mail($mail, $subject, $message, $headers)) {
        echo "<div class='alert alert-success'>Спасибо, мы обязательно рассмотрим вашу жалобу.</div>";
    } else echo "<div class='alert alert-danger'>Ошибка!</div>";
} else {
    echo $error4 . $error3 . $error2;
}
?>