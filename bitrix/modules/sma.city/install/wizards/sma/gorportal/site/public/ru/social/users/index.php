<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Пользователи портала");
$APPLICATION->SetPageProperty("description", "Пользователи портала");
$APPLICATION->SetTitle("Пользователи портала");
?><? $APPLICATION->IncludeComponent("bitrix:forum.user.list", "users", Array(
    "SHOW_USER_STATUS" => "Y",    // Показывать статус пользователя
    "URL_TEMPLATES_MESSAGE_SEND" => "message_send.php?TYPE=#TYPE#&UID=#UID#",    // Страница отправки сообщения
    "URL_TEMPLATES_PM_EDIT" => "pm_edit.php?FID=#FID#&MID=#MID#&UID=#UID#&mode=#mode#",    // Страница личных сообщений
    "URL_TEMPLATES_PROFILE_VIEW" => SITE_DIR . "forum/user/#UID#/",    // Страница профиля пользователя
    "URL_TEMPLATES_USER_POST" => "user_post.php?UID=#UID#&mode=#mode#",    // Страница сообщений пользователя
    "CACHE_TYPE" => "A",    // Тип кеширования
    "CACHE_TIME" => "0",    // Время кеширования (сек.)
    "USERS_PER_PAGE" => "20",    // Количество пользователей на одной странице
    "SET_NAVIGATION" => "Y",    // Показывать навигацию
    "DATE_FORMAT" => "d.m.Y",    // Формат показа даты
    "DATE_TIME_FORMAT" => "d.m.Y H:i:s",    // Формат показа даты и времени
    "PAGE_NAVIGATION_TEMPLATE" => "",    // Название шаблона для вывода постраничной навигации
    "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
    "SEO_USER" => "Y",    // Не индексировать ссылку на профиль
),
    false
); ?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>