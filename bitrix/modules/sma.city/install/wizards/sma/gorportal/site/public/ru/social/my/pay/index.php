<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Пополнение счета");
?><? $APPLICATION->IncludeComponent(
    "bitrix:sale.order.ajax",
    "",
    Array(
        "ALLOW_AUTO_REGISTER" => "N",
        "ALLOW_NEW_PROFILE" => "N",
        "COMPONENT_TEMPLATE" => ".default",
        "COUNT_DELIVERY_TAX" => "N",
        "DELIVERY_NO_AJAX" => "N",
        "DELIVERY_NO_SESSION" => "Y",
        "DELIVERY_TO_PAYSYSTEM" => "p2d",
        "DISABLE_BASKET_REDIRECT" => "N",
        "ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
        "PATH_TO_AUTH" => "/auth/",
        "PATH_TO_BASKET" => "basket.php",
        "PATH_TO_PAYMENT" => "payment.php",
        "PATH_TO_PERSONAL" => "index.php",
        "PAY_FROM_ACCOUNT" => "N",
        "PRODUCT_COLUMNS" => array(),
        "SEND_NEW_USER_NOTIFY" => "N",
        "SET_TITLE" => "Y",
        "SHOW_PAYMENT_SERVICES_NAMES" => "Y",
        "SHOW_STORES_IMAGES" => "N",
        "TEMPLATE_LOCATION" => "popup",
        "USE_PREPAYMENT" => "N"
    )
); ?><br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>