<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Мой кабинет");
?>
<? if ($USER->IsAuthorized()): ?>
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#board"
                           aria-controls="board"
                           role="tab"
                           data-toggle="tab">Мои
                            объявления</a>
                    </li>
                    <li role="presentation">
                        <a href="#vacancy"
                           aria-controls="vacancy"
                           role="tab" data-toggle="tab">Мои
                            вакансии</a>
                    </li>
                    <li role="presentation">
                        <a href="#resume"
                           aria-controls="resume" role="tab"
                           data-toggle="tab">Мои
                            резюме</a>
                    </li>
                    <li role="presentation">
                        <a href="#auto" aria-controls="auto"
                           role="tab" data-toggle="tab">Продажа
                            авто</a>
                    </li>
                    <li role="presentation">
                        <a href="#realty"
                           aria-controls="realty" role="tab"
                           data-toggle="tab">Недвижимость</a>
                    </li>
                </ul>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="board">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:iblock.element.add.list",
                        "my_board",
                        array(
                            "EDIT_URL" => "board/",
                            "NAV_ON_PAGE" => "10",
                            "MAX_USER_ENTRIES" => "100000",
                            "IBLOCK_TYPE" => "board_gorportal",
                            "IBLOCK_ID" => "#BOARD_IBLOCK_ID#",
                            "GROUPS" => array(
                                0 => "1",
                                1 => "3",
                                2 => "4",
                            ),
                            "STATUS" => "ANY",
                            "ELEMENT_ASSOC" => "CREATED_BY",
                            "ALLOW_EDIT" => "Y",
                            "ALLOW_DELETE" => "Y",
                            "SEF_MODE" => "Y",
                            "SEF_FOLDER" => SITE_DIR . "social/my/"
                        ),
                        false
                    ); ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="vacancy">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:iblock.element.add.list",
                        "my_board",
                        array(
                            "EDIT_URL" => "job/",
                            "NAV_ON_PAGE" => "10",
                            "MAX_USER_ENTRIES" => "100000",
                            "IBLOCK_TYPE" => "job_gorportal",
                            "IBLOCK_ID" => "#JOB_IBLOCK_ID#",
                            "GROUPS" => array(
                                0 => "1",
                                1 => "3",
                                2 => "4",
                            ),
                            "STATUS" => "ANY",
                            "ELEMENT_ASSOC" => "CREATED_BY",
                            "ALLOW_EDIT" => "Y",
                            "ALLOW_DELETE" => "Y",
                            "SEF_MODE" => "Y",
                            "SEF_FOLDER" => SITE_DIR . "social/my/"
                        ),
                        false
                    ); ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="resume">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:iblock.element.add.list",
                        "my_board",
                        array(
                            "EDIT_URL" => "job_r/",
                            "NAV_ON_PAGE" => "10",
                            "MAX_USER_ENTRIES" => "100000",
                            "IBLOCK_TYPE" => "job_gorportal",
                            "IBLOCK_ID" => "#JOB_R_IBLOCK_ID#",
                            "GROUPS" => array(
                                0 => "1",
                                1 => "3",
                                2 => "4",
                            ),
                            "STATUS" => "ANY",
                            "ELEMENT_ASSOC" => "CREATED_BY",
                            "ALLOW_EDIT" => "Y",
                            "ALLOW_DELETE" => "Y",
                            "SEF_MODE" => "Y",
                            "SEF_FOLDER" => SITE_DIR . "social/my/"
                        ),
                        false
                    ); ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="auto">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:iblock.element.add.list",
                        "my_board",
                        array(
                            "EDIT_URL" => "auto/",
                            "NAV_ON_PAGE" => "10",
                            "MAX_USER_ENTRIES" => "100000",
                            "IBLOCK_TYPE" => "board_gorportal",
                            "IBLOCK_ID" => "#AUTO_IBLOCK_ID#",
                            "GROUPS" => array(
                                0 => "1",
                                1 => "3",
                                2 => "4",
                            ),
                            "STATUS" => "ANY",
                            "ELEMENT_ASSOC" => "CREATED_BY",
                            "ALLOW_EDIT" => "Y",
                            "ALLOW_DELETE" => "Y",
                            "SEF_MODE" => "Y",
                            "SEF_FOLDER" => SITE_DIR . "social/my/"
                        ),
                        false
                    ); ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="realty">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:iblock.element.add.list",
                        "my_board",
                        array(
                            "EDIT_URL" => "realty/",
                            "NAV_ON_PAGE" => "10",
                            "MAX_USER_ENTRIES" => "100000",
                            "IBLOCK_TYPE" => "board_gorportal",
                            "IBLOCK_ID" => "#REALTY_IBLOCK_ID#",
                            "GROUPS" => array(
                                0 => "1",
                                1 => "3",
                                2 => "4",
                            ),
                            "STATUS" => "ANY",
                            "ELEMENT_ASSOC" => "CREATED_BY",
                            "ALLOW_EDIT" => "Y",
                            "ALLOW_DELETE" => "Y",
                            "SEF_MODE" => "Y",
                            "SEF_FOLDER" => SITE_DIR . "social/my/"
                        ),
                        false
                    ); ?>
                </div>
            </div>


        </div>
        <div class="col-md-3 col-sm-3 hidden-xs">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => SITE_DIR . "include/my_cabinet.php"
                )
            ); ?>
        </div>
    </div>
<? else: ?>
    <br/><h2 class="alert alert-danger"><span
            class="glyphicon glyphicon-warning-sign"></span> У вас нет доступа
        войдите
        на сайт либо зарегистрируйтесь!</h2><br/><br/>
<? endif; ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>