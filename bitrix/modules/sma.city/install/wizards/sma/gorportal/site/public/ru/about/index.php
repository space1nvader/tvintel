<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("О проекте");
?><b style="color: #333333; background-color: #ffffff;">Новый Владивосток</b><span
    style="color: #333333; background-color: #ffffff;">&nbsp;- ведущий региональный интернет-портал Приморского края&nbsp;и города Владивостока.</span>
    <ul style="color: #333333; background-color: #ffffff;">
        <li>Проект существует уже 10 лет и является самым популярным и посещаемым интернет-проектом Приморского края.
        </li>
        <li>Более 40 000 уникальных пользователей в день, более 500 000 просмотров страниц в сутки - такова на сегодня
            статистика проекта.
        </li>
        <li>Ежемесячная аудитория проекта - более 600 000 уникальных пользователей.</li>
        <li>Ежедневно более 300 человек добавляют сайт в "Избранное".</li>
    </ul>
    <i style="color: #333333; background-color: #ffffff;">Подробно ознакомиться со статистикой проекта вы можете&nbsp;здесь.
        Приводим ссылки на независимые счетчики, установленные на страницах сервера:&nbsp;Rambler,&nbsp;Mail.Ru,&nbsp;HotLog.
        Расхождения в показаниях различных счетчиков и собственной статистикой сервера вызваны объективными причинами и
        спецификой Интернета.</i><span style="color: #333333; background-color: #ffffff;"> </span><br
    style="color: #333333; background-color: #ffffff;">
    <br style="color: #333333; background-color: #ffffff;">
    <b style="color: #333333; background-color: #ffffff;">"Новый Владивосток" - наиболее посещаемый портал
        Владивостока</b><span style="color: #333333; background-color: #ffffff;">&nbsp;с самой богатой историей среди региональных проектов. Именно наш портал - на первых местах в результатах поисковых запросов на темы, связанные с Владивостоком. Каждый день на портале - оперативные новости, репортажи, фотоматериалы, живое обсуждение событий.&nbsp;</span>
    <br style="color: #333333; background-color: #ffffff;">
    <br style="color: #333333; background-color: #ffffff;">
    <b style="color: #333333; background-color: #ffffff;">Служба информации "Нового Владивостока"</b><span
    style="color: #333333; background-color: #ffffff;"> </span>освещает<span
    style="color: #333333; background-color: #ffffff;">&nbsp;актуальные проблемы экономико-политической и социальной жизни региона с учетом интереса к ним широких предпринимательских кругов, представителей бизнеса и власти, коммерческих структур и некоммерческих организаций.&nbsp;</span><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>