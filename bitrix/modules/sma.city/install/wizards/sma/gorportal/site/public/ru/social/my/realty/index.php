<?
/**
 * Created by PhpStorm.
 * User: Vladimir Kievskiy
 * Site: www.smart-d.ru
 * Date: 31.01.2015
 * Time: 17:13
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Недвижимость");
?>
<div class="row">
    <div
        class="<? if ($USER->IsAuthorized()): ?>col-md-9 col-sm-9 col-xs-12<? else: ?>col-md-12 col-sm-12 col-xs-12<? endif ?>">
        <? $APPLICATION->IncludeComponent(
            "bitrix:iblock.element.add.form",
            "form_realty_new",
            array(
                "IBLOCK_TYPE" => "board_gorportal",
                "IBLOCK_ID" => "#REALTY_IBLOCK_ID#",
                "STATUS_NEW" => "N",
                "LIST_URL" => SITE_DIR . "social/my/",
                "USE_CAPTCHA" => "Y",
                "USER_MESSAGE_EDIT" => "",
                "USER_MESSAGE_ADD" => "Объявление добавлено",
                "DEFAULT_INPUT_SIZE" => "30",
                "RESIZE_IMAGES" => "N",
                "PROPERTY_CODES" => array(
                    0 => "74",
                    1 => "75",
                    2 => "76",
                    3 => "77",
                    4 => "79",
                    5 => "80",
                    6 => "100",
                    7 => "101",
                    8 => "102",
                    9 => "103",
                    10 => "144",
                    11 => "NAME",
                    12 => "DATE_ACTIVE_TO",
                    13 => "IBLOCK_SECTION",
                    14 => "DETAIL_TEXT",
                ),
                "PROPERTY_CODES_REQUIRED" => array(
                    0 => "75",
                    1 => "77",
                    2 => "79",
                    3 => "101",
                    4 => "NAME",
                ),
                "GROUPS" => array(
                    0 => "2",
                ),
                "STATUS" => "ANY",
                "ELEMENT_ASSOC" => "CREATED_BY",
                "MAX_USER_ENTRIES" => "100000",
                "MAX_LEVELS" => "1",
                "LEVEL_LAST" => "Y",
                "MAX_FILE_SIZE" => "0",
                "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                "SEF_MODE" => "Y",
                "CUSTOM_TITLE_NAME" => "Заголовок",
                "CUSTOM_TITLE_TAGS" => "",
                "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                "CUSTOM_TITLE_IBLOCK_SECTION" => "Раздел",
                "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                "CUSTOM_TITLE_DETAIL_TEXT" => "Дополнительная информация",
                "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                "SEF_FOLDER" => SITE_DIR . "social/my/realty/",
                "COMPONENT_TEMPLATE" => "form_realty_new",
                "PRICE" => "76",
                "MAIL" => "100",
                "PHONE" => "75",
                "MORE_PHOTO" => "74",
                "ADRES" => "79",
                "KOMNAT" => "77",
                "PL" => "80",
                "ETAG" => "102",
                "GOROD" => "101",
                "SOST" => "103"
            ),
            false
        ); ?>
    </div>
    <? if ($USER->IsAuthorized()): ?>
        <div class="col-md-3 col-sm-3 hidden-xs">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => SITE_DIR . "include/my_cabinet.php"
                )
            ); ?>
        </div>
    <? endif ?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
