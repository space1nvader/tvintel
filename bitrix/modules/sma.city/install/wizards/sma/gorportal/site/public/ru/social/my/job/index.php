<?
/**
 * Created by PhpStorm.
 * User: Vladimir Kievskiy
 * Site: www.smart-d.ru
 * Date: 29.01.2015
 * Time: 1:06
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
    <div class="row">
        <div
            class="<? if ($USER->IsAuthorized()): ?>col-md-9 col-sm-9 col-xs-12<? else: ?>col-md-12 col-sm-12 col-xs-12<? endif ?>">
            <? $APPLICATION->IncludeComponent(
                "bitrix:iblock.element.add.form",
                "form_job_new",
                array(
                    "IBLOCK_TYPE" => "job_gorportal",
                    "IBLOCK_ID" => "#JOB_IBLOCK_ID#",
                    "STATUS_NEW" => "N",
                    "LIST_URL" => SITE_DIR . "social/my/",
                    "USE_CAPTCHA" => "Y",
                    "USER_MESSAGE_EDIT" => "Успешно обновлено",
                    "USER_MESSAGE_ADD" => "Успешно добавлено",
                    "DEFAULT_INPUT_SIZE" => "30",
                    "RESIZE_IMAGES" => "Y",
                    "PROPERTY_CODES" => array(
                        0 => "43",
                        1 => "44",
                        2 => "47",
                        3 => "48",
                        4 => "49",
                        5 => "50",
                        6 => "51",
                        7 => "52",
                        8 => "53",
                        9 => "NAME",
                        10 => "DATE_ACTIVE_TO",
                        11 => "IBLOCK_SECTION",
                        12 => "DETAIL_TEXT",
                        13 => "DETAIL_PICTURE",
                    ),
                    "PROPERTY_CODES_REQUIRED" => array(
                        0 => "43",
                        1 => "51",
                        2 => "NAME",
                        3 => "DATE_ACTIVE_TO",
                        4 => "IBLOCK_SECTION",
                        5 => "DETAIL_TEXT",
                    ),
                    "GROUPS" => array(
                        0 => "2",
                    ),
                    "STATUS" => "ANY",
                    "ELEMENT_ASSOC" => "CREATED_BY",
                    "MAX_USER_ENTRIES" => "100000",
                    "MAX_LEVELS" => "1",
                    "LEVEL_LAST" => "Y",
                    "MAX_FILE_SIZE" => "0",
                    "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                    "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                    "SEF_MODE" => "Y",
                    "SEF_FOLDER" => SITE_DIR . "social/my/job/",
                    "CUSTOM_TITLE_NAME" => "Заголовок",
                    "CUSTOM_TITLE_TAGS" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                    "CUSTOM_TITLE_IBLOCK_SECTION" => "Категория",
                    "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                    "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                    "CUSTOM_TITLE_DETAIL_TEXT" => "Дополнительная информация",
                    "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                    "COMPONENT_TEMPLATE" => "form_job_new",
                    "PRICE" => "",
                    "MAIL" => "50",
                    "PHONE" => "43",
                    "MORE_PHOTO" => "",
                    "ZP" => "44",
                    "ZANAT" => "47",
                    "OPLAT" => "48",
                    "OBRAZ" => "52",
                    "OPIT" => "53",
                    "KOMP" => "49",
                    "VAKANS" => "51"
                ),
                false
            ); ?>
        </div>
        <? if ($USER->IsAuthorized()): ?>
            <div class="col-md-3 col-sm-3 hidden-xs">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/my_cabinet.php"
                    )
                ); ?>
            </div>
        <? endif ?>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>