<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Добавить компанию г.Сочи - Sochi-N.ru");
$APPLICATION->SetPageProperty("keywords", "Добавить компанию г.Сочи - Sochi-N.ru");
$APPLICATION->SetPageProperty("description", "Добавить компанию г.Сочи - Sochi-N.ru");
$APPLICATION->SetTitle("Добавить компанию");
?>
    <div class="row">
        <div
            class="<? if ($USER->IsAuthorized()): ?>col-md-9 col-sm-9 col-xs-12<? else: ?>col-md-12 col-sm-12 col-xs-12<? endif ?>">
            <? $APPLICATION->IncludeComponent(
                "bitrix:iblock.element.add.form",
                "form_new",
                array(
                    "IBLOCK_TYPE" => "service",
                    "IBLOCK_ID" => "#CATALOG_IBLOCK_ID#",
                    "STATUS_NEW" => "N",
                    "LIST_URL" => SITE_DIR . "social/my/catalog/",
                    "USE_CAPTCHA" => "Y",
                    "USER_MESSAGE_EDIT" => "",
                    "USER_MESSAGE_ADD" => "Компания успешно добавлена",
                    "DEFAULT_INPUT_SIZE" => "30",
                    "RESIZE_IMAGES" => "N",
                    "PROPERTY_CODES" => array(
                        0 => "4",
                        1 => "5",
                        2 => "6",
                        3 => "7",
                        4 => "8",
                        5 => "138",
                        6 => "NAME",
                        7 => "IBLOCK_SECTION",
                        8 => "DETAIL_TEXT",
                        9 => "DETAIL_PICTURE",
                    ),
                    "PROPERTY_CODES_REQUIRED" => array(
                        0 => "6",
                        1 => "8",
                        2 => "NAME",
                        3 => "IBLOCK_SECTION",
                        4 => "DETAIL_TEXT",
                    ),
                    "GROUPS" => array(
                        0 => "2",
                    ),
                    "STATUS" => "ANY",
                    "ELEMENT_ASSOC" => "CREATED_BY",
                    "MAX_USER_ENTRIES" => "1000",
                    "MAX_LEVELS" => "1",
                    "LEVEL_LAST" => "Y",
                    "MAX_FILE_SIZE" => "0",
                    "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                    "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                    "SEF_MODE" => "Y",
                    "CUSTOM_TITLE_NAME" => "Название компании",
                    "CUSTOM_TITLE_TAGS" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                    "CUSTOM_TITLE_IBLOCK_SECTION" => "Категория",
                    "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                    "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                    "CUSTOM_TITLE_DETAIL_TEXT" => "Описание компании",
                    "CUSTOM_TITLE_DETAIL_PICTURE" => "Логотип",
                    "SEF_FOLDER" => SITE_DIR . "social/my/catalog/",
                    "COMPONENT_TEMPLATE" => "form_new",
                    "TIME_WORK" => "138",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "USE_SHARE" => "N",
                    "ADRESS" => "8",
                    "MAIL" => "7",
                    "SITE" => "5",
                    "PHONE" => "6",
                    "MORE_PHOTO" => "4"
                ),
                false
            ); ?>
        </div>
        <? if ($USER->IsAuthorized()): ?>
            <div class="col-md-3 col-sm-3 hidden-xs">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/my_cabinet.php"
                    )
                ); ?>
            </div>
        <? endif ?>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>