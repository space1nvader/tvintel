<?
/**
 * Created by PhpStorm.
 * User: Vladimir Kievskiy
 * Site: www.smart-d.ru
 * Date: 29.01.2015
 * Time: 1:06
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Резюме");
?>
<div class="row">
    <div class="col-md-9 col-sm-9 col-xs-12">
        <? $APPLICATION->IncludeComponent(
            "bitrix:iblock.element.add.form",
            "form_critik",
            array(
                "IBLOCK_TYPE" => "job_gorportal",
                "IBLOCK_ID" => "#JOB_R_IBLOCK_ID#",
                "STATUS_NEW" => "N",
                "LIST_URL" => SITE_DIR . "social/my/",
                "USE_CAPTCHA" => "N",
                "USER_MESSAGE_EDIT" => "Успешно обновлено",
                "USER_MESSAGE_ADD" => "Успешно добавлено",
                "DEFAULT_INPUT_SIZE" => "30",
                "RESIZE_IMAGES" => "Y",
                "PROPERTY_CODES" => array(
                    0 => "54",
                    1 => "55",
                    2 => "56",
                    3 => "57",
                    4 => "58",
                    5 => "59",
                    6 => "60",
                    7 => "61",
                    8 => "62",
                    9 => "63",
                    10 => "NAME",
                    11 => "DATE_ACTIVE_TO",
                    12 => "IBLOCK_SECTION",
                    13 => "DETAIL_TEXT",
                ),
                "PROPERTY_CODES_REQUIRED" => array(
                    0 => "56",
                    1 => "61",
                    2 => "63",
                    3 => "NAME",
                    4 => "DATE_ACTIVE_TO",
                    5 => "IBLOCK_SECTION",
                    6 => "DETAIL_TEXT",
                ),
                "GROUPS" => array(
                    0 => "1",
                    1 => "3",
                    2 => "4",
                ),
                "STATUS" => "ANY",
                "ELEMENT_ASSOC" => "CREATED_BY",
                "MAX_USER_ENTRIES" => "100000",
                "MAX_LEVELS" => "1",
                "LEVEL_LAST" => "Y",
                "MAX_FILE_SIZE" => "0",
                "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                "SEF_MODE" => "Y",
                "SEF_FOLDER" => SITE_DIR . "social/my/job_r/",
                "CUSTOM_TITLE_NAME" => "Заголовок",
                "CUSTOM_TITLE_TAGS" => "",
                "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                "CUSTOM_TITLE_IBLOCK_SECTION" => "Категория",
                "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                "CUSTOM_TITLE_DETAIL_TEXT" => "Дополнительная информация",
                "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                "COMPONENT_TEMPLATE" => "form_critik"
            ),
            false
        ); ?>
    </div>
    <div class="col-md-3 col-sm-3 hidden-xs">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => SITE_DIR . "include/my_cabinet.php"
            )
        ); ?>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
