<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Добавить авто");
?>
    <div class="row">
        <div
            class="<? if ($USER->IsAuthorized()): ?>col-md-9 col-sm-9 col-xs-12<? else: ?>col-md-12 col-sm-12 col-xs-12<? endif ?>">
            <? $APPLICATION->IncludeComponent(
                "bitrix:iblock.element.add.form",
                "form_auto_new",
                array(
                    "IBLOCK_TYPE" => "board_gorportal",
                    "IBLOCK_ID" => "#AUTO_IBLOCK_ID#",
                    "STATUS_NEW" => "N",
                    "LIST_URL" => SITE_DIR . "social/my/",
                    "USE_CAPTCHA" => "Y",
                    "USER_MESSAGE_EDIT" => "",
                    "USER_MESSAGE_ADD" => "Объявление добавлено",
                    "DEFAULT_INPUT_SIZE" => "30",
                    "RESIZE_IMAGES" => "N",
                    "PROPERTY_CODES" => array(
                        0 => "16",
                        1 => "17",
                        2 => "18",
                        3 => "19",
                        4 => "20",
                        5 => "21",
                        6 => "22",
                        7 => "23",
                        8 => "24",
                        9 => "26",
                        10 => "27",
                        11 => "115",
                        12 => "116",
                        13 => "120",
                        14 => "NAME",
                        15 => "IBLOCK_SECTION",
                        16 => "DETAIL_TEXT",
                    ),
                    "PROPERTY_CODES_REQUIRED" => array(
                        0 => "17",
                        1 => "18",
                        2 => "19",
                        3 => "115",
                        4 => "120",
                        5 => "NAME",
                        6 => "IBLOCK_SECTION",
                    ),
                    "GROUPS" => array(
                        0 => "2",
                    ),
                    "STATUS" => "ANY",
                    "ELEMENT_ASSOC" => "CREATED_BY",
                    "MAX_USER_ENTRIES" => "100000",
                    "MAX_LEVELS" => "1",
                    "LEVEL_LAST" => "Y",
                    "MAX_FILE_SIZE" => "0",
                    "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                    "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                    "SEF_MODE" => "Y",
                    "CUSTOM_TITLE_NAME" => "Заголовок",
                    "CUSTOM_TITLE_TAGS" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                    "CUSTOM_TITLE_IBLOCK_SECTION" => "Раздел",
                    "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                    "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                    "CUSTOM_TITLE_DETAIL_TEXT" => "Дополнительная информация",
                    "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                    "SEF_FOLDER" => SITE_DIR . "social/my/auto/",
                    "COMPONENT_TEMPLATE" => "form_auto_new",
                    "PRICE" => "18",
                    "MAIL" => "19",
                    "PHONE" => "17",
                    "MORE_PHOTO" => "16",
                    "GOROD" => "120",
                    "GOD" => "115",
                    "PROBEG" => "22",
                    "RUL" => "23",
                    "DVIG" => "26",
                    "POWER" => "116",
                    "OBEM" => "27",
                    "PRIVOD" => "21",
                    "KPP" => "20"
                ),
                false
            ); ?>
        </div>
        <? if ($USER->IsAuthorized()): ?>
            <div class="col-md-3 col-sm-3 hidden-xs">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/my_cabinet.php"
                    )
                ); ?>
            </div>
        <? endif ?>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>