<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Продажа авто");
?>
    <div class="row">
    <div class="col-md-9 col-sm-9 col-xs-12">
        <? $APPLICATION->IncludeComponent(
            "bitrix:news",
            "auto_board",
            array(
                "IBLOCK_TYPE" => "board_gorportal",
                "IBLOCK_ID" => "#AUTO_IBLOCK_ID#",
                "NEWS_COUNT" => "20",
                "USE_SEARCH" => "N",
                "USE_RSS" => "N",
                "USE_RATING" => "N",
                "USE_CATEGORIES" => "N",
                "USE_REVIEW" => "N",
                "USE_FILTER" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "SORT",
                "SORT_ORDER2" => "ASC",
                "CHECK_DATES" => "Y",
                "SEF_MODE" => "Y",
                "AJAX_MODE" => "Y",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "Y",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "SET_STATUS_404" => "Y",
                "SET_TITLE" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "ADD_ELEMENT_CHAIN" => "N",
                "USE_PERMISSIONS" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "USE_SHARE" => "N",
                "PREVIEW_TRUNCATE_LEN" => "",
                "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                "LIST_FIELD_CODE" => array(
                    0 => "",
                    1 => "undefined",
                    2 => "",
                ),
                "LIST_PROPERTY_CODE" => array(
                    0 => "GOD",
                    1 => "OBEMDVIG",
                    2 => "PROBEG",
                    3 => "TIPDVIG",
                    4 => "TRANSMIS",
                    5 => "PRICE",
                    6 => "undefined",
                    7 => "",
                ),
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "DISPLAY_NAME" => "Y",
                "META_KEYWORDS" => "-",
                "META_DESCRIPTION" => "-",
                "BROWSER_TITLE" => "-",
                "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                "DETAIL_FIELD_CODE" => array(
                    0 => "CREATED_BY",
                    1 => "undefined",
                    2 => "",
                ),
                "DETAIL_PROPERTY_CODE" => array(
                    0 => "EMAIL",
                    1 => "GOD",
                    2 => "OBMEN",
                    3 => "OBEMDVIG",
                    4 => "PRIVOD",
                    5 => "PROBEG",
                    6 => "RUL",
                    7 => "PHONE",
                    8 => "TIPDVIG",
                    9 => "TRANSMIS",
                    10 => "PRICE",
                    11 => "MORE_PHOTO",
                    12 => "",
                ),
                "DETAIL_DISPLAY_TOP_PAGER" => "N",
                "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                "DETAIL_PAGER_TITLE" => "Страница",
                "DETAIL_PAGER_TEMPLATE" => "",
                "DETAIL_PAGER_SHOW_ALL" => "Y",
                "PAGER_TEMPLATE" => "infinity",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "SEF_FOLDER" => "#SITE_DIR#auto/board/",
                "AJAX_OPTION_ADDITIONAL" => "",
                "SEF_URL_TEMPLATES" => array(
                    "news" => "",
                    "section" => "#SECTION_ID#/",
                    "detail" => "#SECTION_ID#/#ELEMENT_ID#/",
                )
            ),
            false
        ); ?>
    </div>
    <div class="col-md-3 col-sm-3 hidden-xs">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "include",
            array(
                "AREA_FILE_SHOW" => "sect",
                "EDIT_TEMPLATE" => "",
                "AREA_FILE_SUFFIX" => "inc",
                "AREA_FILE_RECURSIVE" => "Y"
            ),
            false
        ); ?>
    </div>
    </div><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>