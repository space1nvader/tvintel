<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Работа в городе");
?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <? $APPLICATION->IncludeComponent("bitrix:menu", "submenu", Array(
                "ROOT_MENU_TYPE" => "submenu",    // Тип меню для первого уровня
                "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                "MAX_LEVEL" => "1",    // Уровень вложенности меню
                "CHILD_MENU_TYPE" => "",    // Тип меню для остальных уровней
                "USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
                "DELAY" => "N",    // Откладывать выполнение шаблона меню
                "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
            ),
                false
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section.list",
                        "job_section_homev",
                        array(
                            "IBLOCK_TYPE" => "job_gorportal",
                            "IBLOCK_ID" => "#JOB_IBLOCK_ID#",
                            "SECTION_ID" => "",
                            "SECTION_CODE" => "",
                            "COUNT_ELEMENTS" => "Y",
                            "TOP_DEPTH" => "1",
                            "SECTION_FIELDS" => array(
                                0 => "",
                                1 => "",
                            ),
                            "SECTION_USER_FIELDS" => array(
                                0 => "",
                                1 => "",
                            ),
                            "SECTION_URL" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_GROUPS" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "VIEW_MODE" => "TEXT",
                            "SHOW_PARENT_NAME" => "Y"
                        ),
                        false
                    ); ?>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "job_section_homer", Array(
                        "IBLOCK_TYPE" => "job_gorportal",    // Тип инфоблока
                        "IBLOCK_ID" => "#JOB_R_IBLOCK_ID#",    // Инфоблок
                        "SECTION_ID" => "",    // ID раздела
                        "SECTION_CODE" => "",    // Код раздела
                        "COUNT_ELEMENTS" => "Y",    // Показывать количество элементов в разделе
                        "TOP_DEPTH" => "2",    // Максимальная отображаемая глубина разделов
                        "SECTION_FIELDS" => array(    // Поля разделов
                            0 => "PICTURE",
                            1 => "",
                        ),
                        "SECTION_USER_FIELDS" => array(    // Свойства разделов
                            0 => "",
                            1 => "",
                        ),
                        "SECTION_URL" => "",    // URL, ведущий на страницу с содержимым раздела
                        "CACHE_TYPE" => "A",    // Тип кеширования
                        "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                        "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                        "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
                        "VIEW_MODE" => "TEXT",
                        "SHOW_PARENT_NAME" => "Y"
                    ),
                        false
                    ); ?>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 hidden-xs">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "include",
                array(
                    "AREA_FILE_SHOW" => "sect",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_SUFFIX" => "inc",
                    "AREA_FILE_RECURSIVE" => "Y"
                ),
                false
            ); ?>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>