<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Мои рецензии");
?>
<? if ($USER->IsAuthorized()): ?>
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12">
            <? $APPLICATION->IncludeComponent(
                "bitrix:iblock.element.add.list",
                "my_critic",
                array(
                    "EDIT_URL" => "add.php",
                    "NAV_ON_PAGE" => "20",
                    "MAX_USER_ENTRIES" => "100000",
                    "IBLOCK_TYPE" => "afisha_gorportal",
                    "IBLOCK_ID" => "#KRITIK_IBLOCK_ID#",
                    "GROUPS" => array(
                        0 => "1",
                        1 => "3",
                        2 => "4",
                    ),
                    "STATUS" => "ANY",
                    "ELEMENT_ASSOC" => "CREATED_BY",
                    "ALLOW_EDIT" => "Y",
                    "ALLOW_DELETE" => "Y",
                    "SEF_MODE" => "Y",
                    "SEF_FOLDER" => SITE_DIR . "social/my/critik/",
                    "COMPONENT_TEMPLATE" => "my_critic"
                ),
                false
            ); ?>
        </div>
        <div class="col-md-3 col-sm-3 hidden-xs">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => SITE_DIR . "include/my_cabinet.php"
                )
            ); ?>
        </div>
    </div>
<? else: ?>
    <br/><h2 class="alert alert-danger"><span
            class="glyphicon glyphicon-warning-sign"></span> У вас нет доступа
        войдите
        на сайт либо зарегистрируйтесь!</h2><br/><br/>
<? endif; ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>