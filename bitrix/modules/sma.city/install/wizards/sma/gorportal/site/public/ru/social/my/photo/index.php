<?
/**
 * Created by PhpStorm.
 * User: Vladimir Kievskiy
 * Site: www.smart-d.ru
 * Date: 29.01.2015
 * Time: 1:06
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
<div class="row">
    <div class="col-md-9 col-sm-9 col-xs-12">
        <? $APPLICATION->IncludeComponent(
            "bitrix:iblock.element.add.list", "my_catalog", Array(
            "EDIT_URL" => "add/",
            // Страница редактирования элемента
            "NAV_ON_PAGE" => "10",
            // Количество элементов на странице
            "MAX_USER_ENTRIES" => "100000",
            // Ограничить кол-во элементов для одного пользователя
            "IBLOCK_TYPE" => "service",    // Тип инфоблока
            "IBLOCK_ID" => "#PHOTO_IBLOCK_ID#",    // Инфоблок
            "GROUPS" => array(    // Группы пользователей, имеющие право на добавление/редактирование
                0 => "1",
                1 => "3",
                2 => "4",
            ),
            "STATUS" => "ANY",
            // Редактирование возможно
            "ELEMENT_ASSOC" => "CREATED_BY",
            // Привязка к пользователю
            "ALLOW_EDIT" => "Y",
            // Разрешать редактирование
            "ALLOW_DELETE" => "Y",    // Разрешать удаление
            "SEF_MODE" => "Y",    // Включить поддержку ЧПУ
            "SEF_FOLDER" => SITE_DIR . "social/my/photo/",
            // Каталог ЧПУ (относительно корня сайта)
        ),
            false
        ); ?>

    </div>
    <div class="col-md-3 col-sm-3 hidden-xs">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => SITE_DIR . "include/my_cabinet.php"
            )
        ); ?>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
