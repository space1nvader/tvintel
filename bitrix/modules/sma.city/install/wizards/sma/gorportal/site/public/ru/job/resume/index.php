<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Банк вакансий");
?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:menu", "submenu", Array(
                    "ROOT_MENU_TYPE" => "submenu",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => "",
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                )
            );
            ?>
        </div>
    </div>

<?
$APPLICATION->IncludeComponent(
    "bitrix:news", "job_r", array(
    "IBLOCK_TYPE" => "job_gorportal",
    "IBLOCK_ID" => "#JOB_R_IBLOCK_ID#",
    "NEWS_COUNT" => "20",
    "USE_SEARCH" => "Y",
    "USE_RSS" => "N",
    "USE_RATING" => "N",
    "USE_CATEGORIES" => "Y",
    "CATEGORY_IBLOCK" => array(
        0 => "8",
    ),
    "CATEGORY_CODE" => "CATEGORY",
    "CATEGORY_ITEMS_COUNT" => "5",
    "CATEGORY_THEME_8" => "list",
    "USE_REVIEW" => "Y",
    "MESSAGES_PER_PAGE" => "10",
    "USE_CAPTCHA" => "Y",
    "REVIEW_AJAX_POST" => "Y",
    "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
    "FORUM_ID" => "#FORUM_ID#",
    "URL_TEMPLATES_READ" => "",
    "SHOW_LINK_TO_FORUM" => "N",
    "USE_FILTER" => "N",
    "SORT_BY1" => "ACTIVE_FROM",
    "SORT_ORDER1" => "DESC",
    "SORT_BY2" => "SORT",
    "SORT_ORDER2" => "ASC",
    "CHECK_DATES" => "Y",
    "SEF_MODE" => "Y",
    "SEF_FOLDER" => "#SITE_DIR#job/resume/",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "SET_STATUS_404" => "Y",
    "SET_TITLE" => "Y",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
    "ADD_SECTIONS_CHAIN" => "Y",
    "ADD_ELEMENT_CHAIN" => "N",
    "USE_PERMISSIONS" => "N",
    "PREVIEW_TRUNCATE_LEN" => "100",
    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
    "LIST_FIELD_CODE" => array(
        0 => "DETAIL_PICTURE",
        1 => "SHOW_COUNTER",
        2 => "",
    ),
    "LIST_PROPERTY_CODE" => array(
        0 => "ZP",
        1 => "",
    ),
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "DISPLAY_NAME" => "Y",
    "META_KEYWORDS" => "-",
    "META_DESCRIPTION" => "-",
    "BROWSER_TITLE" => "NAME",
    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
    "DETAIL_FIELD_CODE" => array(
        0 => "CREATED_BY",
        1 => "",
    ),
    "DETAIL_PROPERTY_CODE" => array(
        0 => "EMAIL",
        1 => "GODROGHDEN",
        2 => "DOPNAV",
        3 => "ZANAT",
        4 => "OSEBE",
        5 => "OBRAZOV",
        6 => "OPIT",
        7 => "SEMYA",
        8 => "PHONE",
        9 => "ZP",
        10 => "",
    ),
    "DETAIL_DISPLAY_TOP_PAGER" => "N",
    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
    "DETAIL_PAGER_TITLE" => "Страница",
    "DETAIL_PAGER_TEMPLATE" => "",
    "DETAIL_PAGER_SHOW_ALL" => "N",
    "PAGER_TEMPLATE" => "infinity",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "USE_SHARE" => "Y",
    "SHARE_HIDE" => "N",
    "SHARE_TEMPLATE" => "",
    "SHARE_HANDLERS" => array(
        0 => "twitter",
        1 => "lj",
        2 => "mailru",
        3 => "facebook",
        4 => "delicious",
        5 => "vk",
    ),
    "SHARE_SHORTEN_URL_LOGIN" => "",
    "SHARE_SHORTEN_URL_KEY" => "",
    "AJAX_OPTION_ADDITIONAL" => "",
    "SEF_URL_TEMPLATES" => array(
        "news" => "",
        "section" => "#SECTION_ID#/",
        "detail" => "#SECTION_ID#/#ELEMENT_ID#/",
        "search" => "search/",
    )
), false
);
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>