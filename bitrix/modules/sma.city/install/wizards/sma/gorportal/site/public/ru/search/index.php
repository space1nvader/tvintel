<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
require_once $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/studio7.gorportal/param.php";
$APPLICATION->SetTitle("Поиск по сайту");
?><? if ($main && $license) {
    $APPLICATION->IncludeComponent(
        "bitrix:search.page",
        ".default",
        array(
            "AJAX_MODE" => "Y",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "Y",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "A",
            "CHECK_DATES" => "Y",
            "COMPONENT_TEMPLATE" => ".default",
            "DEFAULT_SORT" => "rank",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "DISPLAY_TOP_PAGER" => "Y",
            "FILTER_NAME" => "",
            "NAME_TEMPLATE" => "",
            "NO_WORD_LOGIC" => "Y",
            "PAGER_SHOW_ALWAYS" => "Y",
            "PAGER_TEMPLATE" => "",
            "PAGER_TITLE" => "Результаты поиска",
            "PAGE_RESULT_COUNT" => "50",
            "PATH_TO_SONET_MESSAGES_CHAT" => "/company/personal/messages/chat/#USER_ID#/",
            "RESTART" => "N",
            "SHOW_LOGIN" => "Y",
            "SHOW_WHEN" => "N",
            "SHOW_WHERE" => "Y",
            "STRUCTURE_FILTER" => "structure",
            "USE_LANGUAGE_GUESS" => "Y",
            "USE_SUGGEST" => "Y",
            "USE_TITLE_RANK" => "Y",
            "arrFILTER" => array(
                0 => "no",
            ),
            "arrWHERE" => array(
                0 => "blog",
            ),
            "SHOW_ITEM_TAGS" => "Y",
            "TAGS_INHERIT" => "Y",
            "SHOW_ITEM_DATE_CHANGE" => "Y",
            "SHOW_ORDER_BY" => "Y",
            "SHOW_TAGS_CLOUD" => "N",
            "TAGS_SORT" => "NAME",
            "TAGS_PAGE_ELEMENTS" => "150",
            "TAGS_PERIOD" => "",
            "TAGS_URL_SEARCH" => "",
            "FONT_MAX" => "50",
            "FONT_MIN" => "10",
            "COLOR_NEW" => "000000",
            "COLOR_OLD" => "C8C8C8",
            "PERIOD_NEW_TAGS" => "",
            "SHOW_CHAIN" => "Y",
            "COLOR_TYPE" => "Y",
            "WIDTH" => "100%"
        ),
        false
    );
} ?><br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>