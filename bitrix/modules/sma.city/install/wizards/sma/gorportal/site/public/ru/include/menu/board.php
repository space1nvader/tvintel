<div class="row">
    <div class="col-md-8">
        <? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "board_section_menu", Array(
            "IBLOCK_TYPE" => "board_gorportal",    // Тип инфоблока
            "IBLOCK_ID" => "5",    // Инфоблок
            "SECTION_ID" => "",    // ID раздела
            "SECTION_CODE" => "",    // Код раздела
            "COUNT_ELEMENTS" => "N",    // Показывать количество элементов в разделе
            "TOP_DEPTH" => "1",    // Максимальная отображаемая глубина разделов
            "SECTION_FIELDS" => array(    // Поля разделов
                0 => "",
                1 => "",
            ),
            "SECTION_USER_FIELDS" => array(    // Свойства разделов
                0 => "",
                1 => "",
            ),
            "SECTION_URL" => "",    // URL, ведущий на страницу с содержимым раздела
            "CACHE_TYPE" => "A",    // Тип кеширования
            "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
            "CACHE_GROUPS" => "Y",    // Учитывать права доступа
            "ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
            "VIEW_MODE" => "TEXT",
            "SHOW_PARENT_NAME" => "Y"
        ),
            false
        ); ?>
    </div>
    <div class="col-md-4 col-sm-4">
        <div style="margin-top:20px;">
            <a class="btn-outline-green btn-sm" href="<?= SITE_DIR ?>social/my/">Мои объявления</a><br/><br/>
            <a class="btn-outline-red btn-sm" href="<?= SITE_DIR ?>social/my/board/?edit=Y">Добавить объявление</a>
        </div>
    </div>
</div>