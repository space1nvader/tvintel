<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arServices = Array(

    'main' => Array(
        'NAME' => GetMessage("SERVICE_MAIN_SETTINGS"),
        'STAGES' => Array(
            "files.php",
            "template.php",
            "settings.php",
        ),
    ),
    "iblock" => array(
        "NAME" => GetMessage("SERVICE_IBLOCK"),
        "STAGES" => array(
            "types.php",
            "news.php",
            "catalog.php",
            "afisha.php",
            "auto.php",
            "board.php",
            "fotootchet.php",
            "job.php",
            "job_r.php",
            "kritik.php",
            "photo.php",
            "realty.php",
            "seans.php",
            "test-drive.php",
        ),
    ),
    "forum" => Array(
        "NAME" => GetMessage("SERVICE_FORUM"),
    ),
);
?>
