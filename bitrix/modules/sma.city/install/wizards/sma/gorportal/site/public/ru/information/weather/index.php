<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Прогноз погоды");
?>
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12">
            <? $APPLICATION->IncludeComponent(
                "studio7:ywheather",
                "detalise",
                array(
                    "LANG_CHARSET" => "AUTO",
                    "COUNTRY" => "Rossiya",
                    "CITY" => "31960",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "DATE_FORMAT" => "d.m.Y"
                ),
                false
            ); ?>
        </div>
        <div class="col-md-3 col-sm-3 hidden-xs">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "include",
                array(
                    "AREA_FILE_SHOW" => "sect",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_SUFFIX" => "inc",
                    "AREA_FILE_RECURSIVE" => "Y"
                ),
                false
            ); ?>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>