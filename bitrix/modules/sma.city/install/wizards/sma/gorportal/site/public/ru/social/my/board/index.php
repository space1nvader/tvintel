<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Добавить объявление");
?>
    <div class="row">
        <div
            class="<? if ($USER->IsAuthorized()): ?>col-md-9 col-sm-9 col-xs-12<? else: ?>col-md-12 col-sm-12 col-xs-12<? endif ?>">
            <? $APPLICATION->IncludeComponent(
                "bitrix:iblock.element.add.form",
                "form_board_new",
                array(
                    "IBLOCK_TYPE" => "board_gorportal",
                    "IBLOCK_ID" => "#BOARD_IBLOCK_ID#",
                    "STATUS_NEW" => "N",
                    "LIST_URL" => SITE_DIR . "social/my/",
                    "USE_CAPTCHA" => "Y",
                    "USER_MESSAGE_EDIT" => "Успешно обновлено",
                    "USER_MESSAGE_ADD" => "Успешно добавлено",
                    "DEFAULT_INPUT_SIZE" => "30",
                    "RESIZE_IMAGES" => "Y",
                    "PROPERTY_CODES" => array(
                        0 => "28",
                        1 => "29",
                        2 => "32",
                        3 => "37",
                        4 => "NAME",
                        5 => "DATE_ACTIVE_TO",
                        6 => "IBLOCK_SECTION",
                        7 => "DETAIL_TEXT",
                        8 => "DETAIL_PICTURE",
                    ),
                    "PROPERTY_CODES_REQUIRED" => array(
                        0 => "32",
                        1 => "NAME",
                        2 => "IBLOCK_SECTION",
                    ),
                    "GROUPS" => array(
                        0 => "2",
                    ),
                    "STATUS" => "ANY",
                    "ELEMENT_ASSOC" => "CREATED_BY",
                    "MAX_USER_ENTRIES" => "100000",
                    "MAX_LEVELS" => "1",
                    "LEVEL_LAST" => "Y",
                    "MAX_FILE_SIZE" => "0",
                    "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                    "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                    "SEF_MODE" => "Y",
                    "SEF_FOLDER" => SITE_DIR . "social/my/board/",
                    "CUSTOM_TITLE_NAME" => "Заголовок",
                    "CUSTOM_TITLE_TAGS" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                    "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                    "CUSTOM_TITLE_IBLOCK_SECTION" => "Категория",
                    "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                    "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                    "CUSTOM_TITLE_DETAIL_TEXT" => "Дополнительная информация",
                    "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                    "COMPONENT_TEMPLATE" => "form_board_new",
                    "TIME_WORK" => "",
                    "ADRESS" => "",
                    "MAIL" => "37",
                    "SITE" => "",
                    "PHONE" => "32",
                    "MORE_PHOTO" => "28",
                    "PRICE" => "29"
                ),
                false
            ); ?>
        </div>
        <? if ($USER->IsAuthorized()): ?>
            <div class="col-md-3 col-sm-3 hidden-xs">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR . "include/my_cabinet.php"
                    )
                ); ?>
            </div>
        <? endif ?>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>