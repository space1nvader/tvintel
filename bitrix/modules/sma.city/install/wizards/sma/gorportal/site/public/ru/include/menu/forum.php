<div class="row">
    <div class="col-md-7">
        <h4 class="hidden-xs"
            style="padding-top:5px;border-bottom: 3px solid rgba(0, 0, 0, 0.1);text-transform: uppercase;margin:3px;color:#959ba6;font-size: 14px;font-weight: bold;">
            Новые обсуждения</h4>
        <? $APPLICATION->IncludeComponent(
            "bitrix:forum.topic.last",
            "menu_forum",
            array(
                "FID" => array(
                    0 => "4",
                    1 => "3",
                    2 => "5",
                ),
                "SHOW_FORUM_ANOTHER_SITE" => "N",
                "SORT_BY" => "LAST_POST_DATE",
                "SORT_ORDER" => "DESC",
                "SORT_BY_SORT_FIRST" => "N",
                "URL_TEMPLATES_INDEX" => SITE_DIR . "forum/",
                "URL_TEMPLATES_LIST" => "#FID#/",
                "URL_TEMPLATES_READ" => "#FID#/#TITLE_SEO#/",
                "URL_TEMPLATES_MESSAGE" => SITE_DIR . "forum/#FID#/#TITLE_SEO#/",
                "URL_TEMPLATES_PROFILE_VIEW" => SITE_DIR . "social/users/#UID#/",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "0",
                "DATE_TIME_FORMAT" => "d.m.y G:i",
                "SET_TITLE" => "N",
                "SEPARATE" => "в форуме #FORUM#",
                "SHOW_COLUMNS" => array(
                    0 => "USER_START_NAME",
                    1 => "VIEWS",
                ),
                "SHOW_SORTING" => "N",
                "SET_NAVIGATION" => "N",
                "TOPICS_PER_PAGE" => "10"
            ),
            false
        ); ?>
    </div>
</div>
<br>