<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Недвижимость");
?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu", "submenu", Array(
                "ROOT_MENU_TYPE" => "submenu",
                // Тип меню для первого уровня
                "MENU_CACHE_TYPE" => "N",    // Тип кеширования
                "MENU_CACHE_TIME" => "3600",
                // Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
                "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
                "MAX_LEVEL" => "1",    // Уровень вложенности меню
                "CHILD_MENU_TYPE" => "",
                // Тип меню для остальных уровней
                "USE_EXT" => "N",
                // Подключать файлы с именами вида .тип_меню.menu_ext.php
                "DELAY" => "N",
                // Откладывать выполнение шаблона меню
                "ALLOW_MULTI_SELECT" => "N",
                // Разрешить несколько активных пунктов одновременно
            ),
                false
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <? $APPLICATION->IncludeComponent(
                "bitrix:news",
                "realty_board",
                array(
                    "IBLOCK_TYPE" => "board_gorportal",
                    "IBLOCK_ID" => "#REALTY_IBLOCK_ID#",
                    "NEWS_COUNT" => "10",
                    "USE_SEARCH" => "N",
                    "USE_RSS" => "N",
                    "USE_RATING" => "N",
                    "USE_CATEGORIES" => "N",
                    "USE_REVIEW" => "Y",
                    "USE_FILTER" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "DESC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "CHECK_DATES" => "Y",
                    "SEF_MODE" => "Y",
                    "SEF_FOLDER" => "#SITE_DIR#realty/",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "SET_STATUS_404" => "Y",
                    "SET_TITLE" => "Y",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "ADD_ELEMENT_CHAIN" => "N",
                    "USE_PERMISSIONS" => "N",
                    "PREVIEW_TRUNCATE_LEN" => "100",
                    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "LIST_FIELD_CODE" => array(
                        0 => "SHOW_COUNTER",
                        1 => "",
                    ),
                    "LIST_PROPERTY_CODE" => array(
                        0 => "PRICE",
                        1 => "KARTA",
                        2 => "ADRESS",
                        3 => "",
                    ),
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "DISPLAY_NAME" => "Y",
                    "META_KEYWORDS" => "-",
                    "META_DESCRIPTION" => "-",
                    "BROWSER_TITLE" => "NAME",
                    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "DETAIL_FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "DETAIL_PROPERTY_CODE" => array(
                        0 => "ADRESS",
                        1 => "KOMNATA",
                        2 => "KARTA",
                        3 => "PLOSHAD",
                        4 => "PHONE",
                        5 => "PRICE",
                        6 => "MORE_PHOTO",
                        7 => "",
                    ),
                    "DETAIL_DISPLAY_TOP_PAGER" => "N",
                    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                    "DETAIL_PAGER_TITLE" => "Страница",
                    "DETAIL_PAGER_TEMPLATE" => "",
                    "DETAIL_PAGER_SHOW_ALL" => "N",
                    "PAGER_TEMPLATE" => "visual",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "USE_SHARE" => "Y",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "MESSAGES_PER_PAGE" => "10",
                    "USE_CAPTCHA" => "Y",
                    "REVIEW_AJAX_POST" => "Y",
                    "PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
                    "FORUM_ID" => "#FORUM_ID#",
                    "URL_TEMPLATES_READ" => "",
                    "SHOW_LINK_TO_FORUM" => "N",
                    "SHARE_HIDE" => "N",
                    "SHARE_TEMPLATE" => "",
                    "SHARE_HANDLERS" => array(
                        0 => "twitter",
                        1 => "lj",
                        2 => "mailru",
                        3 => "facebook",
                        4 => "delicious",
                        5 => "vk",
                    ),
                    "SHARE_SHORTEN_URL_LOGIN" => "",
                    "SHARE_SHORTEN_URL_KEY" => "",
                    "CATEGORY_IBLOCK" => array(
                        0 => "1",
                    ),
                    "CATEGORY_CODE" => "CATEGORY",
                    "CATEGORY_ITEMS_COUNT" => "5",
                    "CATEGORY_THEME_1" => "photo",
                    "SEF_URL_TEMPLATES" => array(
                        "news" => "",
                        "section" => "#SECTION_CODE#/",
                        "detail" => "#SECTION_CODE#/#ELEMENT_CODE#/",
                    )
                ),
                false
            ); ?>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>