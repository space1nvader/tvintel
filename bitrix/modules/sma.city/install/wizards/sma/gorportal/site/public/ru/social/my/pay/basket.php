<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?><? $APPLICATION->IncludeComponent(
    "bitrix:sale.basket.basket",
    ".default",
    array(
        "ACTION_VARIABLE" => "action",
        "COLUMNS_LIST" => array(
            0 => "NAME",
            1 => "DELETE",
            2 => "PRICE",
        ),
        "COMPONENT_TEMPLATE" => ".default",
        "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
        "HIDE_COUPON" => "Y",
        "PATH_TO_ORDER" => "index.php",
        "PRICE_VAT_SHOW_VALUE" => "N",
        "QUANTITY_FLOAT" => "N",
        "SET_TITLE" => "Y",
        "USE_PREPAYMENT" => "N"
    ),
    false
); ?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>