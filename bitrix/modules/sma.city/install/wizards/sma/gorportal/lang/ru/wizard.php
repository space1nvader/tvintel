<?
$MESS["FINISH_STEP_TITLE"] = "Установка Решения завершена";
$MESS["wiz_go"] = "Далее";
$MESS["wiz_site_name"] = "Многофункциональный региональный портал";
$MESS["FINISH_STEP_CONTENT"] = "С другими решениями, а так же полезными статьями вы можете ознакомиться на сайте <a href=\"http://smart-b.ru\">Smart-B</a>";
$MESS["FINISH_STEP_COMPOSITE"] = "<u>Ваш сайт может работать быстрее любого обычного</u> в режиме <b>Композита</b><br/>Для настройки композитного режима просто перейдите в настройки административного раздела.";
$MESS["FINISH_STEP_COMPOSITE_LINK"] = "Просто включите композит";
$MESS["FINISH_STEP_REINDEX"] = "????";
$MESS["wiz_name"] = "Многофункциональный региональный портал";
$MESS["wiz_slogan"] = "Простая адаптивная страница вашего товара или услуги";
$MESS["wiz_company_name"] = "Название сайта:";
$MESS["wiz_company_description"] = "Описание сайта:";
$MESS["wisCopyright"] = "Разработано в &copy; Smart-B 2019. All Rights Reserved";
$MESS["wiz_seo"] = "Метаданные (SEO):";
$MESS["wiz_seo_title"] = "Заголовок страницы (SEO Title):";
$MESS["wiz_seo_description"] = "Описание сайта (SEO meta Description):";
$MESS["wiz_seo_keywords"] = "Ключевые слова (SEO meta Keywords):";
$MESS["wiz_keywords"] = "Многофункциональный региональный портал, 1С-Битрикс, Адаптивный дизайн, Smart-B";
?>