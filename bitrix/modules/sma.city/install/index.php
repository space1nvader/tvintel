<?
global $MESS;
$PathInstall = str_replace("\\", "/", __FILE__);
$PathInstall = substr($PathInstall, 0, strlen($PathInstall) - strlen("/index.php"));

IncludeModuleLangFile($PathInstall . "/index.php");

Class sma_city extends CModule
{
    const solutionName = 'city';
    const partnerName = 'sma';

    var $MODULE_ID = "sma.city";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $MODULE_GROUP_RIGHTS = "Y";

    function sma_city()
    {
        $arModuleVersion = array();

        include(dirname(__FILE__) . "/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        $this->MODULE_NAME = GetMessage("SMA_INSTALL_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("SMA_INSTALL_DESCRIPTION");
        $this->PARTNER_NAME = GetMessage("SMA_SPER_PARTNER");
        $this->PARTNER_URI = GetMessage("SMA_PARTNER_URI");
    }


    function InstallDB($install_wizard = true)
    {
        global $DB, $DBType, $APPLICATION;
        RegisterModule("sma.city");
        RegisterModule("sma.bill");
        //$DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sma.city/install/db/mysql/install.sql");
        return true;
    }

    function UnInstallDB($arParams = Array())
    {
        global $DB, $DBType, $APPLICATION;
        UnRegisterModule("sma.city");
        UnRegisterModule("sma.bill");
        //$DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/sma.city/install/db/mysql/uninstall.sql');
        return true;
    }

    function InstallEvents()
    {
        RegisterModuleDependences('main', 'OnBuildGlobalMenu', "sma.bill", 'CSmaBill', 'OnBuildGlobalMenu');
        return true;
    }

    function UnInstallEvents()
    {
        //UnRegisterModuleDependences("main", "OnEpilog", "studio7.reviews", "CWD_Reviews2", "CheckInitJQuery");
        //UnRegisterModuleDependences("main", "OnEndBufferContent", "studio7.reviews", "CWD_Reviews2_Buffer", "OnEndBufferContent");
        return true;
    }

    function InstallFiles()
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sma.city/install/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components/", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sma.city/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sma.city/install/modules", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules", true, true);
        //CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sma.city/install/themes/", $_SERVER['DOCUMENT_ROOT']."/bitrix/themes", true, true );
        //CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sma.city/install/tools/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sma.city/install/css/", $_SERVER['DOCUMENT_ROOT'] . '/bitrix/css/' . self::partnerName . '.' . self::solutionName, true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sma.city/install/js/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/" . self::partnerName . '.' . self::solutionName, true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sma.city/install/images/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/images/" . self::partnerName . '.' . self::solutionName, true, true);
        return true;
    }

    function IsCompositeEnabled()
    {
        if (class_exists("CHTMLPagesCache")) {
            if (method_exists("CHTMLPagesCache", "GetOptions")) {
                if ($arHTMLCacheOptions = CHTMLPagesCache::GetOptions()) {
                    if ($arHTMLCacheOptions["COMPOSITE"] == "Y") {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    function EnableComposite()
    {
        if (class_exists("CHTMLPagesCache")) {
            if (method_exists("CHTMLPagesCache", "GetOptions")) {
                if ($arHTMLCacheOptions = CHTMLPagesCache::GetOptions()) {
                    $arHTMLCacheOptions["COMPOSITE"] = "Y";
                    $arHTMLCacheOptions["DOMAINS"] = array_merge((array)$arHTMLCacheOptions["DOMAINS"], (array)$arDomains);
                    CHTMLPagesCache::SetEnabled(true);
                    CHTMLPagesCache::SetOptions($arHTMLCacheOptions);
                    bx_accelerator_reset();
                }
            }
        }
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"] . "/bitrix/components/" . $this->MODULE_ID . "/");
        DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/gorportal/");
        DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/sma.bill/");
        return true;
    }

    function DoInstall()
    {
        $this->InstallDB();
        $this->InstallFiles();
        $this->InstallEvents();
        $this->IsCompositeEnabled();
        $this->EnableComposite();
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->UnInstallEvents();
    }
}

?>