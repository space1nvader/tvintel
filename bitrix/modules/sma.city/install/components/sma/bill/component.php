<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/*************************************************************************
 * Include Modules
 *************************************************************************/

if (!CModule::IncludeModule("studio7.bill")) {
    ShowError(GetMessage("STUDIO7.BILL_MODULE_NOT_FOUND"));
    return;
}

if (intval($arParams["ELEMENT_ID"]) <= 0) {
    ShowError(GetMessage("STUDIO7.BILL_ELEMENT_ID_DEFINE"));
    return;
}

$arResult = Array();
$aModuleOptions = Array(
    "BILL_LINK_TOP_ID",
    "BILL_LINK_FIX_ID",
    "BILL_LINK_MARK_ID",
    "BILL_LINK_TOP_PRICE",
    "BILL_LINK_FIX_PRICE",
    "BILL_LINK_MARK_PRICE",
    "BILL_LINK_FIX_PROP",
    "BILL_LINK_FIXDATE_PROP",
    "BILL_LINK_MARK_PROP",
    "BILL_LINK_FIX_TIME"
);
foreach ($aModuleOptions as $option) {
    $arResult[$option] = COption::GetOptionString("bill", $option);
}

$arResult["ELEMENT_ID"] = $arParams["ELEMENT_ID"];

$this->IncludeComponentTemplate();
?>