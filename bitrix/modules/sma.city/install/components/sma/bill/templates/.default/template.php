<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<div class="bill-link-holder">
    <a id="<?= $arResult["BILL_LINK_TOP_ID"] ?>" href="#"
       onCLick="show_dialog('top', <?= $arResult["ELEMENT_ID"] ?>);return false;"><?= GetMessage("BILL_LINK_TOP") ?></a>
    <a id="<?= $arResult["BILL_LINK_FIX_ID"] ?>" href="#"
       onCLick="show_dialog('fix', <?= $arResult["ELEMENT_ID"] ?>);return false;"><?= GetMessage("BILL_LINK_FIX") ?></a>
    <a id="<?= $arResult["BILL_LINK_MARK_ID"] ?>" href="#"
       onCLick="show_dialog('mark', <?= $arResult["ELEMENT_ID"] ?>);return false;"><?= GetMessage("BILL_LINK_MARK") ?></a>
</div>

<? CUtil::InitJSCore(Array("ajax", "popup")); ?>

<script type="text/javascript">

    if (!document.getElementById('dialog_window')) {
        var dialog_window = new BX.PopupWindow("dialog_window", null, {
            autoHide: true,
            lightShadow: true,
            closeByEsc: true,
            titleBar: {
                content: BX.create("div", {
                    html: '<b><?=GetMessage("BILL_TITLE")?></b>',
                    'props': {'className': 'access-title-bar'}
                })
            },
            closeIcon: {right: "10px", top: "10px"},
            overlay: {
                backgroundColor: 'black',
                opacity: '80'
            },
            buttons: [
                new BX.PopupWindowButton({
                    text: "<?=GetMessage('BILL_DA')?>",
                    className: "popup-window-button-accept",
                    events: {
                        click: function () {
                            process_operation(saved_action, saved_element_id);
                        }
                    }
                }),
                new BX.PopupWindowButton({
                    text: "<?=GetMessage('BILL_NET')?>",
                    className: "webform-button-link-cancel",
                    events: {
                        click: function () {
                            this.popupWindow.close(); // закрытие окна
                        }
                    }
                })
            ]
        });
    }

    var saved_action = "";
    var saved_element_id = "";

    function show_dialog(action, element_id) {

        saved_action = action;
        saved_element_id = element_id;

        BX.ajax.get(
            '<?=$templateFolder?>/ajax.php?action=' + action + '&get_price=Y',
            function (response) {

                dialog_window.setButtons([
                    new BX.PopupWindowButton({
                        text: "<?=GetMessage('BILL_DA')?>",
                        className: "popup-window-button-accept",
                        events: {
                            click: function () {
                                process_operation(saved_action, saved_element_id);
                            }
                        }
                    }),
                    new BX.PopupWindowButton({
                        text: "<?=GetMessage('BILL_NET')?>",
                        className: "webform-button-link-cancel",
                        events: {
                            click: function () {
                                this.popupWindow.close(); // закрытие окна
                            }
                        }
                    })
                ]);

                dialog_window.setContent('<b><?=GetMessage("BILL_PRICE")?></b> ' + response + ' <?=GetMessageJS("STUDIO7_BILL_RUBLEY")?>');
                dialog_window.show();

            }
        );

    }

    function process_operation(action, element_id) {

        BX.ajax.get(
            '<?=$templateFolder?>/ajax.php?action=' + action + '&process=Y&ELEMENT_ID=' + element_id,
            function (response) {

                dialog_window.setButtons([
                    new BX.PopupWindowButton({
                        text: "<?=GetMessage('BILL_CLOSE')?>",
                        className: "webform-button-link-close",
                        events: {
                            click: function () {
                                this.popupWindow.close(); // закрытие окна
                                window.location.reload();
                            }
                        }
                    })
                ]);
                dialog_window.setContent(response);

            }
        );

    }


</script>
