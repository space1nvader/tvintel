<?
$MESS["BILL_USER_IS_NOT_AUTHORIZED"] = "Авторизуйтесь на сайте";
$MESS["BILL_INSUFFICIENT_FUNDS"] = "Недостаточно средств на счете.";
$MESS["BILL_WITHDRAW_ERROR"] = "Ошибка снятия средств со счета.";
$MESS["BILL_OK"] = "Операция выполнена успешно.";
$MESS["BILL_NO_ELEMENT"] = "Элемент не найден";
$MESS["BILL_PRIKREPIT"] = "Закрепить";
$MESS["BILL_DATE_K"] = "Дата окончания закрепления";
$MESS["BILL_VIDELIT"] = "Выделить";
$MESS["STUDIO7_BILL_A"] = "Да";
?>