<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arComponentParameters = array(
    "PARAMETERS" => array(
        "AJAX_MODE" => Array(),
        "ELEMENT_ID" => Array(
            "NAME" => "ID " . GetMessage("STUDIO7_BILL_ELEMENTA"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        )
    )
);
?>