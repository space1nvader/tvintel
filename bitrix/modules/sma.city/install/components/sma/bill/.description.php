<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("STUDIO7_BL_COMPONENT_NAME"),
    "DESCRIPTION" => GetMessage("STUDIO7_BL_COMPONENT_DESCRIPTION"),
    "ICON" => "/images/menu_ext.gif",
    "CACHE_PATH" => "N",
    "PATH" => array(
        "ID" => "studio7",
        "NAME" => "Studio7",
        "CHILD" => array(
            "ID" => "studio7_bill",
            "NAME" => GetMessage("STUDIO7_BL_COMPONENT_NAME")
        )
    ),
);
?>