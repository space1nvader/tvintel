<?
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
IncludeModuleLangFile(__FILE__);

$arAllOptions = Array(
    "system_settings" =>
        Array(
            Array("BILL_LINK_TOP_ID", GetMessage("MAIN_ID_POD"), Array("text", "50")),
            Array("BILL_LINK_FIX_ID", GetMessage("MAIN_ID_ZAK"), Array("text", "50")),
            Array("BILL_LINK_MARK_ID", GetMessage("MAIN_ID_VID"), Array("text", "50")),
        ),
    "service_price" => Array(
        Array("BILL_LINK_TOP_PRICE", GetMessage("BILL_LINK_TOP_PRICE"), Array("text", "50")),
        Array("BILL_LINK_FIX_PRICE", GetMessage("BILL_LINK_FIX_PRICE"), Array("text", "50")),
        Array("BILL_LINK_MARK_PRICE", GetMessage("BILL_LINK_MARK_PRICE"), Array("text", "50")),
    ),
    "element_props" => Array(
        Array("BILL_LINK_FIX_PROP", GetMessage("BILL_LINK_FIX_PROP"), Array("text", "50")),
        Array("BILL_LINK_FIXDATE_PROP", GetMessage("BILL_LINK_FIXDATE_PROP"), Array("text", "50")),
        Array("BILL_LINK_MARK_PROP", GetMessage("BILL_LINK_MARK_PROP"), Array("text", "50")),
    ),
    "additional_settings" => Array(
        Array("BILL_LINK_FIX_TIME", GetMessage("BILL_LINK_FIX_TIME"), Array("text", "50"))
    )
);

$aTabs = array(
    array("DIV" => "edit1", "TAB" => GetMessage("BILL_SETTING"), "ICON" => "search_settings", "TITLE" => GetMessage("BILL_SETTING_MODUL"))
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && check_bitrix_sessid()) {
    if (strlen($RestoreDefaults) > 0) {
        COption::RemoveOption("bill");
    } else {
        foreach ($arAllOptions as $option_key => $arOptions) {
            foreach ($arOptions as $arOption) {
                $name = $arOption[0];
                $val = trim($_POST[$name]);
                if ($arOption[2][0] == "checkbox" && $val != "Y")
                    $val = "N";
                COption::SetOptionString("bill", $name, $val, $arOption[1]);
            }
        }
    }
    if (strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0)
        LocalRedirect($_REQUEST["back_url_settings"]);
    else
        LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
}

$tabControl->Begin();
?>
<form method="post"
      action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<?= LANGUAGE_ID ?>">
    <?
    $tabControl->BeginNextTab();
    foreach ($arAllOptions as $option_key => $arOptions):
        ?>

        <tr class="heading">
            <td colspan="2"><? echo GetMessage($option_key) ?></td>
        </tr>

        <?
        foreach ($arOptions as $arOption):
            $val = COption::GetOptionString("bill", $arOption[0]);
            $type = $arOption[2];
            ?>
            <tr>
                <td valign="top" width="50%">
                    <? if ($type[0] == "checkbox"):?>
                        <? echo "<label for=\"" . htmlspecialchars($arOption[0]) . "\">" . $arOption[1] . "</label>"; ?>:
                    <? else:?>
                        <? echo $arOption[1]; ?>:
                    <? endif; ?>
                </td>
                <td valign="top" width="50%">
                    <? if ($type[0] == "checkbox"):?>
                        <input type="checkbox" name="<? echo htmlspecialchars($arOption[0]) ?>"
                               id="<? echo htmlspecialchars($arOption[0]) ?>"
                               value="Y"<? if ($val == "Y") echo " checked"; ?>>
                    <? elseif ($type[0] == "text"):?>
                        <input type="text" size="<? echo $type[1] ?>" maxlength="255"
                               value="<? echo htmlspecialchars($val) ?>"
                               name="<? echo htmlspecialchars($arOption[0]) ?>">
                    <? elseif ($type[0] == "select"):?>
                        <select name="<? echo htmlspecialchars($arOption[0]) ?>">
                            <? foreach ($type[1] as $key => $value):?>
                                <option <? if ($val == $key) echo " selected"; ?>
                                    value="<?= $key ?>"><?= $value ?></option>
                            <? endforeach; ?>
                        </select>
                    <? elseif ($type[0] == "textarea"):?>
                        <textarea rows="<? echo $type[1] ?>" cols="<? echo $type[2] ?>"
                                  name="<? echo htmlspecialchars($arOption[0]) ?>"><? echo htmlspecialchars($val) ?></textarea>
                    <? endif ?>
                </td>
            </tr>
        <? endforeach ?>
    <? endforeach ?>
    <? $tabControl->Buttons(); ?>
    <input type="submit" name="Update" value="<?= GetMessage("MAIN_SAVE") ?>"
           title="<?= GetMessage("MAIN_OPT_SAVE_TITLE") ?>">
    <input type="submit" name="Apply" value="<?= GetMessage("MAIN_OPT_APPLY") ?>"
           title="<?= GetMessage("MAIN_OPT_APPLY_TITLE") ?>">
    <? if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
        <input type="button" name="Cancel" value="<?= GetMessage("MAIN_OPT_CANCEL") ?>"
               title="<?= GetMessage("MAIN_OPT_CANCEL_TITLE") ?>"
               onclick="window.location='<? echo htmlspecialchars(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
        <input type="hidden" name="back_url_settings" value="<?= htmlspecialchars($_REQUEST["back_url_settings"]) ?>">
    <? endif ?>
    <input type="submit" name="RestoreDefaults" title="<? echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
           OnClick="return confirm('<? echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
           value="<? echo GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
    <?= bitrix_sessid_post(); ?>
    <? $tabControl->End(); ?>
</form>