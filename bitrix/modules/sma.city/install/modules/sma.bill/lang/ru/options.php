<?
$MESS["system_settings"] = "Системные настройки";
$MESS["service_price"] = "Стоимость услуг";
$MESS["element_props"] = "Свойства элементов";
$MESS["additional_settings"] = "Дополнительно";
$MESS["STUDIO7_BILL_SSYLKI_PODNATIA_ELEM"] = "ссылки поднятия элемента";
$MESS["STUDIO7_BILL_SSYLKI_ZAKREPLENIA_E"] = "ссылки закрепления элемента";
$MESS["STUDIO7_BILL_SSYLKI_VYDELENIA_ELE"] = "ссылки выделения элемента";
$MESS["STUDIO7_BILL_STOIMOSTQ_PODNATIA_E"] = "Стоимость поднятия элемента";
$MESS["STUDIO7_BILL_STOIMOSTQ_ZAKREPLENI"] = "Стоимость закрепления элемента";
$MESS["STUDIO7_BILL_STOIMOSTQ_VYDELENIA"] = "Стоимость выделения элемента";
$MESS["STUDIO7_BILL_SVOYSTVO_ELEMENTA_DL"] = "Свойство элемента для закрепления";
$MESS["STUDIO7_BILL_SVOYSTVO_ELEMENTA_DL1"] = "Свойство элемента для даты срока закрепления";
$MESS["STUDIO7_BILL_SVOYSTVO_ELEMENTA_DL2"] = "Свойство элемента для выделения";
$MESS["STUDIO7_BILL_VREMA_NA_KOTOROE_ZAK"] = "Время на которое закрепляем элемент (суток)";
$MESS["STUDIO7_BILL_NASTROYKI"] = "Настройки";
$MESS["STUDIO7_BILL_NASTROYKA_PARAMETROV"] = "Настройка параметров модуля";
?>