<?
session_start();
class __BILINGAuth
{
	function OnUserLoginExternal(&$arArgs)
	{
        ////////// <settings> ////////////
		global $DB, $USER, $APPLICATION;
		$Server = "https://helpdesk.tvintel.info/nstaff/ext/auth/";
		$Server_info = "https://helpdesk.tvintel.info/nstaff/ext/user_info/";
        ////////// </settings> ////////////
		extract($arArgs);


		if(isset($LOGIN) && isset($PASSWORD)){
			$username = $LOGIN;
			$password = $PASSWORD;
			$json = file_get_contents($Server.'/'.$username.'/'.$password);
			$array = json_decode($json, true);




			if ($array["uid"]) {

				$name = $array["name"];

				$json_info = file_get_contents($Server_info.$array["uid"]);
				$array_info = json_decode($json_info, true);

                if ($array_info["internet"]) {
                    $_SESSION['tariff_color'] = '#f68c1f';
                } elseif ($array_info["telefon"]){
                    $_SESSION['tariff_color'] = '#d80c89';
                } elseif ($array_info["ctv"] || $array_info["iptv"] || $array_info["video"]){
                    $_SESSION['tariff_color'] = '#c4d92c';
                }


                $array_info = current($array_info);

				$money=$array_info["0"]["balance"];
				$address=$array_info["0"]["address"];
                 $del = array(",,", ",,,", ",,,,", ", ,");
				$address=str_replace($del,', ',$address);
				$tarif=$array_info["0"]["tarif"];
				$abonplata=$array_info["0"]["abonplata"];
				$status=$array_info["0"]["status"];
				$rent=$array_info["0"]["rent"];
				$payment=$array_info["0"]["payment"];

//				echo "<pre>"; print_r($array_info);  echo  "</pre>";
//				exit();
				$rent_list = "<ul class=''>";
				foreach ($rent as $value) {
					$rent_list .="<li>".$value['dateofcharge'] . "<span>&nbsp;&nbsp;&nbsp;".$value['amount']."</span></li>";
				}
				$rent_list .= "</ul>";

				$payment_list = "<ul class=''>";
				foreach ($payment as $value) {
					$payment_list .="<li>".$value['pay_date'] . "<span>&nbsp;&nbsp;&nbsp;".$value['amount']."</span></li>";
				}
				$payment_list .= "</ul>";


				$arFields = Array(
					"LOGIN" => $username,
					"NAME" => $name,
					"LAST_NAME" => '',
					"PASSWORD" => $password,
					"CONFIRM_PASSWORD"  => $password,
					"EMAIL" => $username.'@tvintel.info',
					"ACTIVE" => "Y",

					"UF_MONEY" => $money,
					"UF_ADDRESS" => $address,
					"UF_TARIF" => $tarif,
					"UF_ABONPLATA" => $abonplata,
					"UF_STATUS" => $status,
						"UF_RENT" => $rent_list,
						"UF_PAYMENT" => $payment_list,

					"LID" => SITE_ID
					);
				$filter = Array
				(
					"LOGIN_EQUAL" => $username,
					);
				$oUser = new CUser;
				$res = CUser::GetList(($by = "login"), ($order = "desc"), $filter);
				if(!($ar_res = $res->Fetch())){
					$ID = $oUser->Add($arFields);
				} else
				{
					$ID = $ar_res["ID"];
					$oUser->Update($ID, $arFields);
				}

				if($ID>0)
				{

					CUser::SetUserGroup($ID, $arGroups);
					$arArgs["store_password"] = "N";
					$USER->Authorize($USER->GetID($ID));
				}

			}else{

			}

		};








	}
};
?>