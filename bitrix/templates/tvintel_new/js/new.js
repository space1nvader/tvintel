$(document).ready(function(){
  // mobile login form
  loginPost(11);
  // hd login form
  loginPost(13);

  // main page checker
  checkPost(1);
  // tariff page checker
  checkPost(2);

  $('.rediruser').click( function(){
    window.location.href = "/user.php";
  });

  $('.openModal').on('click',function(e)
  {
    e.preventDefault();
    var modal = $(this).attr('data-modal');

    $('.modal').removeClass('show');
    $('.modal--' + modal).addClass('show');

    if(modal == 'call'){
      var ttl = $(this).attr('ttl'),
        subtitle = $(this).attr('subtitle'),
        subject = $(this).attr('subject'),
        send = $(this).attr('send')
      if(!ttl)
        ttl = 'Оставить заявку'
      if(!subtitle)
        if(!subject)
          subject = 'Оставить заявку'
      if(!send)
        send = 'Отправить'
      $('.modal__call__ttl').html(ttl)
      $('.modal__call__sttl').html(subtitle)
      $('.modal--call__formName').html(subject)
      $('.modal--call .form__send').html(send)
    }

    if(modal == 'bid'){

      var ttl = $(this).attr('ttl'),
        speed = $(this).attr('speed'),
        price = $(this).attr('price'),
        id = $(this).attr('tid');

      if(!speed ) {
        $('.modal__bid__det_speed').remove();
        if (!ttl && !price) {
          $('.modal__bid__dets').remove();
          $('#name--12').attr('placeholder', 'Название компании');
        }
      }

      $('.modal__bid__det__val--1').html(ttl);
      $('.modal__bid__det__val--2').html(speed);
      $('.modal__bid__det__val--3').html(price);
      $('#modal__bid__det__val--4').val(id);

    }

    if(modal == 'payPopup') {
      var modal_type = $(this).attr('data-type');

      $('.modal__pay__text').hide();
      $('.modal__pay__text-' + modal_type).show();
    }


  });

});

function checkPost(form_number) {
  $('#form--'+form_number+' .form__send').click( function(){
    var street = $('#addr1--'+form_number).val();
    var building = $('#addr2--'+form_number).val();

    const data = { street: street, building: building };

    $.ajax({
      url: '/ajax/check.php',
      type: 'POST',
      data: data,
      dataType : "json",
      success: function(response) {
        if(response.success){

          $('#street--14').val(street);
          $('#house--14').val(building);

          let descr = 'Оставьте данные с вами свяжется оператор';

          if(response.found) {
            $('.modal--check .modal__bid__ttl').html('УРА! <br> Вы можете быть подключены');
            $('.modal--check .modal__check__descr').html(descr);
          } else {
            $('.modal--check .modal__bid__ttl').html('К сожалению, мы пока не сможем вас подключить');
            $('.modal--check .modal__check__descr').html(descr + ' как только появится возможность подключения');
          }

          $('.modal').removeClass('show');
          $('.modal--check').addClass('show');

        } else {
          alert(responce.message);
        }
      },
      error:function(xhr,str){alert('Возникла ошибка!')}
    });

    return false;
  });
}

function loginPost(form_number) {
  $('#form--'+form_number+' .form__send').click( function(){
    var login = $('#login--'+form_number).val();
    var pass = $('#pass--'+form_number).val();

    $.post(
      "/ajax/login.php",
      {
        login: login,
        pass:pass
      },
      function onAjaxSuccess(data)
      {
        if(data==1){
          window.location.href = "/user.php";
        }else{
          $(".err").remove();
          $('.modal__login__form--in').before(data);
        }

      }
    );
    return false;
  });
}