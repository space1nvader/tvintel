<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html class="js-no" id="html">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <?
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/js.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/map.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/new.js');
    ?>

    <script>
      document.getElementById('html').classList.remove('js-no')
      document.getElementById('html').classList.add('js')
    </script>
<script>
function tab(n)
{
    if(n==1)
    {
        document.getElementById('tab1').style.display='block';
        document.getElementById('tab2').style.display='none';
        document.getElementById('nav0').className='prc__nav active';
        document.getElementById('nav1').className='prc__nav';
    }
    else
    {
        document.getElementById('tab2').style.display='block';
        document.getElementById('tab1').style.display='none';
        document.getElementById('nav1').className='prc__nav active';
        document.getElementById('nav0').className='prc__nav';
    }
}
	  </script>
  </head>

<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>


<body class="page page--index">
    <div class="page--in">
      <section class="nav" id="nav">
        <div class="nav--in">
          <div class="nav__head">
            <div class="nav__ct">
              <p class="nav__ct__ttl">служба поддержки и продаж</p>
              <p class="nav__ct__tel"><a class="nav__ct__tel__link nobr" href="tel:+7-862-271-11-11">+7 (862) 271-11-11</a></p>
            </div>
            <p class="nav__closeNav"></p>
          </div>
    <?
     global $USER;
     if (! $USER->IsAuthorized()) {
    ?>

         <div class="nav__loginw">
             <span class="nav__login" data-modal="login">
                 <span class="nav__login__txt">Личный кабинет</span><span class="nav__login__ic"></span></span>
             <div class="nav__login__formw">
                 <form class="nav__login__form form" id="form--11" action="javascript:void(0)" method="post" data-form="2">
                     <div class="nav__login__form--in">
                         <p class="form__item form__item--login">
                             <input class="form__input form__input--login" id="login--11" name="login" type="text" placeholder="Логин">
                         </p>
                         <p class="form__item form__item--pass">
                             <input class="form__input form__input--pass" id="pass--11" name="pass" type="password" placeholder="Пароль">
                         </p>
                         <p class="form__item form__item--send">
                             <input class="modal__call__formName" name="formName" type="hidden" value="Войти">
                             <button class="form__send" type="submit">Войти</button>
                         </p>
                     </div>
                 </form>
             </div>
         </div>
    <? } else { ?>
         
         <? if(strpos($_SERVER['REQUEST_URI'], 'user')) : ?>
             <a href="/?logout=yes" style="justify-content: center; display: flex;">
                <span class="head__login rediruser" style="width: 100%;margin: 0 19px;padding: 10px 10px;text-align: center;justify-content: center; display: flex;">
                    <span class="head__login__txt">
                        Выход
                    </span>
                    <span class="head__signout__ic"></span>
                </span>
             </a>
         <? else: ?>
             <a href="/user.php" style="justify-content: center; display: flex;">
                <span class="head__login rediruser" style="width: 100%;margin: 0 19px;padding: 10px 10px;text-align: center;justify-content: center; display: flex;">
                    <span class="head__login__txt">
                        Личный кабинет
                    </span>
                </span>
             </a>
         <? endif; ?>
    <? };?>
          <ul class="nav__users">
            <li class="nav__user nav__user--1 <?=(!strpos($_SERVER['REQUEST_URI'], 'korporativnym-klientam')) ? 'active' : ''?>">
                <a class="nav__user__link" href="/chastnym-litsam/">Для меня</a>
            </li>
            <li class="nav__user nav__user--2 <?=(strpos($_SERVER['REQUEST_URI'], 'korporativnym-klientam')) ? 'active' : ''?>">
                <a style="width: 50%" class="nav__user__link" href="/korporativnym-klientam/">Для бизнеса</a>
            </li>
          </ul>
          
          <ul class="nav__navs">
              <? $APPLICATION->IncludeComponent(
                  "bitrix:menu",
                  "mobile",
                  array(
                      "ROOT_MENU_TYPE" => "top",
                      "MAX_LEVEL" => "1",
                      "USE_EXT" => "N",
                      "MENU_CACHE_TYPE" => "A",
                      "MENU_CACHE_TIME" => "3600",
                      "MENU_CACHE_USE_GROUPS" => "N",
                      "MENU_CACHE_GET_VARS" => "",
                      "COMPONENT_TEMPLATE" => "mobile"
                  ),
                  false
              ); ?>
          </ul>
          <ul class="nav__items">
			  <li class="nav__item active"><a class="nav__item__link" href="/about/">О компании</a></li>
			  <li class="nav__item"><a class="nav__item__link" href="/about/contacts/">Контактная информация</a></li>
			  <li class="nav__item"><a class="nav__item__link" href="/about/news/">Новости</a></li>
			  <li class="nav__item"><a class="nav__item__link" href="/about/vakansy/">Вакансии</a></li>
              <li class="nav__item"><a class="nav__item__link" href="#">Оборудование</a></li>
			  <li class="nav__item"><a class="nav__item__link" href="/about/requisites/">Реквизиты компании</a></li>
			  <li class="nav__item"><a class="nav__item__link" href="/about/docs/">Лицензии</a></li>
            <li class="nav__item"><a class="nav__item__link" href="#">Договор оферты КТВ</a></li>
            <li class="nav__item"><a class="nav__item__link" href="#">Договор на оказание услуг</a></li>
            <li class="nav__item"><a class="nav__item__link" href="#">Политика конфиденциальности</a></li>
          </ul>
          <p class="nav__copy">© 2019 ООО «РАДИСТ»</p>
        </div>
      </section>
      <section class="head" id="head">
        <div class="head__top">
          <div class="head__top--in">
            <ul class="head__users">
				<li class="head__user head__user--1 <?=(strpos($_SERVER['REQUEST_URI'], 'chastnym-litsam')) ? 'active' : ''?>"><a class="head__user__link" href="/chastnym-litsam/">Для меня</a></li>
				<li class="head__user head__user--2 <?=(strpos($_SERVER['REQUEST_URI'], 'korporativnym-klientam')) ? 'active' : ''?>"><a class="head__user__link" href="/korporativnym-klientam/">Для бизнеса</a></li>
			  </ul>
            <ul class="head__extras">
              <li class="head__extra head__extra--1"><a class="head__extra__link" href="http://tvintel.speedtestcustom.com/">Тест скорости</a></li>
              <li class="head__extra head__extra--2"><a class="head__extra__link" href="/web-camera/">Сочи онлайн</a></li>
            </ul>
          </div>
        </div>
        <div class="head__bot">
          <div class="head__bot--in">
			  <h1 class="head__logo"><a class="head__logo__link scrollTo" data-to="0" href="/" title="Твинтел • Телевидение, интернет, телефония"><img class="head__logo__img" src="<?=SITE_TEMPLATE_PATH?>/img/head-logo.png" alt="Твинтел • Телевидение, интернет, телефония"></a></h1>
            <ul class="head__navs">
                    <? $APPLICATION->IncludeComponent(
	"bitrix:menu",
	"top",
	array(
		"ROOT_MENU_TYPE" => "top",
		"MAX_LEVEL" => "1",
		"USE_EXT" => "N",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "N",
		"MENU_CACHE_GET_VARS" => "",
		"COMPONENT_TEMPLATE" => "top"
	),
	false
); ?>
            </ul>
            <div class="head__ct">
              <p class="head__ct__ttl">служба поддержки и продаж</p>
              <p clas
                 s="head__ct__tel"><a class="head__ct__tel__link nobr" href="tel:+7-862-271-11-11">+7 (862) 271-11-11</a></p>
            </div>
    <? global $USER; ?>
    
    <? if ($USER->IsAuthorized()) :?>
    
        <?
            $userid = $USER->GetID();
            $rsUser = CUser::GetByID($userid);
            $arUser = $rsUser->Fetch();
        ?>
        <? if(!strpos($_SERVER['REQUEST_URI'], 'user')) : ?>
            <p class="head__loginw">
            <span class="head__login rediruser">
                <span class="head__login__txt">
                    Личный кабинет
                </span>
            </span>
            </p>
        <? else: ?>
            <a href="/?logout=yes">
                <p class="head__loginw">
                    <span class="head__login rediruser">
                        <span class="head__login__txt">
                            Выход
                        </span>
                        <span class="head__signout__ic"></span>
                    </span>
                </p>
            </a>
        <? endif; ?>
    <? else: ?>
        <p class="head__loginw">
            <span class="head__login openModal" data-modal="login">
                <span class="head__login__ic"></span>
                <span class="head__login__txt">Личный кабинет</span>
            </span>
        </p>
    <? endif; ?>
            
            <p class="head__openNav"></p>
          </div>
        </div>
      </section>