<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
    $this->setFrameMode(true);
?>

<? if (!empty($arResult)): ?>
    <? foreach ($arResult as $arItem): ?>
        <? if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>
    
        <? if ($arItem["SELECTED"]): ?>
            <li class="nav__nav active"><a class="nav__nav__link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></li>
        <? else: ?>
            <li class="nav__nav"><a class="nav__nav__link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></li>
        <? endif?>

    <? endforeach ?>

<? endif ?>