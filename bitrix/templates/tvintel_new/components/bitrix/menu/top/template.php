<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
    $this->setFrameMode(true);
?>

<? if (!empty($arResult)): ?>
    <? foreach ($arResult as $arItem): ?>
        <? if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>
    
        <? if ($arItem["SELECTED"]): ?>
            <li class="head__nav active"><a class="head__nav__link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
        <? else: ?>
            <li class="head__nav"><a class="head__nav__link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
        <? endif?>

    <? endforeach ?>

<? endif ?>