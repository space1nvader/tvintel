<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
      <section class="news" id="news">
        <div class="news--in">
          <h2 class="news__ttl">Новости компании</h2>
          <ul class="news__its">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
<li class="news__it" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
<a class="news__it__link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                <p class="news__it__pic"><img class="news__it__pic__img" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"></p>
                <div class="news__it__info">
                  <p class="news__it__date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></p>
                  <p class="news__it__ttl"><?echo $arItem["NAME"]?></p>
                </div>
</a>
</li>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</ul>
        </div>
      </section>
