<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
              <li class="prc__it">
                <p class="prc__it__ttl"><?= $arItem["NAME"] ?><span class="strong"></span></p>
                <p class="prc__it__price"><span class="prc__it__price__val"><?= $arItem["PROPERTIES"]["PRICE"]["VALUE"] ?></span><span class="prc__it__price__unit"><span class="txt">руб</span><span class="line"></span><span class="txt">мес</span></span></p>
                <p class="prc__it__speed"><?= $arItem["PROPERTIES"]["DAY"]["VALUE"] ?></p>
                <p class="prc__it__btw">
                  <span class="prc__it__bt openModal"
                        data-modal="bid"
                        ttl="<?= $arItem["NAME"] ?>"
                        speed="<?= $arItem["PROPERTIES"]["DAY"]["VALUE"] ?>"
                        price="<?= $arItem["PROPERTIES"]["PRICE"]["VALUE"] ?> руб/мес"
                        tid="<?=($arItem["PROPERTIES"]["ID_ORIG"]["VALUE"] != '') ? $arItem["PROPERTIES"]["ID_ORIG"]["VALUE"] : 0; ?>"
                  >
                      Подключить
                  </span>
                </p>
              </li>


<? endforeach; ?>

