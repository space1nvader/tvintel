<?php if(!strpos($_SERVER['REQUEST_URI'], 'user.php')):?>

    <?php if(!strpos($_SERVER['REQUEST_URI'], 'korporativnym-klientam')):?>
    <section class="bid" id="bid">
        <div class="bid--in">
            <p class="bid__pic bid__pic--d"><img class="bid__pic__img" src="<?=SITE_TEMPLATE_PATH?>/img/bid-pic.png"></p>
          <div class="bid__info">
            <h2 class="bid__ttl">Весь спорт<br><span class="nobr">в одном месте</span></h2>
            <p class="bid__sttl">и ещё доступнее!</p>
            <div class="bid__footer">
              <p class="bid__btw bid__btw--d"><a class="bid__bt" href="/chastnym-litsam"><span class="bid__bt__txt">Подключить IP-ТВ</span><span class="bid__bt__ic"></span></a></p>
              <p class="bid__price">от 150р.</p>
            </div>
            <p class="bid__btw bid__btw--m"><a class="bid__bt" href="/chastnym-litsam"><span class="bid__bt__txt">Подключить IP-ТВ</span><span class="bid__bt__ic"></span></a></p>
          </div>
        </div>
    </section>
    <?php endif; ?>

    <?php if(!strpos($_SERVER['REQUEST_URI'], 'korporativnym-klientam')):?>

    <section class="pay" id="pay">
        <div class="pay--in">
          <h2 class="pay__ttl">Вы всегда можете оплатить<br class="m"> услуги<br class="d"> удобным для вас<br class="m"> способом</h2>
          <ul class="pay__items">
            <li class="pay__item">
                <a href="/about/oplata">
                    <img class="pay__item__pic" src="<?=SITE_TEMPLATE_PATH?>/img/pay-ic-1.svg">
                </a>
            </li>
            <li class="pay__item">
                <a href="/about/oplata">
                    <img class="pay__item__pic" src="<?=SITE_TEMPLATE_PATH?>/img/pay-ic-2.svg">
                </a>
            </li>
            <li class="pay__item">
                <a href="/about/oplata">
                    <img class="pay__item__pic" src="<?=SITE_TEMPLATE_PATH?>/img/pay-ic-3.svg">
                </a>
            </li>
            <li class="pay__item pay__item--line"></li>
            <li class="pay__item">
                <a href="/about/oplata">
                    <img class="pay__item__pic" src="<?=SITE_TEMPLATE_PATH?>/img/pay-ic-4.svg">
                </a>
            </li>
            <li class="pay__item">
                <a href="/about/oplata">
                    <img class="pay__item__pic" src="<?=SITE_TEMPLATE_PATH?>/img/pay-ic-5.svg">
                </a>
            </li>
          </ul>
        </div>
    </section>

    <?php endif; ?>

<?php endif; ?>


      <footer class="foot" id="foot">
        <div class="foot--in">
          <div class="foot__ct foot__ct--m">
            <p class="foot__ct__ttl">Служба поддержки и продаж</p>
            <p class="foot__ct__tel"><a class="foot__ct__tel__link nobr" href="tel:+7-862-271-11-11">+7 (862) 271-11-11</a></p>
          </div>
          <div class="foot__top">
            <div class="foot__itemsw">
                <ul class="foot__items">
                  <li class="foot__item"><a class="foot__item__link" href="/about/">О компании</a></li>
                  <li class="foot__item"><a class="foot__item__link" href="/about/contacts/">Контактная информация</a></li>
                  <li class="foot__item"><a class="foot__item__link" href="/about/requisites/">Реквизиты компании</a></li>
                  <li class="foot__item"><a class="foot__item__link" href="/about/docs/">Документы</a></li>
                </ul>
                <ul class="foot__items">
                    <li class="foot__item"><a class="foot__item__link" href="/about/news/">Новости</a></li>
                    <li class="foot__item"><a class="foot__item__link" href="/about/vakansy/">Вакансии</a></li>
                  <li class="foot__item"><a class="foot__item__link" href="/about/oborudovanie/">Оборудование</a></li>
                  <li class="foot__item"><a class="foot__item__link" href="/about/politika-konfidentsialnosti/">Политика конфиденциальности</a></li>
                </ul>
<!--              <ul class="foot__items">-->
<!--                <li class="foot__item"><a class="foot__item__link" href="/about/vakansy/">Вакансии</a></li>-->
<!--                <li class="foot__item"><a class="foot__item__link" href="#">Договор оферты КТВ</a></li>-->
<!--                <li class="foot__item"><a class="foot__item__link" href="#">Договор на оказание услуг</a></li>-->
<!--              </ul>-->
            </div>
            <ul class="foot__socs">
              <li class="foot__soc"><a class="foot__soc__link foot__soc__link--1" href="#"></a></li>
              <li class="foot__soc"><a class="foot__soc__link foot__soc__link--2" href="#"></a></li>
              <li class="foot__soc"><a class="foot__soc__link foot__soc__link--3" href="#"></a></li>
              <li class="foot__soc"><a class="foot__soc__link foot__soc__link--4" href="#"></a></li>
              <li class="foot__soc"><a class="foot__soc__link foot__soc__link--5" href="#"></a></li>
              <li class="foot__soc"><a class="foot__soc__link foot__soc__link--6" href="#"></a></li>
            </ul>
          </div>
          <div class="foot__bot">
            <p class="foot__copy">© 2019 ООО «РАДИСТ»</p>
            <div class="foot__ct foot__ct--d">
              <p class="foot__ct__ttl">Служба поддержки и продаж</p>
              <p class="foot__ct__tel"><a class="foot__ct__tel__link nobr" href="tel:+7-862-271-11-11">+7 (862) 271-11-11</a></p>
            </div>
          </div>
        </div>
      </footer>
    </div>

    <div class="modals">
      
      <div class="modal modal--call">
        <div class="modal__bg" @click="modalClose"></div>
        <div class="modal__body modal__body--call"><span class="modal__close" @click="modalClose"></span>
          <div class="modal__call">
            <div class="modal__call__formw">
              <p class="modal__call__ttl">Оставьте свои контакты и мы свяжемся <span class="nobr">с вами в течение 10 минут</span></p>
              <form class="modal__call__form form" id="form--10" action="javascript:void(0)" method="post" data-form="10">
                <div class="modal__call__form--in">
                  <p class="form__item form__item--name">
                    <input class="form__input form__input--name" id="name--10" name="name" type="text" placeholder="Имя">
                  </p>
                  <p class="form__item form__item--tel">
                    <input class="form__input form__input--valid form__input--tel" id="tel--10" name="tel" type="tel" placeholder="Телефон*">
                  </p>
                  <p class="form__item form__item--mail">
                    <input class="form__input form__input--mail" id="mail--10" name="mail" type="text" placeholder="Почта">
                  </p>
                  <p class="form__note">*Поля обязательные для заполнения</p>
                  <p class="form__item form__item--send">
                    <input class="modal__call__formName" name="formName" type="hidden" value="Отправить заявку">
                    <button class="form__send" type="submit" thxTitle="&lt;span class=&quot;b&quot;&gt;Спасибо&lt;/span&gt;&lt;span class=&quot;s&quot;&gt;за Вашу заявку!&lt;/span&gt;" thxDesk="Наши специалисты свяжутся &lt;span class=&quot;nobr&quot;&gt;с вами в ближайшее время&lt;/span&gt;">Отправить заявку</button>
                  </p>
                </div>
                <div class="form__term">
                  <p class="form__term__ic"></p>
                  <p class="form__term__txt">Я даю согласие на обработку<span class="nobr">персональных данных.</span></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

        <div class="modal modal--bid">
            <div class="modal__bg" @click="modalClose"></div>
            <div class="modal__body modal__body--bid"><span class="modal__close" @click="modalClose"></span>
                <div class="modal__bid">
                    <p class="modal__bid__ttl">Заявка на подключение</p>
                    <ul class="modal__bid__dets">
                        <li class="modal__bid__det">
                            <p class="modal__bid__det__ttl">Тарифный план</p>
                            <p class="modal__bid__det__val modal__bid__det__val--1">МИР 100</p>
                        </li>
                        <li class="modal__bid__det modal__bid__det_speed">
                            <p class="modal__bid__det__ttl">Скорость до</p>
                            <p class="modal__bid__det__val modal__bid__det__val--2">60 Мбит</p>
                        </li>
                        <li class="modal__bid__det">
                            <p class="modal__bid__det__ttl">Стоимость</p>
                            <p class="modal__bid__det__val modal__bid__det__val--3">550 руб./м.</p>
                        </li>
                    </ul>
                    <div class="modal__bid__formw">
                        <form class="modal__call__form form" id="form--12" action="javascript:void(0)" method="post" data-form="12">
                            <p class="form__item form__item--name">
                                <input class="form__input form__input--valid form__input--name" id="name--12" name="name" type="text" placeholder="<?=(strpos($_SERVER['REQUEST_URI'], 'korporativnym-klientam')) ? 'Название компании' : 'Имя'?>*">
                            </p>
                            <p class="form__item form__item--tel">
                                <input class="form__input form__input--valid form__input--tel" id="tel--12" name="tel" type="tel" placeholder="Телефон*">
                            </p>
                            <p class="form__item form__item--addr1">
                                <input class="form__input form__input--valid form__input--addr1" id="addr1--12" name="addr1" type="text" placeholder="Название улицы*">
                            </p>
                            <p class="form__item form__item--addr2">
                                <input class="form__input form__input--valid form__input--addr2" id="addr2--12" name="addr2" type="text" placeholder="Дом*">
                            </p>
                            <p class="form__item form__item--addr3">
                                <input class="form__input form__input--addr3" id="addr3--12" name="addr3" type="text" placeholder="<?=(strpos($_SERVER['REQUEST_URI'], 'korporativnym-klientam')) ? 'Офис' : 'Квартира'?>">
                            </p>
                            
                            <p style="margin-top: 30px;">
                                <input type="checkbox" name="agree" checked style="-webkit-appearance: checkbox;"> Я согласен на обработку персональных данных<br>
                            </p>
                            
                            <input id="modal__bid__det__val--4" name="id" type="hidden" value="">
                            <p class="form__item form__item--send">
                                <input class="modal__call__formName" name="formName" type="hidden" value="Отправить заявку">
                                <button class="form__send" type="submit" thxTitle="&lt;span class=&quot;b&quot;&gt;Спасибо&lt;/span&gt;&lt;span class=&quot;s&quot;&gt;за Вашу заявку!&lt;/span&gt;" thxDesk="Наши специалисты свяжутся &lt;span class=&quot;nobr&quot;&gt;с вами в ближайшее время&lt;/span&gt;">Отправить</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal modal--payPopup">
            <div class="modal__bg" @click="modalClose"></div>
            <div class="modal__body modal__body--bid" style="max-width: 800px; margin-top: 50px 0">
                
                <span class="modal__close" @click="modalClose"></span>
                
                <div class="modal__payPopup" style="margin: 50px 0">
                    
                    <div class="modal__pay__text modal__pay__text-card">
                        <p class="modal__bid__ttl">
                            Инструкция по оплате банковскими картами (Visa, Master Card) и Яндекс.деньгами
                        </p>
                        <br />
                        <div class="modal__bid__formw">
                            <p>Для оплаты услуг пластиковой картой или Яндекс.деньгами требуется войти в личный кабинет по адресу <a href="https://client.tvintel.info">https://client.tvintel.info</a>.</p>
                            <p>Далее в левом меню выбираем пункт «Оплата услуг» -&gt; «Пополнить баланс».</p>
                            <p><img src="/bitrix/templates/tvintel/img/image001.png" alt="image001" width="500"></p>
                            <p>На следующей странице выбираем платежную систему «Яндекс деньги».</p>
                            <p><img src="/bitrix/templates/tvintel/img/image003.png" alt="image003" width="500"></p>
                            <p>Далее выбираем номер договора, по которому требуется произвести оплату.</p>
                            <p><img src="/bitrix/templates/tvintel/img/image005.png" alt="image005" width="500"></p>
                            <p>В следующем окне выбираем способ платежа «Платеж с банковской карты» и указываем сумму к оплате. &nbsp;Для оплаты услуг при помощи Яндекс.денег и Webmoney выберите соответствующий пункт меню.</p>
                            <p><img src="/bitrix/templates/tvintel/img/image007.png" alt="image007" width="500"></p>
                            <p>Затем вы будете перенаправлены на страницу Яндекс Кассы, где вам потребуется ввести реквизиты банковской карты и e-mail, на который придет оповещение о платеже.</p>
                            <p><img src="/bitrix/templates/tvintel/img/image009.png" alt="image009" width="500"></p>
                            <p>&nbsp;</p>
                            <p><strong>В зависимости от требований конкретного банка, выпустившего карту, может потребоваться дополнительное подтверждение платежа при помощи SMS, одноразового пароля или иного способа.<br></strong></p>
                        </div>
                    </div>

                    <div class="modal__pay__text modal__pay__text-sber">
                        <p class="modal__bid__ttl">
                            Инструкция по оплате через мобильное приложение Сбербанк-онлайн                        </p>
                        <br />
                        <div class="modal__bid__formw">
                            <div style="text-align: center;">Авторизовываемя в мобильном риложении Сбербанк-Онлайн:<br><br>&nbsp;<img src="/bitrix/templates/tvintel/img/1.PNG" alt=""></div>
                            <div style="text-align: center;">Выбираем раздел "Интернет и ТВ":</div>
                            <div style="text-align: center;"><img src="/bitrix/templates/tvintel/img/2.PNG" alt=""><br><br>В строке поиска указываем "Твинтел":<br><br><img src="/bitrix/templates/tvintel/img/photo_2017-11-29_17-16-02.jpg" alt="photo_2017-11-29_17-16-02.jpg" width="416" height="736"></div>
                            <div style="text-align: center;">Выбираем счет для cписания и указываем номер договора:<br><br></div>
                            <div style="text-align: center;"><img src="/bitrix/templates/tvintel/img/photo_2017-11-29_18-07-25.png" alt="photo_2017-11-29_18-07-25.png" width="390" height="667"></div>
                            <div style="text-align: center;">&nbsp;</div>
                            <div style="text-align: center;">&nbsp;</div>
                            <div style="text-align: center;"><img src="/bitrix/templates/tvintel/img/photo_2017-11-29_18-07-29.png" alt="photo_2017-11-29_18-07-29.png" width="393" height="901"><br><br><br></div>
                            <div style="text-align: center;">Указываем номер договора, сумму платежа и нажимаем "Продолжить".<br><br>Обращаем внимание, что ввиду особенностей работы Сберанка платеж зачислаеся на счет в срок до трех рабочих дней.</div>
                            <div style="text-align: center;">&nbsp;</div>

                            <h2>Инструкция по оплате через Сбербанк Онлайн</h2>
                            <div>

                                Для оплаты услуг через Сбербанк-Онлайн открываем сайт Сбербанка (<a href="http://www.sberbank.ru/">http://www.sberbank.ru/</a>) и входим в систему Сбербанк Онлайн.<br><br><img src="/bitrix/templates/tvintel/img/pay/image001.jpg" alt="image001"><br><br>Вводим логин и пароль в системе Сбербанк-Онлайн.<br><br><img src="/bitrix/templates/tvintel/img/pay/image003.jpg" alt="image003"><br><br>Подтверждаем вход в систему при помощи SMS-пароля.<br><br><img src="/bitrix/templates/tvintel/img/pay/image005.jpg" alt="image005"><br><br>Далее переходим в раздел «Платежи и переводы», в строке поиска вводим «Радист» и нажимаем «Найти».<br><br><img src="/bitrix/templates/tvintel/img/pay/image7.jpg" alt="image7">После этого щелкаем по надписи «Радист».<br><br><img src="/bitrix/templates/tvintel/img/pay/image8.jpg" alt="image8"><br>Выбираем карту, с которой требуется произвести оплату, указываем номер договора и нажимаем «Продолжить».<br><br><img src="/bitrix/templates/tvintel/img/pay/image9.jpg" alt="image9"><br>На следующей странице указываем сумму к оплате и нажимаем «Продолжить».<br><br><img src="/bitrix/templates/tvintel/img/pay/image10.jpg" alt="image10"><span style="font-size: 11pt; line-height: 15.6933336257935px; font-family: Calibri, sans-serif;"><br>Проверяем правильность введенных данных (номер договора и сумма к оплате). Если все верно – вводим&nbsp;</span><span style="font-size: 11pt; line-height: 15.6933336257935px; font-family: Calibri, sans-serif;">SMS-</span><span style="font-size: 11pt; line-height: 15.6933336257935px; font-family: Calibri, sans-serif;">пароль и нажимаем «Подтвердить».</span><br><span style="font-size: 11pt; line-height: 107%; font-family: Calibri, sans-serif;"><br><img src="/bitrix/templates/tvintel/img/pay/image11.jpg" alt="image11"><br></span><span style="font-size: 11pt; line-height: 107%; font-family: Calibri, sans-serif;"><br><br></span><br><br>

                            </div>
                        </div>
                    </div>
                    
                    <div class="modal__pay__text modal__pay__text-qiwi">
                        <p class="modal__bid__ttl">
                            Оплата через QIWI Терминалы
                        </p>
                        <br />
                        <div class="modal__bid__formw">
                            Пополнить счет можно в любом QIWI Терминале по пути домой. QIWI Терминалы легко узнать по трем большим кнопкам на экране. Заплатить просто: достаточно нажать 3-5 кнопок и внести наличные. Деньги будут зачислены моментально. Карта расположения терминалов и контактная информация на <a href="http://www.qiwi.com">www.qiwi.com<br></a><br>Как пополнить счет в QIWI Терминале:<br><br>На главном экране выберите раздел «Оплата услуг»;<br><br><img src="/bitrix/templates/tvintel/img/qw1.jpg" alt="qw1"><br><br>Нажмите кнопку «Поиск по всем услугам»;<br><br><img src="/bitrix/templates/tvintel/img/qw2.jpg" alt="qw2"><br><br>Введите название компании или услуги в строку поиска и выберите нужный вариант;<br><br><img src="/bitrix/templates/tvintel/img/qw3.jpg" alt="qw3"><br><br>Укажите необходимые для платежа данные;Внесите наличные в купюроприемник;Сохраните квитанцию об оплате до зачисления платежа.
                            <h2>Карта приема платежей QIWI Терминалов</h2>
                            <div>
                                <a href="https://qiwi.ru/replenish/map.action?lat=43.60174851451021&amp;lng=39.72512895&amp;zoom=13"><img src="/bitrix/templates/tvintel/img/mapq.jpg" alt="mapq" style="width: 100%;"></a>
                            </div>
                            <h2>Адреса приема платежей QIWI Терминалов</h2>
                            <div>Адлерская ул, 4 <br>
                                Аллея Челтенхэма ул, д. 8 <br>
                                Аллея Челтенхэма ул, 4 <br>
                                Альпийская ул, 3А <br>
                                Апшеронская ул, 11 <br>
                                Армавирская ул, 150 <br>
                                Армавирская ул, 34 <br>
                                Армавирская ул, 56А <br>
                                Армавирская ул, 98 <br>
                                Армянская ул, 43 <br>
                                Армянская ул, 76/8 <br>
                                Батумское ш, 25 <br>
                                Батумское ш, 41 <br>
                                Батумское ш, 57а <br>
                                Бзугу ул, 6 <br>
                                Бытха ул, д.39 корп.В <br>
                                Бытха ул, 1 <br>
                                Бытха ул, 25 <br>
                                Бытха ул, 3/4 <br>
                                Бытха ул, 30 <br>
                                Бытха ул, 48А <br>
                                Бытха ул, 53 <br>
                                Бытха ул, 54 <br>
                                Виноградная ул, 122 <br>
                                Виноградная ул, 224/4 <br>
                                Виноградная ул, 272 <br>
                                Виноградная ул, 45 <br>
                                Виноградная ул, 46а <br>
                                Виноградная ул, 66 <br>
                                Виноградная ул, 85Б <br>
                                Виноградная ул, 93 <br>
                                Виноградный пер, 22 <br>
                                Волгоградская ул, 8/1 <br>
                                Воровского ул, 19 <br>
                                Воровского ул, 36 <br>
                                Воровского ул, 4 <br>
                                Ворошиловская ул, 20 <br>
                                Восточная ул, 8в <br>
                                Высокогорная ул, 54 <br>
                                Гагарина ул, д.59, стр.1 <br>
                                Гагарина ул, 15 <br>
                                Гагарина ул, 32 <br>
                                Гагарина ул, 48/1 <br>
                                Гагарина ул, 53А <br>
                                Гагарина ул, 63 <br>
                                Гагарина ул, 7б <br>
                                Гайдара ул, 11 <br>
                                Главная ул, 23/5 <br>
                                Горького пер, 14/1 <br>
                                Горького ул, 1 <br>
                                Горького ул, 22 <br>
                                Горького ул, 30/2 <br>
                                Горького ул, 53 <br>
                                Горького ул, 56 <br>
                                Госпитальная ул, 3 <br>
                                Дагомысская ул, 42 <br>
                                Дагомысская ул, 7/2 <br>
                                Дарвина ул, д.57а <br>
                                Дарвина ул, 101/1 <br>
                                Дарвина ул, 4 <br>
                                Дарвина ул, 82, кор.1 <br>
                                Дарвина ул, 93 <br>
                                Декабристов ул, 78Б <br>
                                Дивноморская ул, 17 <br>
                                Дмитриевой ул, 2а <br>
                                Донская ул, 100 <br>
                                Донская ул, 15, А <br>
                                Донская ул, 20 <br>
                                Донская ул, 28 <br>
                                Донская ул, 3/9 <br>
                                Донская ул, 36 <br>
                                Донская ул, 5, А <br>
                                Донская ул, 90 <br>
                                Донская ул, 94я <br>
                                Донской пер, 23 <br>
                                Единство ул, 21 <br>
                                Железнодорожная ул, 3 <br>
                                Загородная ул, 1/16 <br>
                                Калараш ул, 11 <br>
                                Калараш ул, 111 <br>
                                Калараш ул, 52 <br>
                                Калараш ул, 66 <br>
                                Калараш ул, 99 <br>
                                Калиновая ул, 33 <br>
                                Камо ул, д.3,корп.А <br>
                                Кольцевая ул, 16 <br>
                                Коммунаров ул, 1/4 <br>
                                Комсомольская ул, 1 <br>
                                Конституции СССР ул, д.18 <br>
                                Красноармейская ул, д.2, корп.1 <br>
                                Красноармейская ул, 19 <br>
                                Красноармейская ул, 41 <br>
                                Краснодонская ул, 18 <br>
                                Крымская ул, 25а <br>
                                Кубанская ул, 11 <br>
                                Курортный пр-кт, д.72, корп.7 <br>
                                Курортный пр-кт, 32 <br>
                                Курортный пр-кт, 40 <br>
                                Курортный пр-кт, 5 <br>
                                Курортный пр-кт, 58, кор.16 <br>
                                Курортный пр-кт, 73 <br>
                                Курортный пр-кт, 75 <br>
                                Курортный пр-кт, 92 <br>
                                Курортный пр-кт, 92/5 <br>
                                Лазарева ул, 11К <br>
                                Лазарева ул, 88А <br>
                                Ленина ул, 233/1 <br>
                                Ленина ул, 300 <br>
                                Лермонтова ул, 12 К 4 <br>
                                Лысая гора ул, 26 <br>
                                Лысая гора ул, 37 <br>
                                Львовская ул, 26а <br>
                                Львовская ул, 8/5 (СчА) <br>
                                Мацестинская ул, 7 <br>
                                Молодежная ул, 23 <br>
                                Навагинская ул, 14 <br>
                                Навагинская ул, 3/2 <br>
                                Невская ул, 14А <br>
                                Невская ул, 18 <br>
                                Невская ул, 46 <br>
                                Невская ул, 50A <br>
                                Несебрская ул, 6 <br>
                                Новая Заря ул, 3 <br>
                                Новороссийское шоссе ул, 12/11 <br>
                                Новороссийское шоссе ул, 17/1 <br>
                                Новороссийское шоссе ул, 2 <br>
                                Новороссийское шоссе ул, 2/6 <br>
                                Орджоникидзе ул, 24 <br>
                                Павлова ул, 1 <br>
                                Павлова ул, 2А <br>
                                Павлова ул, 25 <br>
                                Параллельная ул, 18 <br>
                                Парковая ул, 19 <br>
                                Партизанская ул, 15 <br>
                                Партизанская ул, 64 <br>
                                Пасечная ул, 20 <br>
                                Пионерская ул, 3/10 <br>
                                Пионерская ул, 91 <br>
                                Пирогова ул, д.10 корп. 1/7 <br>
                                Пирогова ул, 4 <br>
                                Пирогова ул, 4 корп А <br>
                                Пирогова ул, 40/1 <br>
                                Победы ул, 1 <br>
                                Победы ул, 101 <br>
                                Победы ул, 110А <br>
                                Победы ул, 153 <br>
                                Победы ул, 165 <br>
                                Победы ул, 170 <br>
                                Победы ул, 2 <br>
                                Победы ул, 264 <br>
                                Победы ул, 267 <br>
                                Победы ул, 31 <br>
                                Победы ул, 370А <br>
                                Победы ул, 67 <br>
                                Победы ул, 77 <br>
                                Победы ул, 85 <br>
                                Победы ул, 89 <br>
                                Подгорная ул, 33 <br>
                                Политехническая ул, 39 <br>
                                Поярко ул, 2 <br>
                                Приморская ул, 3А <br>
                                Приморская ул, 3/10 <br>
                                Промышленный пер, 4а <br>
                                Просвещения ул, 169а <br>
                                Речной пер, 16 <br>
                                Родниковая ул, 23 <br>
                                Роз ул, 14 <br>
                                Роз ул, 6 <br>
                                Роз ул, 67 <br>
                                Роз ул, 95 <br>
                                Ростовская ул, 6 <br>
                                Ростовская ул, 7 <br>
                                Северная ул, 12 <br>
                                Северная ул, 6 <br>
                                Советская ул, 40 <br>
                                Соколова ул, 1 <br>
                                Строительный пер, 2Д <br>
                                Строительный пер, 9 <br>
                                Сухумское шоссе ул, 19 <br>
                                Сухумское шоссе ул, 50/2 <br>
                                Теневой пер, 18 <br>
                                Тимирязева ул, 16 <br>
                                Тимирязева ул, 7 <br>
                                Титова ул, 4 <br>
                                Тоннельная ул, 2 <br>
                                Туапсинская ул, 11 <br>
                                Туапсинская ул, 13 <br>
                                Туапсинская ул, 5/33 <br>
                                Туапсинская ул, 9 <br>
                                Учительская ул, 19 <br>
                                Цветной б-р, 40 <br>
                                Целинная ул, 5 <br>
                                Центральная ул, 13 <br>
                                Чайковского ул, 19 <br>
                                Чайковского ул, 27/2 <br>
                                Чайковского ул, 7 <br>
                                Чайная ул, 24/1 <br>
                                Чебрикова ул, 36 <br>
                                Чебрикова ул, 5 <br>
                                Чебрикова ул, 7 <br>
                                Черноморская (Хостин) ул, 12 <br>
                                Чехова пер, 8 <br>
                                Чехова ул, 23 <br>
                                Чехова ул, 54 <br>
                                Шоссейная ул, 2б <br>
                                Юных Ленинцев ул, 10/2 МГ <br>
                                Юных Ленинцев ул, 5/1 <br>
                                Ялтинская ул, 11 <br>
                                Ялтинская ул, 14 <br>
                                Ясногорская ул, 4/2 <br>
                                Ясногорская ул, 4/2 <br>
                                Ясногорская ул, 6 <br>
                                Я.Фабрициуса ул, 3/7 <br>
                                20 Горно-Стрелковой Дивизии ул, 16 <br>
                                20 Горно-Стрелковой Дивизии ул, 24 <br>
                                50 лет СССР ул, 8/2</div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        
        
      <div class="modal modal--thx" id="modal--thx">
        <div class="modal__bg" @click="modalClose"></div>
        <div class="modal__body modal__body--thx"><span class="modal__close" @click="modalClose"></span>
          <div class="modal__thx">
            <div class="modal__thx__res">
              <div class="modal__thx__ttl">Ошибка!</div>
              <p class="modal__thx__txt">Попробуйте <span class="nobr">перезагрузить страницу</span></p>
            </div>
          </div>
        </div>
      </div>
      <div class="modal modal--login" id="modal--login">
        <div class="modal__bg" @click="modalClose"></div>
        <div class="modal__bodyw modal__bodyw--login">
          <div class="modal__body modal__body--login"><span class="modal__close" @click="modalClose"></span>
            <div class="modal__login">
              <div class="modal__login__formw">
                <p class="modal__login__ttl">Добро пожаловать</p>
                <p class="modal__login__txt">Любой абонент Твинтелюг имеет доступ к cвоему личному кабинету. Войдите, чтобы получить доступ к вашим персональным данным.</p>
                <form class="modal__login__form form" id="form--13" action="javascript:void(0)" method="post" data-form="11">
                  <div class="modal__login__form--in">
                    <p class="form__item form__item--login">
                      <input class="form__input form__input--login" id="login--13" name="login" type="text" placeholder="Логин">
                    </p>
                    <p class="form__item form__item--pass">
                      <input class="form__input form__input--pass" id="pass--13" name="pass" type="password" placeholder="Пароль">
                    </p>
                    <p class="form__item form__item--send">
                      <input class="modal__call__formName" name="formName" type="hidden" value="Войти">
                      <button class="form__send" type="submit">Войти</button>
                    </p>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

    <div class="modal modal--check">
        <div class="modal__bg" @click="modalClose"></div>
        <div class="modal__body modal__body--bid"><span class="modal__close" @click="modalClose"></span>
            
            <div class="modal__bid">
                <p class="modal__bid__ttl"></p>
                <p class="modal__check__descr"></p>
                
                <div class="modal__bid__formw">
                    <form class="modal__call__form form" id="form--14" action="javascript:void(0)" method="post" data-form="12">
                        <p class="form__item form__item--name">
                            <input class="form__input form__input--name" id="name--14" name="name" type="text" placeholder="Имя">
                        </p>
                        <p class="form__item form__item--tel">
                            <input class="form__input form__input--valid form__input--tel" id="tel--14" name="tel" type="tel" placeholder="Телефон*">
                        </p>
                        
                        <input id="tariff_id--14" name="id" type="hidden" value="61">
                        <input id="street--14" name="addr1" type="hidden" value="">
                        <input id="house--14" name="addr2" type="hidden" value="">
                        
                        <p class="form__item form__item--send">
                            <input class="modal__call__formName" name="formName" type="hidden" value="Отправить заявку">
                            <button class="form__send" type="submit" thxTitle="&lt;span class=&quot;b&quot;&gt;Спасибо&lt;/span&gt;&lt;span class=&quot;s&quot;&gt;за Вашу заявку!&lt;/span&gt;" thxDesk="Наши специалисты свяжутся &lt;span class=&quot;nobr&quot;&gt;с вами в ближайшее время&lt;/span&gt;">Отправить</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    </div>

<script id="chat-24-widget-code" type="text/javascript">
  !function (e) {
    var t = {};
    function n(c) { if (t[c]) return t[c].exports; var o = t[c] = {i: c, l: !1, exports: {}}; return e[c].call(o.exports, o, o.exports, n), o.l = !0, o.exports }
    n.m = e, n.c = t, n.d = function (e, t, c) { n.o(e, t) || Object.defineProperty(e, t, {configurable: !1, enumerable: !0, get: c}) }, n.n = function (e) {
      var t = e && e.__esModule ? function () { return e.default } : function () { return e  };
      return n.d(t, "a", t), t
    }, n.o = function (e, t) { return Object.prototype.hasOwnProperty.call(e, t) }, n.p = "/packs/", n(n.s = 0)
  }([function (e, t) {
    window.chat24WidgetCanRun = 1, window.chat24WidgetCanRun && function () {
      window.chat24ID = "0b62c570202e67f8b16b3b886d0a4320", window.chat24io_lang = "ru";
      var e = "https://livechat.chat2desk.com", t = document.createElement("script");
      t.type = "text/javascript", t.async = !0, fetch(e + "/packs/manifest.json").then(function (e) {
        return e.json()
      }).then(function (n) {
        t.src = e + n["widget.js"];
        var c = document.getElementsByTagName("script")[0];
        c ? c.parentNode.insertBefore(t, c) : document.documentElement.firstChild.appendChild(t);
        var o = document.createElement("link");
        o.href = e + n["widget.css"], o.rel = "stylesheet", o.id = "chat-24-io-stylesheet", o.type = "text/css", document.getElementById("chat-24-io-stylesheet") || document.getElementsByTagName("head")[0].appendChild(o)
      })
    }()
  }]);

</script>



<div id="chat-24-widget-container" style="position: fixed; bottom: 0px; right: 0px;"><div id="chat-24-desktop">
        <div id="chat-24-locales"></div>
        <div id="chat-24-position-right"></div>
        <div id="chat-24-inner-container" class="chat-24-vertical-position-2 chat-24-position-2  chat-24-content-visible" style="margin-right: 20px; box-shadow: 0px 3px 6px 0px rgba(0, 0, 0, 0.4);">
            <div id="chat-24-roll">
                <div id="chat-24-close"></div>
                <div id="chat-24-roll-icon" style="background-image: url('https://livechat.chat2desk.com/images/widget/roll/icons/w1.jpg')">
                    <span class="chat-24-badge-unread">0</span>
                </div>
            </div>
            <div id="chat-24-popup-loader" style="display: none;">
                <img src="https://livechat.chat2desk.com/images/widget/popup-loader.gif">
            </div>
            <div id="chat-24-title" class="chat-24-widget-title-nexa-font" style="font-family:FontfabricNexaScriptLight">

            </div>
            <div id="chat-24-content" style="display: block;">
                <a id="chat-24-icon-2" target="_blank" href="https://wa.me/79882319889" style="width: 44px; height: 44px;" onmouseenter="mouseEnterHandler(event)" onmouseleave="mouseLeaveHandler(event)" onclick="return clickHandler(event, 2)" title="+79882319889" transport="WhatsApp" class="chat-24-icon chat-24-icon-wa">
                </a>
                <a id="chat-24-icon-7" target="_blank" href="#" style="width: 44px; height: 44px;" onmouseenter="mouseEnterHandler(event)" onmouseleave="mouseLeaveHandler(event)" onclick="return clickHandler(event, 7)" title="Чат с оператором" transport="Онлайн-чат" class="chat-24-icon chat-24-icon-lch">
                    <span id="chat-24-badge-unread" class="chat-24-badge-unread">0</span>
                </a>
                <a href="#" id="chat-24-close-icons" class="chat-24-icon" style="width: 44px; height: 44px;"></a>
                <div style="clear: both;"></div>
            </div>
            <div id="chat-24-footer" style="display: block">ТВИНТЕЛ</div>

            <div data-react-class="LiveChatApp" data-react-props="{&quot;transport&quot;:7,&quot;lang&quot;:&quot;ru&quot;,&quot;avatar_enable&quot;:false,&quot;auto_start&quot;:false,&quot;auto_start_delay&quot;:0,&quot;logo&quot;:&quot;https://storage.chat2desk.com//companies/company_563/live_chat_avatars/channel819-21-43-56-5a3d7c9d00505.png&quot;,&quot;size&quot;:2,&quot;contact_name&quot;:&quot;ТВИНТЕЛ&quot;,&quot;show_powered&quot;:true,&quot;mobile&quot;:false,&quot;powered_link&quot;:&quot;https://chat2desk.com/?utm_source=widget\u0026utm_medium=referal\u0026utm_campaign=$domain&quot;,&quot;powered_site_name&quot;:&quot;Chat2Desk&quot;,&quot;locales&quot;:{&quot;title&quot;:&quot;Чат с оператором&quot;,&quot;loseConnection&quot;:&quot;Потеряно интернет-соединение…&quot;,&quot;attachmentSizeLimited&quot;:&quot;Не более 5 МБ размер вложения&quot;,&quot;attachmentCountLimited&quot;:&quot;Не более 5 вложений в одном сообщении&quot;,&quot;operator&quot;:&quot;Оператор&quot;,&quot;autoAnswer&quot;:&quot;Оператор (авто)&quot;,&quot;placeholder&quot;:&quot;Введите текст&quot;,&quot;you&quot;:&quot;Вы&quot;,&quot;presentFormTitle&quot;:&quot;Представьтесь, пожалуйста&quot;,&quot;presentFormBtn&quot;:&quot;Продолжить&quot;,&quot;notRequired&quot;:&quot;Необязательно&quot;},&quot;start_after_write&quot;:false,&quot;show_top_block&quot;:true,&quot;show_bottom_block&quot;:true,&quot;default_bg_image&quot;:&quot;image_1.jpg&quot;,&quot;bg_image&quot;:null,&quot;tile_image&quot;:false,&quot;bubbles_client_class&quot;:&quot;chat-24-bubbles-green&quot;,&quot;bubbles_operator_class&quot;:&quot;chat-24-bubbles-dark-blue&quot;,&quot;present_form_rows&quot;:[],&quot;show_op_name&quot;:true}" class="LiveChatApp"><div><div class="chat-24-popup chat-24-lc-size-2 desktop_lch" id="chat-24-popup-7" style="display: none;"><div id="chat-24-inner-container-lch"><div id="head_lch"><img class="chat-24-lch-logo" src="https://storage.chat2desk.com//companies/company_563/live_chat_avatars/channel819-21-43-56-5a3d7c9d00505.png"><div class="title-lch-container has-logo"><div class="title_lch">ТВИНТЕЛ</div><div class="sub_title_lch">Чат с оператором</div></div></div><div id="lose_connection_lch"><span class="icon_lose"></span><span>Потеряно интернет-соединение…</span></div><div id="body_lch" class="size-2" style="background-image: url(&quot;https://livechat.chat2desk.com/images/widget/bg/image_1.jpg&quot;); background-size: cover; background-position: 50% 50%; background-repeat: no-repeat no-repeat;"><div id="widget-message-area"><div class="fake_start_message"></div><div class="fake_end_message" style="height: 100px;"></div></div></div><div id="chat-24-live-chat-input" class="size-2 bottom_block"><div class="chat-24-live-chat-input-inner"><textarea id="message-input-lch" placeholder="Введите текст"></textarea><span id="choose-file-btn-lch"></span><div class="" aria-disabled="false" style="position: relative;"><input type="file" multiple="" autocomplete="off" style="position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; opacity: 0.00001; pointer-events: none;"></div><span id="send-btn-lch" class="chat-24-bubbles-green active"></span><div class="copy_lch">Powered by <a href="https://chat2desk.com/?utm_source=widget&amp;utm_medium=referal&amp;utm_campaign=https://tvintel.info/" target="_blank">Chat2Desk</a></div></div></div><div id="attachment-wrapper-lch" class="bottom_block"></div><div class="chat-24-popup-close-container size-2"><div class="chat-24-popup-close-lch"></div></div></div></div></div></div>


        </div>
    </div>
</div>

  </body>
</html>