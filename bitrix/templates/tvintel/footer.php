<!--TODO pfvtybnm /index_dev.phpзаменить на корень при отпрвке на прод-->
<? if ($GLOBALS["APPLICATION"]->GetCurPage() != "/") { ?>

<div class="footer_top_area">
    <div class="container newsBlock">
        <div class="row">
            <div class="col-lg-4">
                <div class="footer_widget">
                    <div class="footer_logo">
                        <a href="index.html">
                            <img src="<?= SITE_TEMPLATE_PATH ?>/img/logo.png" alt="FOOTER LOGO">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12">

                <div class="row">
                    <div class="col-md-4">
                        <div class="footer_widget">
                            <div class="footer_social_links">
                                <a href="/sravnenie/">Оборудование</a>
                                <a href="/o-kompanii/rekvizity-kompanii.php">Реквизиты компании</a>
                                <a href="/o-kompanii/litsenzii.php">Лицензии</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="footer_widget">
                            <div class="footer_social_links">
                                <a href="/o-kompanii/dogovora-oferty-ktv.php">Договор оферты КТВ</a>
                                <a href="/o-kompanii/dogovor-na-okazanie-uslug-dostupa-k-seti-internet-fl.php">Договор на оказание услуг</a>
                                <a href="#">Политика конфиденциальности
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="footer_widget">
                            <div class="footer_social_links">
                                <a href="/contacts/">Офисы компании</a>
                                <a href="/o-kompanii/vakansii.php">Вакансии</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4">
                        <div class="footer_widget">
                            <div class="footer_social_links">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer_area">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12">
                <p>Copyright © 2018 <span><i class="icofont icofont-heart-alt"></i></span> All Right Reserved</p>
            </div>
        </div>
    </div>
</footer>
    <style>
        .go_connect {
            margin-left: 0;
        }
        .mast__title {
            top: 0;
            margin: 0 auto;
            position: relative;
        }
    </style>
<?}?>
<div class="resp_nav">
    <div class="main_nav">
        <ul>
            <li class="first"><a href="/" class="">Главная</a></li>
            <li class=""><a href="/news/">Новости</a></li>
            <li class=""><a href="/o-kompanii/" class="">О нас</a></li>
            <li class=""><a href="/pay/" class="">Способы оплаты</a></li>
            <li class=""><a href="/contacts/" class="">Контакты</a></li>
        </ul>
    </div>
    <ul class="resp_links">
        <li class="split"></li>
        <li>
            <a href="/user.php" target="_blank">Личный кабинет</a>
        </li>
        <li>
            <a href="http://tvintel.speedtestcustom.com/" target="_blank">Тест скорости</a>
        </li>
        <li>
            <a href="/web-camera/" target="_blank">Сочи онлайн</a>
        </li>
        <li>
            <a href="/" title="Главная"><img src="<?= SITE_TEMPLATE_PATH ?>/img/logo.png" alt="" style="width: 100%;"></a>
        </li>
    </ul>

</div>

<div class="nav-menu">
    <a href="/" title="Главная" class="resp_logo"><img src="<?= SITE_TEMPLATE_PATH ?>/img/logo.png" alt="Эконтел"></a>
    <div class="nav-button" style="transform: matrix(1, 0, 0, 1, 0, 0);">
        <span style="top: 0px; transform: matrix(1, 0, 0, 1, 0, 0);"></span>
        <span style="transform: matrix(1, 0, 0, 1, 0, 0);"></span>
        <span style="top: 0px; transform: matrix(1, 0, 0, 1, 0, 0);"></span>
    </div>
    <span class="resp_ph">+7 (862) 2710-100</span>
</div>


<script
        src="http://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

	<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/new/bootstrap.js"); ?>
	<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/new/jquery.shiningImage.min.js"); ?>
	<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/new/script_old.js"); ?>
	<? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/new/script.js"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>
<script>
    $( document ).ready(function() {
        $(document).on('click', '.telefonMore', function () {
            $(this).parent().find('.telefonInfo').toggle('fast');
            $(this).toggleClass('active');
            if(!$(this).hasClass('active')){
                $(this).text('Подробнее');
            }else {
                $(this).text('Закрыть');
            }
        } );
        $(document).on('click', '.telefonClose', function () {
            $(this).parent().parent().parent().find('.telefonInfo').hide('fast');
        } );
    });
    
</script>

</body>
</html>
