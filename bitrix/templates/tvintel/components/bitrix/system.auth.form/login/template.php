<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init();
?>

<div class="bx-system-auth-form">

	<?
	if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
		ShowMessage($arResult['ERROR_MESSAGE']);
	?>

	<?if($arResult["FORM_TYPE"] == "login"):?>



        <div class="prev step2">
            <span class="youLogin">123456789</span>
            <span class="prevLink">✕</span>
        </div>
        <div class="description step1">
            Войдите, чтобы получить доступ к вашим персональным данным.
        </div>
        <form id="auth-form" name="system_auth_form<?=$arResult["RND"]?>"  method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
            <div class="message "></div>
            <?if($arResult["BACKURL"] <> ''):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?endif?>
            <?foreach ($arResult["POST"] as $key => $value):?>
                <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
            <?endforeach?>
            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />

            <div class="login">
                <div class="_3ra5oZ step1"><div class="_3kC4iN" role="button" tabindex="-1"><div class="_3BIjlc">?</div><div class="_3vr11o"></div><div class="LQD7yQ" style="transform: translateX(-204px); width: 220px;"><div class="YBQxbS">Номер телефона, номер лицевого счёта домашнего интернета или ТВ, или ваш собственный логин</div></div></div></div>
                <input name="USER_LOGIN" maxlength="50" class="step1 log" type="text" placeholder="Номер договора">
                <script>
                    BX.ready(function() {
                        var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
                        if (loginCookie)
                        {
                            var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
                            var loginInput = form.elements["USER_LOGIN"];
                            loginInput.value = loginCookie;
                        }
                    });
                </script>
            </div>

            <div class="password">
                <button class="_3AI0Ig step2    " type="button" title="Показать пароль"><svg width="19" height="11" viewBox="0 -2 19 11"><g fill="currentColor" fill-rule="evenodd"><path d="M0 0h1c0 .65 3 5 8.5 5S18 .65 18 0h1c0 1.55-4 6-9.5 6S0 1.545 0 0z"></path><path d="M16.323 2.35l-.743.67 2.677 2.972.743-.67-2.677-2.972zM13.53 4.2l-.93.367 1.466 3.72.93-.365L13.53 4.2zM9 5h1v4H9V5zm-3.534-.8l.93.367-1.466 3.72L4 7.923 5.466 4.2zm-2.79-1.85l.744.67L.743 5.99 0 5.322 2.677 2.35z"></path></g></svg></button>
                <input class="step2 pass" type="password" name="USER_PASSWORD"  placeholder="Пароль">
                <?if($arResult["SECURE_AUTH"]):?>
                    <span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
				<div class="bx-auth-secure-icon"></div>
			</span>
                    <noscript>
				<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
                    </noscript>
                    <script type="text/javascript">
                        document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
                    </script>
                <?endif?>
            </div>
            <input class="submit step2" type="submit" value="Войти">
        </form>
        <button class="next step1">Далее</button>


	<?
	elseif($arResult["FORM_TYPE"] == "otp"):
		?>

	<form  name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
		<?if($arResult["BACKURL"] <> ''):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?endif?>
		<input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="OTP" />
		<table width="95%">
			<tr>
				<td colspan="2">
					<?echo GetMessage("auth_form_comp_otp")?><br />
					<input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off" /></td>
				</tr>
				<?if ($arResult["CAPTCHA_CODE"]):?>
				<tr>
					<td colspan="2">
						<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
						<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
						<input type="text" name="captcha_word" maxlength="50" value="" /></td>
					</tr>
					<?endif?>
					<?if ($arResult["REMEMBER_OTP"] == "Y"):?>
					<tr>
						<td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y" /></td>
						<td width="100%"><label for="OTP_REMEMBER_frm" title="<?echo GetMessage("auth_form_comp_otp_remember_title")?>"><?echo GetMessage("auth_form_comp_otp_remember")?></label></td>
					</tr>
					<?endif?>
					<tr>
						<td colspan="2"><input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /></td>
					</tr>
					<tr>
						<td colspan="2"><noindex><a href="<?=$arResult["AUTH_LOGIN_URL"]?>" rel="nofollow"><?echo GetMessage("auth_form_comp_auth")?></a></noindex><br /></td>
					</tr>
				</table>
			</form>

			<?
			else:
				?>

			<form   action="<?=$arResult["AUTH_URL"]?>">
				<table width="95%">
					<tr>
						<td align="center">
							<?=$arResult["USER_NAME"]?><br />
							[<?=$arResult["USER_LOGIN"]?>]<br />
							<a href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_PROFILE")?>"><?=GetMessage("AUTH_PROFILE")?></a><br />
						</td>
					</tr>
					<tr>
						<td align="center">
							<?foreach ($arResult["GET"] as $key => $value):?>
							<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
							<?endforeach?>
							<input type="hidden" name="logout" value="yes" />
							<input type="submit" name="logout_butt" value="<?=GetMessage("AUTH_LOGOUT_BUTTON")?>" />
						</td>
					</tr>
				</table>
			</form>
			<?endif?>
		</div>
