<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="cardTarif">
        <div class="card__face card__face--front">
            <div class="single_pricing_section">
                <!--                    <h3>--><? // echo $arItem["NAME"] ?><!--</h3>-->
                <p class="period"><?= $arItem["NAME"] ?></p>
                <p class="period"><?= $arItem["PROPERTIES"]["PERIOD"]["VALUE"] ?></p>
                <? if ($arItem["PROPERTIES"]["SALE"]["VALUE"]): ?>
                <p class="vigoda">Выгода <strong><?= $arItem["PROPERTIES"]["SALE"]["VALUE"] ?></strong> ₽</p>
                <? else: ?>
                <div style="height: 27px;"></div>
                <? endif; ?>
                <div class="planeta">
                    <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="">
                </div>
                <div class="pricing_details descriptionTarif">
                    <? if ($arItem["PROPERTIES"]["DAY"]["VALUE"]): ?>
                        <p class="descriptionTarifItem forDay">
                            <img src="/bitrix/templates/tvintel/img/day.png" alt="">
                            <span><?= $arItem["PROPERTIES"]["DAY"]["VALUE"] ?></span>
                        </p>
                        <span class="dayTime">с 09:00 до 23:59</span>
                    <? endif; ?>
                    <? if ($arItem["PROPERTIES"]["NIGHT"]["VALUE"]): ?>

                        <p class="descriptionTarifItem forDay">
                            <img src="/bitrix/templates/tvintel/img/no-day.png" alt="">
                            <span><?= $arItem["PROPERTIES"]["NIGHT"]["VALUE"] ?></span>
                        </p>
                        <span class="dayTime">с 00:00 до 08:59</span>
                    <? endif; ?>
                    <? if ($arItem["PROPERTIES"]["ANAL"]["VALUE"]): ?>

                        <p class="descriptionTarifItem">
                            <img src="/bitrix/templates/tvintel/img/analog.png" alt=""
                                 style="margin-left: -2px;margin-right: 0;">
                            <span><?= $arItem["PROPERTIES"]["ANAL"]["VALUE"] ?></span>
                        </p>
                    <? endif; ?>
                    <? if ($arItem["PROPERTIES"]["CIFRA"]["VALUE"]): ?>

                        <p class="descriptionTarifItem">
                            <img src="/bitrix/templates/tvintel/img/cifra.png" alt="" style="margin-left: 3px;">

                            <span><?= $arItem["PROPERTIES"]["CIFRA"]["VALUE"] ?></span>
                        </p>
                    <? endif; ?>
                    <? if ($arItem["PROPERTIES"]["HD_CANAL"]["VALUE"]): ?>

                        <p class="descriptionTarifItem">
                            <img src="/bitrix/templates/tvintel/img/hd.png" alt="" style="margin-left: -5px;margin-right: 0;">

                            <span><?= $arItem["PROPERTIES"]["HD_CANAL"]["VALUE"] ?></span>
                        </p>
                    <? endif; ?>
 <? if ($arItem["PROPERTIES"]["TELEPHONE"]["~VALUE"]): ?>
                    <p class="telefonMore">
                        Подробнее
                    </p>
                    <div class="telefonInfo" style="display: none">
                        <p class="descriptionTarifItem">
                            <?= $arItem["PROPERTIES"]["TELEPHONE"]["~VALUE"]["TEXT"] ?>
                        </p>
                    </div>
 <? endif; ?>
                    <? echo $arItem["PREVIEW_TEXT"]; ?>
                </div>
                <h3 class="price"><?= $arItem["PROPERTIES"]["PRICE"]["VALUE"] ?> <span>₽</span></h3>

                <a href="javascript:void(0);" class="boxed_btn price_btn tarifCardSend">Подключить</a>
            </div>
        </div>
        <div class="card__face card__face--back">
            <img class="tarifCardClose"
                 src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMCIgaGVpZ2h0PSIxNiIgdmlld0JveD0iMCAwIDEwIDE2Ij4gICAgPHBhdGggZmlsbD0iIzAwMCIgZmlsbC1ydWxlPSJldmVub2RkIiBkPSJNMS44OTIgMEwwIDEuODY3IDYuMjE2IDggMCAxNC4xMzMgMS44OTIgMTYgMTAgOHoiIG9wYWNpdHk9Ii43Ii8+PC9zdmc+"
                 alt=""
                 style="position: absolute;width: 15px;margin: 10px;opacity: 0.35;cursor: pointer;margin-top: 44px;transform: rotate(180deg);margin-left: 18px;">
            <div class="single_pricing_section">
                <p class="period"><?= $arItem["NAME"] ?></p>
                <h3 class="price"><?= $arItem["PROPERTIES"]["PRICE"]["VALUE"] ?> <span>₽</span></h3>
                <p class="period"><?= $arItem["PROPERTIES"]["PERIOD"]["VALUE"] ?></p>
                <? if ($arItem["PROPERTIES"]["SALE"]["VALUE"]): ?>
                    <p class="vigoda">Выгода <strong><?= $arItem["PROPERTIES"]["SALE"]["VALUE"] ?></strong> ₽</p>
                <? else: ?>
                    <div style="height: 27px;"></div>
                <? endif; ?>
                <div class="planeta">
                    <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="">
                </div>

                <form class="tariff_send_ajax" action="/for-apartment/sendMail.php" method="post">
                    <div class="form-group ">
                        <input type="hidden" name="hidden_title" tabindex="0" value="<? echo $arItem["NAME"] ?>">
                        <input type="text"
                               name="name"
                               class="form-control"
                               required="required"
                               placeholder="Как вас зовут*"
                               tabindex="0">
                    </div>
                    <div class="form-group ">
                        <input type="text" name="phone" class="form-control phoneIn" required="required"
                               placeholder="+7________*" tabindex="0">
                    </div>
                    <div class="form-group ajax-adress">
                        <style>
                            .select2-container {
                                max-width: 100%;
                            }

                            .select2-results__option {
                                font-size: 12px;
                                line-height: 18px;
                            }
                        </style>
                        <input type="hidden" name="street" class="form-control" list="street" required="required"
                               placeholder="Улица*" tabindex="0">
                        <select class="js-example-basic-single" name="adress">
                            <option value=" 20 Горно-Стрелковой Дивизии 18 а ">Выберите адрес.................</option>

                        </select>

                    </div>


                    <!--                        <div class="form-row">-->
                    <!--                            <div class="form-group col-md-6">-->
                    <!--                                <input type="text" name="house" class="form-control" required="required" placeholder="Дом*" tabindex="0">-->
                    <!--                            </div>-->
                    <!--                            <div class="form-group col-md-6">-->
                    <!--                                <input type="text" name="apartment" class="form-control" placeholder="Квартира" tabindex="0">-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <div class="form-group">
                        <div class="form-check">
                            <input checked="" type="checkbox" class="form-check-input checkbox" tabindex="0">
                            <label class="form-check-label" for="">
                                Согласие
                                на обработку персональных данных
                            </label>
                        </div>
                    </div>
                    <div class="button-center">
                        <input class="boxed_btn price_btn" type="submit" value="Подключить" style="background: none;">
                    </div>
                </form>
            </div>
        </div>
    </div>
<? endforeach; ?>

