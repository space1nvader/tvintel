<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="mfeedback">
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>
    <form action="<?=POST_FORM_ACTION_URI?>" method="POST">
        <?=bitrix_sessid_post()?>
        <div class="row">
            <div class="col-lg-6">
                <p class="main_input"><input type="text" placeholder="Ваше имя:" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>"></p>
            </div>
            <div class="col-lg-6">
                <p class="main_input"><input type="email" placeholder="Ваш email:" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>"></p>
            </div>
            <div class="" style="padding: 0 10px;">
                <p class="text_leave">
                    <textarea name="MESSAGE" placeholder="Ваше сообщение"><?=$arResult["MESSAGE"]?></textarea>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
                <input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>" class="boxed_btn submit_btn">
            </div>
        </div>
    </form>
</div>