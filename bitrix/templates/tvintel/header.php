<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
CJSCore::Init(array("jquery2"));
?>
<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
	<?$APPLICATION->ShowHead();?>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">

	<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/new/reset.css"); ?>
	<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/new/style_old.css"); ?>
	<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/new/style.css"); ?>

    <!--TODO pfvtybnm /index_dev.phpзаменить на корень при отпрвке на прод-->
    <? if ($GLOBALS["APPLICATION"]->GetCurPage() != "/") { ?>
        <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/new/blogStyle.css"); ?>
	<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/new/adaptive.css"); ?>
	<? }?>
    <? if ($GLOBALS["APPLICATION"]->GetCurPage() == "/sravnenie/") { ?>
        <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/new/sravnenie.css"); ?>
    <? }?>
        <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/new/selector.css"); ?>
	<title><?$APPLICATION->ShowTitle()?></title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

</head>
<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
    <div class="submenu">
        <div class="submenu-for-tariff">
            <ul>
                <li><a href="/for-apartment/">Для квартиры</a></li>
                <li><a href="/for_dom/">Для дома</a></li>
                <li><a href="/for_biz/">Для бизнеса</a></li>
            </ul>
        </div>
    </div>
    <?/*$APPLICATION->IncludeComponent("bitrix:menu", "bootstrap_v4", Array(
        "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
        "MAX_LEVEL" => "1",	// Уровень вложенности меню
        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
        "MENU_CACHE_TYPE" => "A",	// Тип кеширования
        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
        "MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
    ),
        false
    );*/?>




