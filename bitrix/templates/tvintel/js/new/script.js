/*-----
Spanizer
- Wraps letters with spans, for css animations
-----*/
$( document ).ready(function() {

(function($) {
    var s,
        spanizeLetters = {
            settings: {
                letters: $('.js-spanize'),
            },
            init: function() {
                s = this.settings;
                this.bindEvents();
            },
            bindEvents: function(){
                s.letters.html(function (i, el) {
                    //spanizeLetters.joinChars();
                    var spanizer = $.trim(el).split("");
                    return '<span>' + spanizer.join('</span><span>') + '</span>';
                });
            },
        };
    spanizeLetters.init();
})(jQuery);


    $(document).on('click', '.next', function () {
        $('.step1').hide();
        $('.step2').show();
        $('.pass').attr('type', 'text');

        var price = $('.log').val();
        $('.youLogin').text(price);
    });
    $(document).on('click', '.prevLink', function () {
        $('.step2').hide();
        $('.step1').show();
    });
    $(document).on('click', '._3AI0Ig', function () {
        $('.pass').attr('type', 'text');
        $('._3AI0Ig ').addClass('showPass')
    });
    $(document).on('click', '.showPass', function () {
        $('.pass').attr('type', 'password');
        $('._3AI0Ig').removeClass('showPass')
    });

    $(document).on('click', '.typeTarif', function () {
        var scrollTop = $('.tabsTarif').offset().top;
        $(document).scrollTop(scrollTop-90);
    });
    $(document).on('click', '.lkPopup .lk.th', function () {
        $('.loginPopUp').stop().slideToggle(400);
    });



    $(window).scroll(function(){
        $('.nav').toggleClass('navScroll', $(this).scrollTop() > 0);
    });

    $( ".card" ).each(function( i ) {
        setTimeout((function(){
            $(this).addClass('is-flipped');
        }).bind(this) , 500 * i)
    });


    setTimeout(function() {
        $('.tarif-items').hide();
        $('.tvBlock').show();
    }, 2000);
    $(document).on('click', '.tv', function () {
        if(!$(this).hasClass('active')){
        $('.tarif-items').hide();
        $('.tvBlock').show();
        $('.typeTarif').removeClass('active');
        $(this).addClass('active');
        $('#video source').attr('src', '/video/for-biz_tv.mp4');
        $('#video').get(0).load();
        $('#video').get(0).play();
            $('.tarif-items').slick('unslick');
            $('.tarif-items').slick({
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: false
                        }
                    }
                ]
            });
        }
    });
    $(document).on('click', '.int', function () {
        if(!$(this).hasClass('active')){
        $('.tarif-items').hide();
        $('.intBlock').show();
        $('.typeTarif').removeClass('active');
        $(this).addClass('active');
            $('.tarif-items').slick('unslick');
            $('.tarif-items').slick({
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: false
                        }
                    }
                ]
            });
        }
    });
    $(document).on('click', '.tel', function () {
        if(!$(this).hasClass('active')) {
            $('.tarif-items').hide();
            $('.telBlock').show();
            $('.typeTarif').removeClass('active');
            $(this).addClass('active');
            $('.tarif-items').slick('unslick');
            $('.tarif-items').slick({
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: false
                        }
                    }
                ]
            });
        }
    });
    $(document).on('click', '.paket', function () {
        if(!$(this).hasClass('active')) {
            $('.tarif-items').hide();
            $('.paketBlock').show();
            $('.typeTarif').removeClass('active');
            $(this).addClass('active');
            $('.tarif-items').slick('unslick');
            $('.tarif-items').slick({
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: false
                        }
                    }
                ]
            });
        }
    });


        $(document).on('click', '.popupOpen1', function () {
            $('#popup1').addClass('active');
        });
    $(document).on('click', '.popupOpen2', function () {
        $('#popup2').addClass('active');
    });
    $(document).on('click', '.popupOpen3', function () {
        $('#popup3').addClass('active');
    });
    $(document).on('click', '.popupOpen4', function () {
        $('#popup4').addClass('active');
    });
    $(document).on('click', '.popupOpen5', function () {
        $('#popup5').addClass('active');
    });
    $(document).on('click', '.popupOpen6', function () {
        $('#popup6').addClass('active');
    });
    $(document).on('click', '.popupOpen7', function () {
        $('#popup7').addClass('active');
    });
        $(document).on('click', '.popClose', function () {
            $('.allPopUp').removeClass('active');
            $('#popupRek').removeClass('active');
        });

        $('.navbar ul li:nth-child(2)').load("/submenu.html");

    var status = $('.n103 .col-sm-9 p').text();
    var status2 = $('.col-auto.m-0.p-0 ul li:nth-child(3) span').text();
    if(status === "Включено" || status2 === "Включено"){
        $('.n103 .col-sm-9 p').addClass('status_on');
        $('.col-auto.m-0.p-0 ul li:nth-child(3) span').addClass('status_on');
    }else {
        $('.n103 .col-sm-9 p').addClass('status_off');
        $('.col-auto.m-0.p-0 ul li:nth-child(3) span').addClass('status_off');
    }

    $('.step1.log').keydown(function(e) {
        if(e.keyCode === 13) {
            e.preventDefault();
            $('.next.step1').trigger('click');
            $('.step2.pass').focus();
        }
    });
});
$(window).load(function()
{
    $(".assideItem img").shiningImage();
});
