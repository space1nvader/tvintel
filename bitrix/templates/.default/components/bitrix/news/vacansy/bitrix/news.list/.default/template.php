<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
            <li class="job__it job__it--1" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
              <div class="job__info">
                <p class="job__it__ttl"><?echo $arItem["NAME"]?></p>
                <p class="job__it__sttl">Требования</p>
                <ul class="job__it__dets">
                  <li class="job__it__det"><?echo $arItem["PREVIEW_TEXT"]?></li>
                </ul>
                <p class="job__it__sttl">Обязанности</p>
                <ul class="job__it__dets">
                  <li class="job__it__det"><?echo $arItem["DETAIL_TEXT"]?></li>
                </ul>
              </div>
              <div class="job__it__salary">
                <p class="job__it__salary__valw"><?echo $arItem['PROPERTIES']['SALARY']['VALUE']?></p>
                <p class="job__it__salary__note"></p>
              </div>
            </li>

<?// echo "<pre>"; print_r($arItem); echo "</pre>";?>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</ul>

