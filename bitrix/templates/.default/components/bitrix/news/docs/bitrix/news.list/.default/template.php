<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
    $db_props = CIBlockElement::GetProperty(4, $arItem['ID'], array("sort" => "asc"));
    $ar_props = $db_props->Fetch();
    $value = $ar_props["VALUE"];
    
    $arItem['DISPLAY_PROPERTIES']['PDF'] = CIBlockFormatProperties::GetDisplayValue($arResult, $ar_props, 'doc_pdf');
    
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

    <?php if($arItem['DISPLAY_PROPERTIES']['PDF']['FILE_VALUE']['SRC']):?>
        <li class="docs__it" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <a target="_blank" class="docs__it__link" href="<?=$arItem['DISPLAY_PROPERTIES']['PDF']['FILE_VALUE']['SRC']?>">
                <p class="docs__it__pic">
                    <img class="docs__it__pic__img" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"></p>
                <div class="docs__it__info">
                    <p class="docs__it__det"><?echo $arItem["DETAIL_TEXT"];?></p>
                    <p class="docs__it__ttl"><?echo $arItem["NAME"]?></p>
                </div>
            </a>
        </li>
    <?php else: ?>
        <li class="docs__it" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <a class="docs__it__link" href="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" data-lightbox="docs">
                <p class="docs__it__pic"><img class="docs__it__pic__img" src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"></p>
                <div class="docs__it__info">
                  <p class="docs__it__det"><?echo $arItem["DETAIL_TEXT"];?></p>
                  <p class="docs__it__ttl"><?echo $arItem["NAME"]?></p>
                </div>
            </a>
        </li>
    <?php endif; ?>


<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

