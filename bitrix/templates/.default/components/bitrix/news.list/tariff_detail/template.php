<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="tv__itsw">
    <ul class="tv__its">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <li class="tv__it">
            <p class="tv__it__ttl"><?= $arItem["NAME"] ?><span class="strong"></span></p>
            <p class="tv__it__price"><span class="prc__it__price__val"><?= $arItem["PROPERTIES"]["PRICE"]["VALUE"] ?></span><span class="prc__it__price__unit"><span class="txt">руб</span><span class="line"></span><span class="txt">мес</span></span></p>
            <p class="tv__it__speed"><?= $arItem["PROPERTIES"]["DAY"]["VALUE"] ?></p>
            <div class="tv__it__desc" style="width: 100%"><?=$arItem["PROPERTIES"]["TELEPHONE"]["~VALUE"]["TEXT"]?></div>
            <p style="width: 50%"></p>
            <p class="tv__it__btw">
              <span class="prc__it__bt openModal"
                    data-modal="bid"
                    ttl="<?= $arItem["NAME"] ?>"
                    speed="<?= $arItem["PROPERTIES"]["DAY"]["VALUE"] ?>"
                    price="<?= $arItem["PROPERTIES"]["PRICE"]["VALUE"] ?> руб/мес"
                    tid="<?=($arItem["PROPERTIES"]["ID_ORIG"]["VALUE"] != '') ? $arItem["PROPERTIES"]["ID_ORIG"]["VALUE"] : 0; ?>"
              >
                    Подключить
                </span>
            </p>
        </li>
    <? endforeach; ?>
    </ul>
    
    <div class="tv__check">
        <?php if(! isset($arParams["NOT_SHOW_ADDRESSCHECK_BLOCK"])):?>
     
	    <p class="tv__check__pic">
            <img class="tv__check__pic__img" src="<?=SITE_TEMPLATE_PATH?>/img/inet-check-pic.jpg">
        </p>
        
        <div class="tv__check__formw">
            
            <p class="tv__check__ttl">Проверьте возможность подключения:</p>
            
            <form class="tv__check__form form" id="form--2" action="javascript:void(0)" method="post" data-form="2">
                
                <p class="form__item form__item--addr1">
                    <input class="form__input form__input--addr1" id="addr1--2" name="addr1" type="text" placeholder="Название улицы">
                </p>
                
                <p class="form__item form__item--addr2">
                    <input class="form__input form__input--addr2" id="addr2--2" name="addr2" type="text" placeholder="Дом">
                </p>
                
                <p class="form__item form__item--send">
                    <input class="modal__formName" name="formName" type="hidden" value="Проверить адрес">
                    <button class="form__send" type="submit">Проверить<span class="d"> адрес</span></button>
                </p>
                
            </form>
        </div>
        <?php endif; ?>
    </div>


</div>

