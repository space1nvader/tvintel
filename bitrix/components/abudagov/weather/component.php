<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */


if (!$arParams['CACHE_TIME']) {
	$arParams['CACHE_TIME'] = 6*60*60;
}

$arParams['PREG_PATTERN'] = array('~\((.*)\)~', '~\,(.*)~', '~\s*~', '~[\-\_]~');

$arResult = array();

$obCache = new CPHPCache();
if ($obCache->InitCache((30*24*60*60), serialize(SITE_ID), "/yandex-weather")) {
	$arCities = $obCache->GetVars();
} elseif ($obCache->StartDataCache()) {
	$citiesXml = simplexml_load_file('https://pogoda.yandex.ru/static/cities.xml');
	if ($citiesXml) {
		$arCities = array();

		foreach ($citiesXml as $country) {
			foreach($country->city as $city) {
				$attr = current($city->attributes());
				foreach ($attr as $k => $v) {
					$attr[$k] = iconv('UTF-8', SITE_CHARSET, $v);
				}
				$originName = iconv('UTF-8', SITE_CHARSET, (string) $city);
				$cityName = preg_replace($arParams['PREG_PATTERN'], '', strtoupper(trim($originName)));
				$partName = preg_replace($arParams['PREG_PATTERN'], '', strtoupper(trim(iconv('UTF-8', SITE_CHARSET, $attr['part']))));

				$arCities[] = array_merge(
					$attr,
					array(
						'name' => $originName,
						'search-name' => $cityName,
						'search-part' => $partName,
					)
				);
				unset($originName, $cityName);
			}
		}
	}
	$obCache->EndDataCache($arCities);
}

if (!function_exists('xmlToArray')) {
	function xmlToArray ($xml) {
		$arReturn = current($xml->attributes());
		if (count($xml) > 0 ) {
			foreach ($xml as $k => $v) {
				if (!$arReturn[$k]) {
					$arReturn[$k] = xmlToArray($v);
				} else {
					if ((is_array($arReturn[$k]) && !$arReturn[$k][0]) || !is_array($arReturn[$k])) {
						$arReturn[$k] = array(0 => $arReturn[$k]);
					}
					$arReturn[$k][] = xmlToArray($v);
				}
			}
		} else {
			$arReturn['value'] = (string) $xml;
		}
		return $arReturn;
	}
}

if (($arParams['CITY_NAME'] || $arParams['PART_NAME']) && $arCities) {

	$obCache = new CPHPCache();
	if ($obCache->InitCache($arParams['CACHE_TIME'], serialize($arParams), '/'.SITE_ID.'/'.$componentName)) {
		$arResult = $obCache->GetVars();
	} elseif ($obCache->StartDataCache()) {
		if ($arParams['CITY_NAME']) {
			$cityName = preg_replace($arParams['PREG_PATTERN'], '', strtoupper(trim($arParams['CITY_NAME'])));
		}
		if ($arParams['PART_NAME']) {
			$partName = preg_replace($arParams['PREG_PATTERN'], '', strtoupper(trim($arParams['PART_NAME'])));
		}
		foreach($arCities as $arCity) {
			if (
				(($cityName && $partName) && ($cityName == $arCity['search-name'] && $partName == $arCity['search-part']))
				|| (($cityName && !$partName) && $cityName == $arCity['search-name'])
				|| ((!$cityName && $partName) && $partName == $arCity['search-part'])
			) {
				$weatherXml = simplexml_load_file('https://export.yandex.ru/bar/reginfo.xml?region='.$arCity['region'].'&lang='.LANGUAGE_ID);
				if ($weatherXml) {
					$arResult['ITEMS'][] = array_merge(array('city' => $arCity), xmlToArray($weatherXml));
				}
			}
		}
		$obCache->EndDataCache($arResult);
	}

	if ($arResult) {
		$this->IncludeComponentTemplate();
	}
}
?>