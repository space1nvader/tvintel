<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<span id="ab-weather" class="ab-weather">
	<?$frame = $this->createFrame('ab-weather', false)->begin('');?>
	<?if ($arItem = $arResult['ITEMS'][0]) :?>
		<?if ($weather = current($arItem["weather"]['day']["day_part"])) :?>
			<span class="ab-yandex-weather" data-name="<?=$arItem['city']['part']?> <?=$arItem['city']['name']?>">
				<span class="ab-yandex-weather__img">
					<img src="<?=$weather['image-v3']['value']?>" alt="<?=$weather['type']?>" title="<?=$weather['weather_type']['value']?>" />
				</span>
				<span class="ab-yandex-weather__temp">
					<?=$weather["temperature"]["value"]?> &deg;C
				</span>
			</span>
		<?endif;?>
	<?endif;?>
	<?$frame->end();?>
</span>