<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("YANDEX_WEATHER_NAME"),
	"DESCRIPTION" => GetMessage("YANDEX_WEATHER_DESC"),
	"ICON" => "/images/yandex-weather.gif",
	"PATH" => array(
		"ID" => "service",
		"CHILD" => array(
			"ID" => "weather",
			"NAME" => GetMessage("YANDEX_WEATHER_SECTION"),
		),
	),
);

?>