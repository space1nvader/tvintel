<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"CITY_NAME" => array(
			"NAME" => GetMessage('Nazvaniegoroda'),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"PART_NAME" => array(
			"NAME" => GetMessage('Nazvanierajona'),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"CACHE_TIME"  =>  array("DEFAULT"=>36000),

	),
);
?>