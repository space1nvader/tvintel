function wdr2_get_insert_param(key, value) {
	key = encodeURI(key); value = encodeURI(value);
	var kvp = document.location.search.substr(1).split('&');
	for (var i = 0; i < kvp.length; i++) {
		if (kvp[i].length==0) {
			kvp.splice(i,1);
			i--;
		}
	}
	var i=kvp.length; var x; while(i--)  {
		x = kvp[i].split('=');
		if (x[0]==key) {
			x[1] = value;
			kvp[i] = x.join('=');
			break;
		}
	}
	if(i<0) {kvp[kvp.length] = [key,value].join('=');}
	document.location.search = kvp.join('&'); 
}
function wdr2_init_public_edit(UniqID, Messages) {
	var ScriptName = '/bitrix/tools/wd_reviews2.php';
	$('#wdr2_list_'+UniqID+' .wdr2_user_review_admin .wdr2_buttons a').click(function(Event){
		Event.preventDefault();
		var Action = $(this).attr('data-action');
		var ReviewID = $(this).attr('data-review');
		if (Action=='up') {
			$.ajax({url:ScriptName,type:'GET',data:'action=review_up&review_id='+ReviewID,success:function(HTML) {
				if (HTML.indexOf('WD_REVIEWS2_REVIEW_UPDOWN_SUCCESS')>-1) {
					wdr2_get_insert_param('wdr2_update','Y');
				}
			}});
		} else if (Action=='down') {
			$.ajax({url:ScriptName,type:'GET',data:'action=review_down&review_id='+ReviewID,success:function(HTML) {
				if (HTML.indexOf('WD_REVIEWS2_REVIEW_UPDOWN_SUCCESS')>-1) {
					wdr2_get_insert_param('wdr2_update','Y');
				}
			}});
		} else if (Action=='moderate_y') {
			$.ajax({url:ScriptName,type:'GET',data:'action=review_moderate_y&review_id='+ReviewID,success:function(HTML) {
				if (HTML.indexOf('WD_REVIEWS2_REVIEW_MODERATE_SUCCESS')>-1) {
					wdr2_get_insert_param('wdr2_update','Y');
				}
			}});
		} else if (Action=='moderate_n') {
			$.ajax({url:ScriptName,type:'GET',data:'action=review_moderate_n&review_id='+ReviewID,success:function(HTML) {
				if (HTML.indexOf('WD_REVIEWS2_REVIEW_MODERATE_SUCCESS')>-1) {
					wdr2_get_insert_param('wdr2_update','Y');
				}
			}});
		} else if (Action=='delete') {
			if (confirm(Messages['USER_DELETE_CONFIRM'])) {
				$.ajax({url:ScriptName,type:'GET',data:'action=review_delete&review_id='+ReviewID,success:function(HTML) {
					if (HTML.indexOf('WD_REVIEWS2_REVIEW_DELETE_SUCCESS')>-1) {
						wdr2_get_insert_param('wdr2_update','Y');
					}
				}});
			}
		} else if (Action=='answer') {
			$(this).parents('.wdr2_user_review_admin').find('.wdr2_answer_block').slideToggle(200);
		}
	});
	$('#wdr2_list_'+UniqID+' .wdr2_user_review_admin .wdr2_answer_block input[type=button]').click(function(Event){
		var Textarea = $(this).parents('.wdr2_answer_block').find('textarea');
		var Asnwer = Textarea.val();
		var ReviewID = $(this).attr('data-review');
		Asnwer = encodeURIComponent(Asnwer);
		$.ajax({url:ScriptName+'?action=review_answer&review_id='+ReviewID,type:'POST',data:'answer='+Asnwer,success:function(HTML) {
			if (HTML.indexOf('WD_REVIEWS2_REVIEW_ANSWER_SUCCESS')>-1) {
				wdr2_get_insert_param('wdr2_update','Y');
			}
		}});
	});
}