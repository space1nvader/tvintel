<?
$MESS['WD_REVIEWS2_NO_REVIEWS'] = 'Отзывов пока нет.';
$MESS['WD_REVIEWS2_IS_REVIEW_HELPFUL'] = 'Отзыв полезен?';

$MESS['WD_REVIEWS2_USER_ANSWER'] = 'Ответить';
$MESS['WD_REVIEWS2_USER_UP'] = 'Поднять';
$MESS['WD_REVIEWS2_USER_DOWN'] = 'Не поднимать';
$MESS['WD_REVIEWS2_USER_MODERATE_Y'] = 'Разрешить';
$MESS['WD_REVIEWS2_USER_MODERATE_N'] = 'Запретить';
$MESS['WD_REVIEWS2_USER_DELETE'] = 'Удалить';

$MESS['WD_REVIEWS2_USER_DELETE_CONFIRM'] = 'Удалить выбранный отзыв?';

$MESS['WD_REVIEWS2_USER_ASNWER_TITLE'] = 'Ответ администрации';
$MESS['WD_REVIEWS2_USER_BTN_SAVE'] = 'Сохранить ответ';
?>