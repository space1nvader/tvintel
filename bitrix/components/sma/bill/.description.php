<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("SMART_B_BL_COMPONENT_NAME"),
    "DESCRIPTION" => GetMessage("SMART_B_BL_COMPONENT_DESCRIPTION"),
    "ICON" => "/images/menu_ext.gif",
    "CACHE_PATH" => "N",
    "PATH" => array(
        "ID" => "Smart-B",
        "NAME" => "Smart-B",
        "CHILD" => array(
            "ID" => "sma_bill",
            "NAME" => GetMessage("SMART_B_BL_COMPONENT_NAME")
        )
    ),
);
?>