<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arComponentParameters = array(
    "PARAMETERS" => array(
        "AJAX_MODE" => Array(),
        "ELEMENT_ID" => Array(
            "NAME" => GetMessage("SMART_B_BL_PARAMETR_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        )
    )
);
?>