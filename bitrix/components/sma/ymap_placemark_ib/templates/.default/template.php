<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? /*/
	d($arResult,'arResult - cs');
	d($arParams,'arParams');
	//*/


if ($arParams['BASE_INFO'] == 'Y'):?>
    <div id="catalog-section-studio7">
        <? if ($arParams["DISPLAY_TOP_PAGER"]):?>
            <?= $arResult["NAV_STRING"] ?><br/>
        <? endif; ?>

        <style type="text/css">
            div#catalog-section-studio7 {
                text-align: <?=($arParams['ANONS_ALIGN'])?>;
            }

            div#catalog-section-studio7 div.anons-map-studio7 {
                overflow: hidden;
                vertical-align: top;

            <?if($arParams['ANONS_WIDTH']):?> display: inline-block;

                width: <?=$arParams['ANONS_WIDTH']?>;
                <?endif;
                if($arParams['ANONS_MARGIN']):
                    ?> margin: <?=$arParams['ANONS_MARGIN'];?>;
                <?endif;
                if($arParams['ANONS_BG_COLOR']):
                    ?> background-color: <?=$arParams['ANONS_BG_COLOR']?>;
            <?endif;
            if($arParams['ANONS_BORDER']):?> border: <?=$arParams['ANONS_BORDER']?>;
            <?endif;
            if($arParams['ANONS_RADIUS']):?> -webkit-border-radius: <?=$arParams['ANONS_RADIUS']?>;
                -moz-border-radius: <?=$arParams['ANONS_RADIUS']?>;
                border-radius: <?=$arParams['ANONS_RADIUS']?>;
            <? endif;
            if($arParams['ANONS_BOX_SHADOW']):?> -webkit-box-shadow: <?=$arParams['ANONS_BOX_SHADOW']?>;
                -moz-box-shadow: <?=$arParams['ANONS_BOX_SHADOW']?>;
                -ms-box-shadow: <?=$arParams['ANONS_BOX_SHADOW']?>;
                box-shadow: <?=$arParams['ANONS_BOX_SHADOW']?>;
            <?endif;?> <? if($arParams['ANONS_TEXT_COLOR']):?> color: <?=$arParams['ANONS_TEXT_COLOR']?>;
            <? endif;?>

            }

            div.anons-map-studio7 div.desc-studio7 {
            <?if($arParams['ANONS_PADDING']):?> margin: <?=$arParams['ANONS_PADDING']?>;
            <?endif;?> text-align: <?=($arParams['ANONS_TEXT_ALIGN'])?>;
            }

            <?if($arParams['ANONS_LINK_COLOR']):?>
            div#catalog-section-studio7 div.anons-map-studio7 a {
                color: <?=$arParams['ANONS_LINK_COLOR']?>;
            }

            <?endif;?>


        </style>
        <!--[if lt IE 8]>
        <style type="text/css">
            div#catalog-section-studio7 div.anons-map-studio7 {
                display: inline;
            }
        </style>
        <![endif]-->
        <? $mapDestinations = ''; ?>
        <? foreach ($arResult["ITEMS"] as $cell => $arElement):?>
            <?
            $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));

            ?>


            <div class="anons-map-studio7" id="<?= $this->GetEditAreaId($arElement['ID']); ?>">
                <div class="desc-studio7">
                    <? $bLink = true;
                    $detailLink = $strong = '';
                    if (in_array('ld', $arParams['LINKS_BASE']))
                        if (!$arElement['DETAIL_TEXT'])
                            $bLink = false;

                    if (in_array('b', $arParams['LINKS_BASE']))
                        //echo '<strong>'.$arElement['ID'].' '.intval(in_array('b',$arParams['LINKS_BASE']));
                        $strong = '<strong  id="forMap_' . $arElement['ID'] . '" class="name-placemark-studio7">';

                    if (in_array('a', $arParams['LINKS_BASE'])
                        && $arElement['DETAIL_PAGE_URL']
                        && !in_array('ad', $arParams['LINKS_BASE'])
                        && $bLink
                    )
                        $detailLink .= '<a ' . (!in_array('b', $arParams['LINKS_BASE'])
                                ? 'id="forMap_' . $arElement['ID'] . '"'
                                : '') .
                            ' class="name-placemark-studio7" href="' . $arElement['DETAIL_PAGE_URL'] . '">';
                    ?>

                    <? if (is_array($arElement["PREVIEW_PICTURE"]) && in_array('PREVIEW_PICTURE', $arParams['FIELD_CODE_BASE'])):?>
                        <!-- 						<div class="photo-studio7"> -->

                        <?= $detailLink;
                        $source = $arElement["PREVIEW_PICTURE"];
                        if (is_array($arElement["PREVIEW_PICTURE"]['BASE']) && !empty($arElement["PREVIEW_PICTURE"]))
                            $source = $arElement["PREVIEW_PICTURE"]['BASE'];
                        ?><img align="left" border="0" src="<?= $source["SRC"]
                        ?>" width="<?= $source["WIDTH"]
                        ?>" height="<?= $source["HEIGHT"]
                        ?>" alt="<?= $arElement["NAME"] ?>" title="<?= $arElement["NAME"] ?>" /><?
                        if ($detailLink) echo '</a>'; ?>
                        <!-- 						</div> -->
                    <?
                    elseif (
                        is_array($arElement["DETAIL_PICTURE"])
                        && (in_array('DETAIL_PICTURE', $arParams['FIELD_CODE_BASE'])
                            || in_array('PREVIEW_PICTURE', $arParams['FIELD_CODE_BASE'])
                        )
                    ):?>

                        <?= $detailLink;
                        $source = $arElement["DETAIL_PICTURE"];
                        if (is_array($arElement["DETAIL_PICTURE"]['BASE']) && !empty($arElement["DETAIL_PICTURE"]))
                            $source = $arElement["DETAIL_PICTURE"]['BASE'];
                        ?><img align="left"  border="0" src="<?= $source["SRC"]
                        ?>" width="<?= $source["WIDTH"]
                        ?>" height="<?= $source["HEIGHT"]
                        ?>" alt="<?= $arElement["NAME"]
                        ?>" title="<?= $arElement["NAME"] ?>" /><?
                        if ($detailLink) echo '</a>'; ?>

                    <?endif ?>


                    <? foreach ($arParams['FIELD_CODE_BASE'] as $fieldCode)
                        switch ($fieldCode) {
                            case    'NAME':
                                ?>
                                <p><?= $strong . $detailLink . $arElement["NAME"] . ($detailLink ? '</a>' : '') . ($strong ? '</strong>' : ''); ?></p>
                                <? break;
                            case    'PREVIEW_PICTURE':
                            case    'DETAIL_PICTURE':
                                break;
                            default:
                                echo $arElement[$fieldCode] . ($arElement[$fieldCode] ? '<br/>' : ''); ?>
                            <?
                        } ?>


                    <? foreach ($arElement["DISPLAY_PROPERTIES"] as $pid => $arProperty)
                        if (in_array($arProperty['CODE'], $arParams['PROPERTY_CODE_BASE'])):?>
                            <?= $arProperty["NAME"] ?>:&nbsp;<?
                            if (is_array($arProperty["DISPLAY_VALUE"]))
                                echo implode($arParams['DELIMETER_MULTI'], $arProperty["DISPLAY_VALUE"]);
                            else
                                echo $arProperty["DISPLAY_VALUE"]; ?><br/>
                        <?endif; ?>
                    <br/>
                    <? if (
                        ($arElement['DETAIL_PAGE_URL']
                            && in_array('ad', $arParams['LINKS_BASE'])
                            && $bLink)
                        ||
                        ($bLink
                            && $arElement['DETAIL_PAGE_URL']
                            && $arParams['ANIMATE_CLICK'])
                    ):?>
                        <a href="<?= $arElem['DETAIL_PAGE_URL'] ?>"><?= $arParams['LINKS_MAP_TEXT'] ?></a>
                    <?endif; ?>


                    <? if (is_array($arElement["OFFERS"]) && !empty($arElement["OFFERS"])):?>
                        <? foreach ($arElement["OFFERS"] as $arOffer):?>
                            <? foreach ($arParams["OFFERS_FIELD_CODE"] as $field_code):?>
                                <small><? echo GetMessage("IBLOCK_FIELD_" . $field_code) ?>:&nbsp;<?
                                    echo $arOffer[$field_code]; ?></small><br/>
                            <?endforeach; ?>
                            <? foreach ($arOffer["DISPLAY_PROPERTIES"] as $pid => $arProperty):?>
                                <small><?= $arProperty["NAME"] ?>:&nbsp;<?
                                    if (is_array($arProperty["DISPLAY_VALUE"]))
                                        echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
                                    else
                                        echo $arProperty["DISPLAY_VALUE"]; ?></small><br/>
                            <?endforeach ?>
                            <? foreach ($arOffer["PRICES"] as $code => $arPrice):?>
                                <? if ($arPrice["CAN_ACCESS"]):?>
                                    <p><?= $arResult["PRICES"][$code]["TITLE"]; ?>:&nbsp;&nbsp;
                                        <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                                            <s><?= $arPrice["PRINT_VALUE"] ?></s> <span
                                                class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
                                        <?
                                        else:?>
                                            <span class="catalog-price"><?= $arPrice["PRINT_VALUE"] ?></span>
                                        <?endif ?>
                                    </p>
                                <?endif; ?>
                            <?endforeach; ?>
                            <p>
                            <? if ($arParams["DISPLAY_COMPARE"]):?>
                                <noindex>
                                    <a href="<? echo $arOffer["COMPARE_URL"] ?>"
                                       rel="nofollow"><? echo GetMessage("CATALOG_COMPARE") ?></a>&nbsp;
                                </noindex>
                            <?endif ?>
                            <? if ($arOffer["CAN_BUY"]):?>
                                <? if ($arParams["USE_PRODUCT_QUANTITY"]):?>
                                    <form action="<?= POST_FORM_ACTION_URI ?>" method="post"
                                          enctype="multipart/form-data">

                                        <div class="quantity-studio7">
                                            <?= GetMessage("CT_BCS_QUANTITY") ?>:
                                            <input type="text" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"] ?>"
                                                   value="1" size="5">
                                            <input type="hidden" name="<? echo $arParams["ACTION_VARIABLE"] ?>"
                                                   value="BUY">
                                            <input type="hidden" name="<? echo $arParams["PRODUCT_ID_VARIABLE"] ?>"
                                                   value="<? echo $arOffer["ID"] ?>">
                                            <input type="submit" name="<? echo $arParams["ACTION_VARIABLE"] . "BUY" ?>"
                                                   value="<? echo GetMessage("CATALOG_BUY") ?>">
                                            <input type="submit"
                                                   name="<? echo $arParams["ACTION_VARIABLE"] . "ADD2BASKET" ?>"
                                                   value="<? echo GetMessage("CATALOG_ADD") ?>">
                                        </div>
                                    </form>
                                <?
                                else:?>
                                    <noindex>
                                        <a href="<? echo $arOffer["BUY_URL"] ?>"
                                           rel="nofollow"><? echo GetMessage("CATALOG_BUY") ?></a>
                                        &nbsp;<a href="<? echo $arOffer["ADD_URL"] ?>"
                                                 rel="nofollow"><? echo GetMessage("CATALOG_ADD") ?></a>
                                    </noindex>
                                <?endif; ?>
                            <?
                            elseif (count($arResult["PRICES"]) > 0):?>
                                <?= GetMessage("CATALOG_NOT_AVAILABLE") ?>
                            <?endif ?>
                            </p>
                        <?endforeach; ?>
                    <?
                    else:?>
                        <? foreach ($arElement["PRICES"] as $code => $arPrice):?>
                            <? if ($arPrice["CAN_ACCESS"]):?>
                                <p><?= $arResult["PRICES"][$code]["TITLE"]; ?>:&nbsp;&nbsp;
                                    <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                                        <s><?= $arPrice["PRINT_VALUE"] ?></s> <span
                                            class="catalog-price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
                                    <?
                                    else:?><span class="catalog-price"><?= $arPrice["PRINT_VALUE"] ?></span><?endif; ?>
                                </p>
                            <?endif; ?>
                        <?endforeach; ?>
                        <? if (is_array($arElement["PRICE_MATRIX"])):?>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="data-table">
                                <thead>
                                <tr>
                                    <? if (count($arElement["PRICE_MATRIX"]["ROWS"]) >= 1 && ($arElement["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_FROM"] > 0 || $arElement["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_TO"] > 0)):?>
                                        <td valign="top" nowrap><?= GetMessage("CATALOG_QUANTITY") ?></td>
                                    <?endif ?>
                                    <? foreach ($arElement["PRICE_MATRIX"]["COLS"] as $typeID => $arType):?>
                                        <td valign="top" nowrap><?= $arType["NAME_LANG"] ?></td>
                                    <?endforeach ?>
                                </tr>
                                </thead>
                                <? foreach ($arElement["PRICE_MATRIX"]["ROWS"] as $ind => $arQuantity):?>
                                    <tr>
                                        <? if (count($arElement["PRICE_MATRIX"]["ROWS"]) > 1 || count($arElement["PRICE_MATRIX"]["ROWS"]) == 1 && ($arElement["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_FROM"] > 0 || $arElement["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_TO"] > 0)):?>
                                            <th nowrap><?
                                                if (IntVal($arQuantity["QUANTITY_FROM"]) > 0 && IntVal($arQuantity["QUANTITY_TO"]) > 0)
                                                    echo str_replace("#FROM#", $arQuantity["QUANTITY_FROM"], str_replace("#TO#", $arQuantity["QUANTITY_TO"], GetMessage("CATALOG_QUANTITY_FROM_TO")));
                                                elseif (IntVal($arQuantity["QUANTITY_FROM"]) > 0)
                                                    echo str_replace("#FROM#", $arQuantity["QUANTITY_FROM"], GetMessage("CATALOG_QUANTITY_FROM"));
                                                elseif (IntVal($arQuantity["QUANTITY_TO"]) > 0)
                                                    echo str_replace("#TO#", $arQuantity["QUANTITY_TO"], GetMessage("CATALOG_QUANTITY_TO"));
                                                ?></th>
                                        <?endif ?>
                                        <? foreach ($arElement["PRICE_MATRIX"]["COLS"] as $typeID => $arType):?>
                                            <td><?
                                                if ($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"] < $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"]):?>
                                                    <s><?= FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]) ?></s>
                                                    <span
                                                        class="catalog-price"><?= FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]); ?></span>
                                                <?
                                                else:?>
                                                    <span
                                                        class="catalog-price"><?= FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]); ?></span>
                                                <?endif ?>&nbsp;
                                            </td>
                                        <?endforeach ?>
                                    </tr>
                                <?endforeach ?>
                            </table><br/>
                        <?endif ?>
                        <? if ($arParams["DISPLAY_COMPARE"]):?>
                            <noindex>
                                <a href="<? echo $arElement["COMPARE_URL"] ?>"
                                   rel="nofollow"><? echo GetMessage("CATALOG_COMPARE") ?></a>&nbsp;
                            </noindex>
                        <?endif ?>
                        <? if ($arElement["CAN_BUY"]):?>
                            <? if ($arParams["USE_PRODUCT_QUANTITY"] || count($arElement["PRODUCT_PROPERTIES"])):?>
                                <form action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data">

                                    <? if ($arParams["USE_PRODUCT_QUANTITY"]):?>
                                        <div>
                                            <label><?= GetMessage("CT_BCS_QUANTITY") ?>:
                                                <input type="text"
                                                       name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"] ?>"
                                                       value="1" size="5">
                                            </label>

                                        </div>
                                    <?endif; ?>
                                    <? foreach ($arElement["PRODUCT_PROPERTIES"] as $pid => $product_property):?>
                                        <div class="basket-props-studio7" style="overflow:hidden">
                                            <div
                                                style="float:left; margin-right:10px"><?= $arElement["PROPERTIES"][$pid]["NAME"] ?>
                                                :
                                            </div>
                                            <? if (
                                                $arElement["PROPERTIES"][$pid]["PROPERTY_TYPE"] == "L"
                                                && $arElement["PROPERTIES"][$pid]["LIST_TYPE"] == "C"
                                            ):?>
                                                <? foreach ($product_property["VALUES"] as $k => $v):?>
                                                    <label><input type="radio"
                                                                  name="<? echo $arParams["PRODUCT_PROPS_VARIABLE"] ?>[<? echo $pid ?>]"
                                                                  value="<? echo $k ?>" <? if ($k == $product_property["SELECTED"]) echo '"checked"' ?>><? echo $v ?>
                                                    </label><br>
                                                <?endforeach; ?>
                                            <?
                                            else:?>
                                                <select
                                                    name="<? echo $arParams["PRODUCT_PROPS_VARIABLE"] ?>[<? echo $pid ?>]">
                                                    <? foreach ($product_property["VALUES"] as $k => $v):?>
                                                        <option
                                                            value="<? echo $k ?>" <? if ($k == $product_property["SELECTED"]) echo '"selected"' ?>><? echo $v ?></option>
                                                    <?endforeach; ?>
                                                </select>
                                            <?endif; ?>

                                        </div>
                                    <?endforeach; ?>

                                    <input type="hidden" name="<? echo $arParams["ACTION_VARIABLE"] ?>" value="BUY">
                                    <input type="hidden" name="<? echo $arParams["PRODUCT_ID_VARIABLE"] ?>"
                                           value="<? echo $arElement["ID"] ?>">
                                    <input type="submit" name="<? echo $arParams["ACTION_VARIABLE"] . "BUY" ?>"
                                           value="<? echo GetMessage("CATALOG_BUY") ?>">
                                    <input type="submit" name="<? echo $arParams["ACTION_VARIABLE"] . "ADD2BASKET" ?>"
                                           value="<? echo GetMessage("CATALOG_ADD") ?>">
                                </form>
                            <?
                            else:?>
                                <noindex>
                                    <a href="<? echo $arElement["BUY_URL"] ?>"
                                       rel="nofollow"><? echo GetMessage("CATALOG_BUY") ?></a>&nbsp;<a
                                        href="<? echo $arElement["ADD_URL"] ?>"
                                        rel="nofollow"><? echo GetMessage("CATALOG_ADD") ?></a>
                                </noindex>
                            <?endif; ?>
                        <?
                        elseif ((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])):?>
                            <?= GetMessage("CATALOG_NOT_AVAILABLE") ?>
                        <?endif ?>
                    <?endif ?>
                    &nbsp;
                </div>
            </div>
            <? $cell++;

        endforeach; // foreach($arResult["ITEMS"] as $arElement):
        ?>


        <? if ($arParams["DISPLAY_BOTTOM_PAGER"]):?>
            <br/><?= $arResult["NAV_STRING"] ?>
        <? endif; ?>
        <br/>
    </div>
<? endif; ?>

<? /////////////////////////////////////////////MAP////////////////////////////////////////////////

if (!is_array($arParams['PROPERTY_CODE']))
    $arParams['PROPERTY_CODE'] = array();
$arParams['DISPLAY_FIELDS_MAP'] = array_merge($arParams['DISPLAY_FIELDS_MAP'], $arParams['PROPERTY_CODE']);


foreach ($arResult['ITEMS'] as $keyElem => $arElem) {
    if (!$arElem['PROPERTIES'][$arParams['PROPERTY_COORDINATE']]['VALUE']
        && !$arElem[$arParams['PROPERTY_COORDINATE']]
    )//main prop(field) for map
        continue;
    $desc = $link = '';
    foreach ($arParams['DISPLAY_FIELDS_MAP'] as $key => $field)            //make description place
    {
        if ($field == $arParams['PROPERTY_COORDINATE'])
            continue;

        if ($arParams['LINKS_MAP'] && $key == 0)                            //links in description
        {
            $bLink = true;
            if (in_array('ld', $arParams['LINKS_MAP']))
                if (!$arElem['DETAIL_TEXT'])
                    $bLink = false;

            if (in_array('b', $arParams['LINKS_MAP']))
                $desc .= '<strong>';

            if (in_array('a', $arParams['LINKS_MAP'])
                && $arElem['DETAIL_PAGE_URL']
                && !in_array('ad', $arParams['LINKS_MAP'])
                && $bLink
            )
                $desc .= '<a href="' . $arElem['DETAIL_PAGE_URL'] . '">';
        }


        if (!in_array($field, $arParams['PROPERTY_CODE']))                //field of elem
            $source = $arElem[$field];
        else                                                            //propery
            $source = $arElem['DISPLAY_PROPERTIES'][$field]['VALUE'];

        if (!$source)
            continue;

        if ($arParams['DISPLAY_PROPERTY_KEYS'] == 'Y')                        //show names property
            if ($arElem['PROPERTIES'][$field]['NAME'])
                $desc .= $arElem['PROPERTIES'][$field]['NAME'] . ': ';


        if (is_array($source) && strpos($field, 'PICTURE') === false)        //multi props
        {
            if ($field !== 'PRICE') {
                foreach ($source as $keySource => $value)
                    $desc .= $value . $arParams['DELIMETER_MULTI'];

                $desc = substr($desc, 0, -1 * strlen($arParams['DELIMETER_MULTI']));
                $desc .= "\r\n";
            } else {                                                        //price on map
                foreach ($arElement["PRICES"] as $code => $arPrice)
                    if ($arPrice["CAN_ACCESS"]) {

                        $desc .= '<p>';
                        if ($arParams['DISPLAY_PROPERTY_KEYS'] == 'Y')                        //show price name
                            $desc .= $arResult["PRICES"][$code]["TITLE"] . ':&nbsp;';
                        if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"])
                            $desc .= '<s>' . $arPrice["PRINT_VALUE"] . '</s><span class="catalog-price ymap">' . $arPrice["PRINT_DISCOUNT_VALUE"] . '</span>';
                        else
                            $desc .= '<span class="catalog-price ymap">' . $arPrice["PRINT_VALUE"] . '</span>';

                        $desc .= '</p>';
                    }

            }
        } elseif (strpos($field, 'PICTURE') === false) {
            if ($arElem["DISPLAY_PROPERTIES"][$field]['MAP'])            //file properties (images)
                $desc .= ($bLink ? '<a href="' . $arElem['DETAIL_PAGE_URL'] . '">' : '') .
                    '<img align="left" src="' . (is_array($arElem["DISPLAY_PROPERTIES"][$field]['MAP'])
                        ? $arElem["DISPLAY_PROPERTIES"][$field]['MAP']['SRC']
                        : $arElem["DISPLAY_PROPERTIES"][$field]['SRC']) . '" alt="' . $arElem['NAME'] . '"/>' .
                    ($bLink ? '</a>' : '');
            else
                $desc .= $source . "\r\n";

        } else
            $desc .= ($bLink ? '<a href="' . $arElem['DETAIL_PAGE_URL'] . '">' : '') .
                '<img align="left" class="icon-ymap-studio7" src="' . (is_array($source['MAP'])
                    ? $source['MAP']['SRC']
                    : $source['SRC']) . '" alt="' . $arElem['NAME'] . '"/>' .
                ($bLink ? '</a>' : '');


        if ($arParams['LINKS_MAP'] && $key == 0) {
            if (in_array('b', $arParams['LINKS_MAP']))
                $desc .= '</strong>';

            if ($bLink)
                if (in_array('a', $arParams['LINKS_MAP'])
                    && $arElem['DETAIL_PAGE_URL']
                    && !in_array('ad', $arParams['LINKS_MAP'])
                    && $bLink
                )
                    $desc .= '</a>';
                elseif (
                    $arElem['DETAIL_PAGE_URL']
                    && in_array('ad', $arParams['LINKS_MAP'])
                    && $bLink
                )
                    $link = "\r\n<a href=\"" . $arElem['DETAIL_PAGE_URL'] . "\">" . $arParams['LINKS_MAP_TEXT'] . "</a>";
            if ($bLink)
                $desc .= "\r\n";
        }


    }


    $desc .= $link;

    if (!in_array($arParams['PROPERTY_COORDINATE'], $arParams['PROPERTY_CODE']))                        //if coordinate in field
        $arCoordinate = explode(',', $arElem[$arParams['PROPERTY_COORDINATE']]);
    else
        $arCoordinate = explode(',', $arElem['PROPERTIES'][$arParams['PROPERTY_COORDINATE']]['VALUE']);
    //echo $keyElem.' : '.$desc.' '.$arCoordinate[0].',  '.$arCoordinate[1].'<br>';
    if (is_array($arCoordinate) && count($arCoordinate) > 1)                                                                        //save place
    {
        $arResult['POSITION']['PLACEMARKS'][$arElem['ID']]['LAT'] = $arCoordinate[0];
        $arResult['POSITION']['PLACEMARKS'][$arElem['ID']]['LON'] = $arCoordinate[1];
        $arResult['POSITION']['PLACEMARKS'][$arElem['ID']]['DESC'] = $desc;
        if ($arParams['PROPERTY_COORDINATE'] == 'NAME' && count($arCoordinate) > 2)
            $arResult['POSITION']['PLACEMARKS'][$arElem['ID']]['NAME'] = $arCoordinate[2];
        else
            $arResult['POSITION']['PLACEMARKS'][$arElem['ID']]['NAME'] = $arResult['ITEMS'][$keyElem]['NAME'];


    }


} ?>
<!--<pre>-->
<? ////d($arResult['POSITION'],'position');
//	print_r($arParams)
//?>
<!--	</pre>-->
<script type="text/javascript">
    //#TODO many  maps on one page
    if (typeof arPlaceMarkVea == 'undefined')
        var arPlaceMarkVea = {};

    var delayFlyingVea =<?=intval($arParams['ANIMATE_CLICK'])?>;
    <?foreach ($arResult['POSITION']['PLACEMARKS'] as $placeMarkID=>    $destination):?>
    arPlaceMarkVea['forMap_<?=$placeMarkID?>'] = {
        coord: [<?=$destination['LAT']?>,<?=$destination['LON']?>],
        desc: "<?=preg_replace("/\r?\n/", "<br />", addslashes($destination['DESC']))?>"

        <?if($arParams['NAMES_BALLOON'] == 'Y'){?>
        , name: "<?=addslashes($destination['NAME'])?>"
        <?}?>
    };
    <?endforeach;?>

    <?if(is_array($arParams['ICON_PLACEMARK']) && $arParams['ICON_PLACEMARK']['src']){?>
    var mapOptionsVea = {
        balloonCloseButton: true,
        iconImageHref: '<?=$arParams['ICON_PLACEMARK']['src']?>',
        iconImageSize: [<?=intval($arParams['ICON_PLACEMARK']['width'])?>,
            <?=intval($arParams['ICON_PLACEMARK']['height'])?>],
        iconImageOffset: [<?=-1 * intval(round($arParams['ICON_PLACEMARK']['width'] / 2))?>,
            <?=-1 * intval(round($arParams['ICON_PLACEMARK']['height'] / 2))?>]
    }


    <?}
    elseif($arParams['ICON_PLACEMARK']){?>
    var mapOptionsVea = {
        balloonCloseButton: true,
        preset: '<?=$arParams['ICON_PLACEMARK']?>'
    };
    <?}
    else{?>
    var mapOptionsVea = {
        balloonCloseButton: true
    };
    <?}


    if(trim($arParams['ANIMATE_BODY']) != ''):?>
    var delayBodyAnimateVea =<?=intval($arParams['ANIMATE_BODY'])?>;
    <? endif;?>


    <?if (trim($arParams['ONMAPREADY']) != ''):?>
    if (typeof window.<?=$arParams['ONMAPREADY']?> == 'function')
        var mapLoaderCallbackVea = window.<?=$arParams['ONMAPREADY']?>;
    <?endif;?>



    BX_SetPlacemarks_<?=$arParams['MAP_ID']?>= mapLoaderCallbackMainVea;
</script>



