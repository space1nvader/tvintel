var get$ = function () {
    var oXML = false;
    var doneLoading = function (oXML, src) {
        if (oXML.readyState == 4) {
            if (oXML.status == 200)		//#FIXED
            {
                eval(oXML.responseText);
            }
            else
                console.log('Error in loadding jQuery by script-studio7.js! May be file ' + src + ' not exist?');
        }
        else;
        //studio7.d('Loadding jQuery...');
    }

    try {	//opera
        oXML = new XMLHttpRequest();
    }
    catch (err) //	ie
    {
        var activeXObjects = new array('Msxml2.XMLHTTP.6.0', 'Msxml2.XMLHTTP.5.0', 'Msxml2.XMLHTTP.4.0',
            'Msxml2.XMLHTTP.3.0', 'Msxml2.XMLHTTP', 'Microsoft.XMLHTTP');

        for (var i = 0; i < activeXObjects.length; i++)
            try {
                oXML = new ActiveXObject(activeXObjects[i]);
            }
            catch (another) {
            }

    }

    if (oXML != false) {
        var src = '/bitrix/components/studio7/ymap_placemark_ib/get$.php'

        oXML.open("GET", src, false);
        oXML.onreadystatechange = function () {
            doneLoading(oXML, src);
        }
        oXML.send(null);

    }
    else {
        alert('Произошла ошибка, неудалось загрузить jQuery, попробуйте обновить страницу! ' +
            'Если ошибка будет повторяться, обратитесь к администраторам сайта, пожалуйста.');
    }

};

var setFlyingMapVea = function (map, objects) {

    //	studio7.d('objects',mapDestinations);
    if (typeof map != 'undefined') {
        if (typeof $ == 'undefined')
            get$();

        var offsetMapY = $('div.ymap-studio7').offset().top;


        if (typeof arPlaceMarkVea != 'undefined') {
            $('div#catalog-section-studio7 strong.name-placemark-studio7,div#catalog-section-studio7 a.name-placemark-studio7').click(function () {

                var index = $(this).attr('id');

                //studio7.d(window.mapDestinations,index,window.mapDestinations[index],window.mapDestinations.for_map29);
                var flying = function () {
                    map.panTo(arPlaceMarkVea[index].coord, {flying: 1, duration: delayFlyingVea, delay: 0});
                    //#FIXME open balloon
                    map.balloon.open(arPlaceMarkVea[index].coord, {
                            content: arPlaceMarkVea[index].desc
                        },
                        {closeButton: true}
                    );

                };

                if (typeof delayBodyAnimateVea != 'undefined')
                    $('body,html').animate({
                            scrollTop: (offsetMapY - 200) + 'px'
                        },
                        delayBodyAnimateVea,
                        flying
                    );
                else
                    flying();

                return false;

            })

        }

    }
};

var mapLoaderCallbackMainVea = function (map) {
    if (typeof arPlaceMarkVea != 'undefined') {
        //studio7.d('arPlaceMarkVea',arPlaceMarkVea);

        for (var id in arPlaceMarkVea) {
            var obPlacemark = new ymaps.Placemark(
                arPlaceMarkVea[id].coord,
                {
                    balloonContent: arPlaceMarkVea[id].desc,
                    hintContent: arPlaceMarkVea[id].name,
                    iconContent: arPlaceMarkVea[id].name

                },
                mapOptionsVea
            );

            map.geoObjects.add(obPlacemark);
        }
        if (typeof delayFlyingVea != 'undefined')
            setFlyingMapVea(map);
    }
    if (typeof mapLoaderCallbackVea != 'undefined')
        mapLoaderCallbackVea(map, arPlaceMarkVea);

}


if (!window.BX_YMapAddPlacemark) {
    window.BX_YMapAddPlacemark = function (map, arPlacemark) {
        if (null == map)
            return false;

        if (!arPlacemark.LAT || !arPlacemark.LON)
            return false;

        var props = {};
        if (null != arPlacemark.TEXT && arPlacemark.TEXT.length > 0) {
            var value_view = '';

            if (arPlacemark.TEXT.length > 0) {
                var rnpos = arPlacemark.TEXT.indexOf("\n");
                value_view = rnpos <= 0 ? arPlacemark.TEXT : arPlacemark.TEXT.substring(0, rnpos);
            }

            props.balloonContent = arPlacemark.TEXT.replace(/\n/g, '<br />');
            props.hintContent = value_view;
        }

        var obPlacemark = new ymaps.Placemark(
            [arPlacemark.LAT, arPlacemark.LON],
            props,
            {balloonCloseButton: true}
        );

        map.geoObjects.add(obPlacemark);

        return obPlacemark;
    }
}

if (!window.BX_YMapAddPolyline) {
    window.BX_YMapAddPolyline = function (map, arPolyline) {
        if (null == map)
            return false;

        if (null != arPolyline.POINTS && arPolyline.POINTS.length > 1) {
            var arPoints = [];
            for (var i = 0, len = arPolyline.POINTS.length; i < len; i++) {
                arPoints.push([arPolyline.POINTS[i].LAT, arPolyline.POINTS[i].LON]);
            }
        }
        else {
            return false;
        }

        var obParams = {clickable: true};
        if (null != arPolyline.STYLE) {
            obParams.strokeColor = arPolyline.STYLE.strokeColor;
            obParams.strokeWidth = arPolyline.STYLE.strokeWidth;
        }
        var obPolyline = new ymaps.Polyline(
            arPoints, {balloonContent: arPolyline.TITLE}, obParams
        );

        map.geoObjects.add(obPolyline);

        return obPolyline;
    }
}