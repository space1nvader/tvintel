<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule("iblock"))
    return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE" => "Y"));
while ($arr = $rsIBlock->Fetch())
    $arIBlock[$arr["ID"]] = "[" . $arr["ID"] . "] " . $arr["NAME"];

$arProperty_LNS = array();
$arProperty_N = array();
$arProperty_X = array();
if (0 < intval($arCurrentValues['IBLOCK_ID'])) {
    $rsProp = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("IBLOCK_ID" => $arCurrentValues["IBLOCK_ID"], "ACTIVE" => "Y"));
    while ($arr = $rsProp->Fetch()) {
        if ($arr["PROPERTY_TYPE"] != "F")
            $arProperty[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];

        if ($arr["PROPERTY_TYPE"] == "N")
            $arProperty_N[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];

        if ($arr["PROPERTY_TYPE"] != "F") {
            if ($arr["MULTIPLE"] == "Y")
                $arProperty_X[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
            elseif ($arr["PROPERTY_TYPE"] == "L")
                $arProperty_X[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
            elseif ($arr["PROPERTY_TYPE"] == "E" && $arr["LINK_IBLOCK_ID"] > 0)
                $arProperty_X[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
        }
    }
}

$arProperty_UF = array();
$arSProperty_LNS = array();
$arUserFields = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("IBLOCK_" . $arCurrentValues["IBLOCK_ID"] . "_SECTION");
foreach ($arUserFields as $FIELD_NAME => $arUserField) {
    $arProperty_UF[$FIELD_NAME] = $arUserField["LIST_COLUMN_LABEL"] ? $arUserField["LIST_COLUMN_LABEL"] : $FIELD_NAME;
    if ($arUserField["USER_TYPE"]["BASE_TYPE"] == "string")
        $arSProperty_LNS[$FIELD_NAME] = $arProperty_UF[$FIELD_NAME];
}

$arOffers = CIBlockPriceTools::GetOffersIBlock($arCurrentValues["IBLOCK_ID"]);
$OFFERS_IBLOCK_ID = is_array($arOffers) ? $arOffers["OFFERS_IBLOCK_ID"] : 0;
$arProperty_Offers = array();
if ($OFFERS_IBLOCK_ID) {
    $rsProp = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("IBLOCK_ID" => $OFFERS_IBLOCK_ID, "ACTIVE" => "Y"));
    while ($arr = $rsProp->Fetch()) {
        if ($arr["PROPERTY_TYPE"] != "F")
            $arProperty_Offers[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
    }
}

$arPrice = array();
if (CModule::IncludeModule("catalog")) {
    $rsPrice = CCatalogGroup::GetList($v1 = "sort", $v2 = "asc");
    while ($arr = $rsPrice->Fetch()) $arPrice[$arr["NAME"]] = "[" . $arr["NAME"] . "] " . $arr["NAME_LANG"];
} else {
    $arPrice = $arProperty_N;
}

$arAscDesc = array(
    "asc" => GetMessage("IBLOCK_SORT_ASC"),
    "desc" => GetMessage("IBLOCK_SORT_DESC"),
);

$arComponentParameters = array(
    "GROUPS" => array(
        "PREVIEW_ELEMENTS" => array(
            "NAME" => GetMessage('PREVIEW_ELEMENTS'),
        ),
        "PRICES" => array(
            "NAME" => GetMessage("IBLOCK_PRICES"),
        ),

        "MAP" => array(
            "NAME" => GetMessage('MAP')
        )
    ),
    "PARAMETERS" => array(
        "AJAX_MODE" => array(),
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_IBLOCK"),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
        ),
        "SECTION_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_SECTION_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => '={$_REQUEST["SECTION_ID"]}',
        ),
        "SECTION_CODE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("IBLOCK_SECTION_CODE"),
            "TYPE" => "STRING",
            "DEFAULT" => '',
        ),
        "SECTION_USER_FIELDS" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("CP_BCS_SECTION_USER_FIELDS"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arProperty_UF,
        ),

        "FILTER_NAME" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("IBLOCK_FILTER_NAME_IN"),
            "TYPE" => "STRING",
            "DEFAULT" => "arrFilter",
        ),
        "INCLUDE_SUBSECTIONS" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("CP_BCS_INCLUDE_SUBSECTIONS"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "SHOW_ALL_WO_SECTION" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("CP_BCS_SHOW_ALL_WO_SECTION"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "SECTION_URL" => CIBlockParameters::GetPathTemplateParam(
            "SECTION",
            "SECTION_URL",
            GetMessage("IBLOCK_SECTION_URL"),
            "",
            "URL_TEMPLATES"
        ),
        "DETAIL_URL" => CIBlockParameters::GetPathTemplateParam(
            "DETAIL",
            "DETAIL_URL",
            GetMessage("IBLOCK_DETAIL_URL"),
            "",
            "URL_TEMPLATES"
        ),

        "USE_BASKET" => array(
            "PARENT" => "URL_TEMPLATES",
            "NAME" => GetMessage('USE_BASKET'),
            "TYPE" => "CHECKBOX",
            "REFRESH" => "Y"
        ),
        "META_KEYWORDS" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("T_IBLOCK_DESC_KEYWORDS"),
            "TYPE" => "LIST",
            "DEFAULT" => "-",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => array_merge(Array("-" => " "), $arSProperty_LNS),
        ),
        "META_DESCRIPTION" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("T_IBLOCK_DESC_DESCRIPTION"),
            "TYPE" => "LIST",
            "DEFAULT" => "-",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => array_merge(Array("-" => " "), $arSProperty_LNS),
        ),
        "BROWSER_TITLE" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("CP_BCS_BROWSER_TITLE"),
            "TYPE" => "LIST",
            "MULTIPLE" => "N",
            "DEFAULT" => "-",
            "VALUES" => array_merge(Array("-" => " ", "NAME" => GetMessage("IBLOCK_FIELD_NAME")), $arSProperty_LNS),
        ),
        "ADD_SECTIONS_CHAIN" => Array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("CP_BCS_ADD_SECTIONS_CHAIN"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "DISPLAY_COMPARE" => Array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("T_IBLOCK_DESC_DISPLAY_COMPARE"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "SET_TITLE" => Array(),
        "SET_STATUS_404" => Array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("CP_BCS_SET_STATUS_404"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "PAGE_ELEMENT_COUNT" => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("IBLOCK_PAGE_ELEMENT_COUNT"),
            "TYPE" => "STRING",
            "DEFAULT" => "30",
            "REFRESH" => 'Y'
        ),

        'INIT_MAP_TYPE' => array(
            'NAME' => GetMessage('MYMS_PARAM_INIT_MAP_TYPE'),
            'TYPE' => 'LIST',
            'VALUES' => array(
                'MAP' => GetMessage('MYMS_PARAM_INIT_MAP_TYPE_MAP'),
                'SATELLITE' => GetMessage('MYMS_PARAM_INIT_MAP_TYPE_SATELLITE'),
                'HYBRID' => GetMessage('MYMS_PARAM_INIT_MAP_TYPE_HYBRID'),
                'PUBLIC' => GetMessage('MYMS_PARAM_INIT_MAP_TYPE_PUBLIC'),
                'PUBLIC_HYBRID' => GetMessage('MYMS_PARAM_INIT_MAP_TYPE_PUBLIC_HYBRID'),
            ),
            'DEFAULT' => 'MAP',
            'ADDITIONAL_VALUES' => 'N',
            'PARENT' => 'VISUAL',
        ),

        'MAP_DATA' => array(
            'NAME' => GetMessage('MYMS_PARAM_DATA'),
            'TYPE' => 'CUSTOM',
            'JS_FILE' => '/bitrix/components/bitrix/map.yandex.view/settings/settings.js',
            'JS_EVENT' => 'OnYandexMapSettingsEdit',
            'JS_DATA' => LANGUAGE_ID . '||' . GetMessage('MYMS_PARAM_DATA_SET') . '||' . GetMessage('MYMS_PARAM_DATA_NO_KEY') . '||' . GetMessage('MYMS_PARAM_DATA_GET_KEY') . '||' . GetMessage('MYMS_PARAM_DATA_GET_KEY_URL'),
            'DEFAULT' => serialize(array(
                'yandex_lat' => GetMessage('MYMS_PARAM_DATA_DEFAULT_LAT'),
                'yandex_lon' => GetMessage('MYMS_PARAM_DATA_DEFAULT_LON'),
                'yandex_scale' => 10
            )),
            'PARENT' => 'VISUAL',
        ),

        'MAP_WIDTH' => array(
            'NAME' => GetMessage('MYMS_PARAM_MAP_WIDTH'),
            'TYPE' => 'STRING',
            'DEFAULT' => '600',
            'PARENT' => 'VISUAL',
        ),

        'MAP_HEIGHT' => array(
            'NAME' => GetMessage('MYMS_PARAM_MAP_HEIGHT'),
            'TYPE' => 'STRING',
            'DEFAULT' => '500',
            'PARENT' => 'VISUAL',
        ),

        'CONTROLS' => array(
            'NAME' => GetMessage('MYMS_PARAM_CONTROLS'),
            'TYPE' => 'LIST',
            'MULTIPLE' => 'Y',
            'VALUES' => array(
                /*'TOOLBAR' => GetMessage('MYMS_PARAM_CONTROLS_TOOLBAR'),*/
                'ZOOM' => GetMessage('MYMS_PARAM_CONTROLS_ZOOM'),
                'SMALLZOOM' => GetMessage('MYMS_PARAM_CONTROLS_SMALLZOOM'),
                'MINIMAP' => GetMessage('MYMS_PARAM_CONTROLS_MINIMAP'),
                'TYPECONTROL' => GetMessage('MYMS_PARAM_CONTROLS_TYPECONTROL'),
                'SCALELINE' => GetMessage('MYMS_PARAM_CONTROLS_SCALELINE'),
                'SEARCH' => GetMessage('MYMS_PARAM_CONTROLS_SEARCH'),
            ),

            'DEFAULT' => array(/*'TOOLBAR', */
                'ZOOM', 'MINIMAP', 'TYPECONTROL', 'SCALELINE'),
            'PARENT' => 'VISUAL',
        ),

        'OPTIONS' => array(
            'NAME' => GetMessage('MYMS_PARAM_OPTIONS'),
            'TYPE' => 'LIST',
            'MULTIPLE' => 'Y',
            'VALUES' => array(
                'ENABLE_SCROLL_ZOOM' => GetMessage('MYMS_PARAM_OPTIONS_ENABLE_SCROLL_ZOOM'),
                'ENABLE_DBLCLICK_ZOOM' => GetMessage('MYMS_PARAM_OPTIONS_ENABLE_DBLCLICK_ZOOM'),
                'ENABLE_RIGHT_MAGNIFIER' => GetMessage('MYMS_PARAM_OPTIONS_ENABLE_RIGHT_MAGNIFIER'),
                'ENABLE_DRAGGING' => GetMessage('MYMS_PARAM_OPTIONS_ENABLE_DRAGGING'),
                /*'ENABLE_HOTKEYS' => GetMessage('MYMS_PARAM_OPTIONS_ENABLE_HOTKEYS'),*/
                /*'ENABLE_RULER' => GetMessage('MYMS_PARAM_OPTIONS_ENABLE_RULER'),*/
            ),


            'DEFAULT' => array('ENABLE_SCROLL_ZOOM', 'ENABLE_DBLCLICK_ZOOM', 'ENABLE_DRAGGING'),
            'PARENT' => 'VISUAL',
        ),

        'MAP_ID' => array(
            'NAME' => GetMessage('MYMS_PARAM_MAP_ID'),
            'TYPE' => 'STRING',
            'DEFAULT' => '',
            'PARENT' => 'VISUAL',
        ),
        "ICON_PLACEMARK" => array(
            'NAME' => GetMessage('ICON_PLACEMARK'),
            'TYPE' => 'STRING',
            'DEFAULT' => '',
            'PARENT' => 'VISUAL',
        ),
        'PROPERTY_COORDINATE' => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("PROPERTY_COORDINATE"),
            "TYPE" => "LIST",
            "VALUES" => array_merge($arProperty, array(
                "NAME" => GetMessage("NAME"),
                "PREVIEW_TEXT" => GetMessage("PREVIEW_TEXT"),
                "DETAIL_TEXT" => GetMessage("DETAIL_TEXT"),

            ))


        ),
        'DISPLAY_FIELDS_MAP' => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("DISPLAY_FIELDS"),
            "TYPE" => "LIST",
            "VALUES" => array(
                "NAME" => GetMessage("NAME"),
                "DATE_ACTIVE_FROM" => GetMessage("ACTIVE_FROM"),
                "DATE_ACTIVE_TO" => GetMessage("ACTIVE_TO"),
                "IBLOCK_SECTION" => GetMessage("IBLOCK_SECTION"),
                "PREVIEW_TEXT" => GetMessage("PREVIEW_TEXT"),
                "DETAIL_TEXT" => GetMessage("DETAIL_TEXT"),
                "PREVIEW_PICTURE" => GetMessage("PREVIEW_PICTURE"),
                "DETAIL_PICTURE" => GetMessage("DETAIL_PICTURE"),
            ),
            "MULTIPLE" => 'Y'


        ),
        'PROPERTY_CODE' => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("DISPLAY_PROPERTY"),
            "TYPE" => "LIST",
            "VALUES" => $arProperty,
            "MULTIPLE" => 'Y',
            "REFRESH" => 'Y',


        ),
        'LINKS_MAP' => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("LINKS_MAP"),
            "TYPE" => "LIST",
            "VALUES" => array(
                "a" => GetMessage("LINKS_MAP_a"),
                "b" => GetMessage("LINKS_MAP_b"),
                "ad" => GetMessage("LINKS_MAP_ad"),
                "ld" => GetMessage("LINKS_MAP_ld"),

            ),
            "MULTIPLE" => 'Y',

        ),
        'DISPLAY_PROPERTY_KEYS' => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("DISPLAY_PROPERTY_KEYS"),
            "TYPE" => "CHECKBOX",
        ),
        "IMG_MAX_WIDTH_MAP" => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("IMG_MAX_WIDTH_MAP"),
            "TYPE" => "STRING",
            "DEFAULT" => 100
        ),
        "IMG_MAX_HEIGHT_MAP" => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("IMG_MAX_HEIGHT_MAP"),
            "TYPE" => "STRING",
            "DEFAULT" => 80
        ),
        "NAMES_BALLOON" => array(
            'PARENT' => "VISUAL",
            "NAME" => GetMessage("NAMES_BALLOON"),
            "TYPE" => "CHECKBOX",
        ),


        "BASE_INFO" => array(
            "PARENT" => "PREVIEW_ELEMENTS",
            "NAME" => GetMessage('BASE_INFO'),
            "TYPE" => "CHECKBOX",
            "REFRESH" => "Y"
        ),


        "PRICE_CODE" => array(
            "PARENT" => "PRICES",
            "NAME" => GetMessage("IBLOCK_PRICE_CODE"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arPrice,
        ),
        "USE_PRICE_COUNT" => array(
            "PARENT" => "PRICES",
            "NAME" => GetMessage("IBLOCK_USE_PRICE_COUNT"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "SHOW_PRICE_COUNT" => array(
            "PARENT" => "PRICES",
            "NAME" => GetMessage("IBLOCK_SHOW_PRICE_COUNT"),
            "TYPE" => "STRING",
            "DEFAULT" => "1",
        ),
        "PRICE_VAT_INCLUDE" => array(
            "PARENT" => "PRICES",
            "NAME" => GetMessage("IBLOCK_VAT_INCLUDE"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "PRODUCT_PROPERTIES" => array(
            "PARENT" => "PRICES",
            "NAME" => GetMessage("CP_BCS_PRODUCT_PROPERTIES"),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arProperty_X,
        ),
        "USE_PRODUCT_QUANTITY" => array(
            "PARENT" => "PRICES",
            "NAME" => GetMessage("CP_BCS_USE_PRODUCT_QUANTITY"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "CACHE_TIME" => Array("DEFAULT" => 36000000),
        "CACHE_FILTER" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("IBLOCK_CACHE_FILTER"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "CACHE_GROUPS" => array(
            "PARENT" => "CACHE_SETTINGS",
            "NAME" => GetMessage("CP_BCS_CACHE_GROUPS"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
    ),
);

if ($arCurrentValues['PROPERTY_CODE'])
    $arComponentParameters['PARAMETERS']["DELIMETER_MULTI"] = array(
        "PARENT" => "VISUAL",
        "NAME" => GetMessage("DELIMETER_MULTI"),
        "TYPE" => "STRING",
        "DEFAULT" => ' - ',
    );

if (in_array('ad', $arCurrentValues['LINKS_MAP']))
    $arComponentParameters['PARAMETERS']["LINKS_MAP_TEXT"] = array(
        "PARENT" => "VISUAL",
        "NAME" => GetMessage("LINKS_MAP_TEXT"),
        "TYPE" => "STRING",
        "DEFAULT" => GetMessage("LINKS_MAP_TEXT_DEF"),
    );

if ($arCurrentValues['BASE_INFO'] == 'Y')                    //view content before map
{
    $arComponentParameters['PARAMETERS']["ELEMENT_SORT_FIELD"] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("IBLOCK_ELEMENT_SORT_FIELD"),
        "TYPE" => "LIST",
        "VALUES" => array(
            "shows" => GetMessage("IBLOCK_SORT_SHOWS"),
            "sort" => GetMessage("IBLOCK_SORT_SORT"),
            "timestamp_x" => GetMessage("IBLOCK_SORT_TIMESTAMP"),
            "name" => GetMessage("IBLOCK_SORT_NAME"),
            "id" => GetMessage("IBLOCK_SORT_ID"),
            "active_from" => GetMessage("IBLOCK_SORT_ACTIVE_FROM"),
            "active_to" => GetMessage("IBLOCK_SORT_ACTIVE_TO"),
        ),
        "ADDITIONAL_VALUES" => "Y",
        "DEFAULT" => "sort",
    );
    $arComponentParameters['PARAMETERS']["ELEMENT_SORT_ORDER"] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("IBLOCK_ELEMENT_SORT_ORDER"),
        "TYPE" => "LIST",
        "VALUES" => $arAscDesc,
        "DEFAULT" => "asc",
        "ADDITIONAL_VALUES" => "Y",
    );

    $arComponentParameters['PARAMETERS']["PROPERTY_CODE_BASE"] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("IBLOCK_PROPERTY"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $arProperty,
        "ADDITIONAL_VALUES" => "Y",
    );
    $arComponentParameters['PARAMETERS']["FIELD_CODE_BASE"] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("STUDIO7_ELEMS_ON_YMAPS_POLA_ELEMENTOV"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => array(
            "NAME" => GetMessage("NAME"),
            "DATE_ACTIVE_FROM" => GetMessage("ACTIVE_FROM"),
            "DATE_ACTIVE_TO" => GetMessage("ACTIVE_TO"),
            "IBLOCK_SECTION" => GetMessage("IBLOCK_SECTION"),
            "PREVIEW_TEXT" => GetMessage("PREVIEW_TEXT"),
            "DETAIL_TEXT" => GetMessage("DETAIL_TEXT"),
            "PREVIEW_PICTURE" => GetMessage("PREVIEW_PICTURE"),
            "DETAIL_PICTURE" => GetMessage("DETAIL_PICTURE"),

        ),
        "ADDITIONAL_VALUES" => "Y",
    );
    $arComponentParameters['PARAMETERS']["LINKS_BASE"] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage('LINKS_BASE'),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => array(
            "a" => GetMessage('LINKS_BASE_a'),
            "b" => GetMessage('LINKS_BASE_b'),
            "ad" => GetMessage('LINKS_BASE_ad'),
            "ld" => GetMessage('LINKS_BASE_ld'),


        ),

    );

    $arComponentParameters['PARAMETERS']["IMG_MAX_WIDTH_BASE"] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("IMG_MAX_WIDTH_BASE"),
        "TYPE" => "STRING",
        "DEFAULT" => 150
    );
    $arComponentParameters['PARAMETERS']["IMG_MAX_HEIGHT_BASE"] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("IMG_MAX_HEIGHT_BASE"),
        "TYPE" => "STRING",
        "DEFAULT" => 100
    );

    //ширина блоков, цвет фона, обводка, радиус закругления, цвет текста, цвет ссылок, тень блока
    $arComponentParameters['PARAMETERS']['ANONS_WIDTH'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANONS_WIDTH"),
        "TYPE" => "STRING",
        "DEFAULT" => '20%'
    );
    $arComponentParameters['PARAMETERS']['ANONS_MARGIN'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANONS_MARGIN"),
        "TYPE" => "STRING",
        "DEFAULT" => '5px 20px 10px 0px'
    );
    $arComponentParameters['PARAMETERS']['ANONS_PADDING'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANONS_PADDING"),
        "TYPE" => "STRING",
        "DEFAULT" => '10px 10px 10px 10px'
    );
    $arComponentParameters['PARAMETERS']['ANONS_ALIGN'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANONS_ALIGN"),
        "TYPE" => "LIST",
        "VALUES" => array(
            "left" => GetMessage('left'),
            "right" => GetMessage('right'),
            "center" => GetMessage('center'),
        ),
        "DEFAULT" => 'center'
    );
    $arComponentParameters['PARAMETERS']['ANONS_TEXT_ALIGN'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANONS_TEXT_ALIGN"),
        "TYPE" => "LIST",
        "VALUES" => array(
            "left" => GetMessage('left'),
            "right" => GetMessage('right'),
            "center" => GetMessage('center'),
        ),
        "DEFAULT" => 'left'
    );

    $arComponentParameters['PARAMETERS']['ANONS_BG_COLOR'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANONS_BG_COLOR"),
        "TYPE" => "STRING",
        "DEFAULT" => ''
    );

    $arComponentParameters['PARAMETERS']['ANONS_BORDER'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANONS_BORDER"),
        "TYPE" => "STRING",
        "DEFAULT" => ''
    );
    $arComponentParameters['PARAMETERS']['ANONS_RADIUS'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANONS_RADIUS"),
        "TYPE" => "STRING",
        "DEFAULT" => ''
    );
    $arComponentParameters['PARAMETERS']['ANONS_TEXT_COLOR'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANONS_TEXT_COLOR"),
        "TYPE" => "STRING",
        "DEFAULT" => ''
    );
    $arComponentParameters['PARAMETERS']['ANONS_LINK_COLOR'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANONS_LINK_COLOR"),
        "TYPE" => "STRING",
        "DEFAULT" => ''
    );
    $arComponentParameters['PARAMETERS']['ANONS_BOX_SHADOW'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANONS_BOX_SHADOW"),
        "TYPE" => "LIST",
        "VALUES" => array(
            'none' => GetMessage("STUDIO7_ELEMS_ON_YMAPS_BEZ_TENI"),
            '1px 1px 5px rgba(0,0,0,0.5)' => GetMessage('ANONS_BOX_SHADOW_SRB'),
            '2px 2px 10px rgba(0,0,0,0.5)' => GetMessage('ANONS_BOX_SHADOW_MRB'),
            '2px 3px 10px rgba(0,0,0,1)' => GetMessage('ANONS_BOX_SHADOW_LRB'),
            '-1px 1px 5px rgba(0,0,0,0.5)' => GetMessage('ANONS_BOX_SHADOW_SRT'),
            '-2px 2px 10px rgba(0,0,0,0.5)' => GetMessage('ANONS_BOX_SHADOW_MRT'),
            '-2px 3px 10px rgba(0,0,0,1)' => GetMessage('ANONS_BOX_SHADOW_LRT'),
        ),
        "ADDITIONAL_VALUES" => 'Y',
        "DEFAULT" => 'none'
    );
    $arComponentParameters['PARAMETERS']['ANIMATE_CLICK'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANIMATE_CLICK"),
        "TYPE" => "STRING",
        "DEFAULT" => ''
    );
    $arComponentParameters['PARAMETERS']['ANIMATE_BODY'] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("ANIMATE_BODY"),
        "TYPE" => "STRING",
        "DEFAULT" => ''
    );


    $arComponentParameters['PARAMETERS']["OFFERS_FIELD_CODE"] = CIBlockParameters::GetFieldCode(GetMessage("CP_BCS_OFFERS_FIELD_CODE"), "PREVIEW_ELEMENTS");
    $arComponentParameters['PARAMETERS']["OFFERS_PROPERTY_CODE"] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("CP_BCS_OFFERS_PROPERTY_CODE"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $arProperty_Offers,
        "ADDITIONAL_VALUES" => "Y",
    );
    $arComponentParameters['PARAMETERS']["OFFERS_SORT_FIELD"] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("CP_BCS_OFFERS_SORT_FIELD"),
        "TYPE" => "LIST",
        "VALUES" => array(
            "shows" => GetMessage("IBLOCK_FIELD_SHOW_COUNTER"),
            "sort" => GetMessage("IBLOCK_FIELD_SORT"),
            "timestamp_x" => GetMessage("IBLOCK_FIELD_TIMESTAMP_X"),
            "name" => GetMessage("IBLOCK_FIELD_NAME"),
            "id" => GetMessage("IBLOCK_FIELD_ID"),
            "active_from" => GetMessage("IBLOCK_FIELD_ACTIVE_FROM"),
            "active_to" => GetMessage("IBLOCK_FIELD_ACTIVE_TO"),
        ),
        "ADDITIONAL_VALUES" => "Y",
        "DEFAULT" => "sort",
    );
    $arComponentParameters['PARAMETERS']["OFFERS_SORT_ORDER"] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage("CP_BCS_OFFERS_SORT_ORDER"),
        "TYPE" => "LIST",
        "VALUES" => $arAscDesc,
        "DEFAULT" => "asc",
        "ADDITIONAL_VALUES" => "Y",
    );
    $arComponentParameters['PARAMETERS']["OFFERS_LIMIT"] = array(
        "PARENT" => "PREVIEW_ELEMENTS",
        "NAME" => GetMessage('CP_BCS_OFFERS_LIMIT'),
        "TYPE" => "STRING",
        "DEFAULT" => 5,
    );

}


if ($arCurrentValues['USE_BASKET'] == 'Y') {
    $arComponentParameters['PARAMETERS']["BASKET_URL"] = array(
        "PARENT" => "URL_TEMPLATES",
        "NAME" => GetMessage("IBLOCK_BASKET_URL"),
        "TYPE" => "STRING",
        "DEFAULT" => "/personal/basket.php",
    );
    $arComponentParameters['PARAMETERS']["ACTION_VARIABLE"] = array(
        "PARENT" => "URL_TEMPLATES",
        "NAME" => GetMessage("IBLOCK_ACTION_VARIABLE"),
        "TYPE" => "STRING",
        "DEFAULT" => "action",
    );
    $arComponentParameters['PARAMETERS']["PRODUCT_ID_VARIABLE"] = array(
        "PARENT" => "URL_TEMPLATES",
        "NAME" => GetMessage("IBLOCK_PRODUCT_ID_VARIABLE"),
        "TYPE" => "STRING",
        "DEFAULT" => "id",
    );
    $arComponentParameters['PARAMETERS']["PRODUCT_QUANTITY_VARIABLE"] = array(
        "PARENT" => "URL_TEMPLATES",
        "NAME" => GetMessage("CP_BCS_PRODUCT_QUANTITY_VARIABLE"),
        "TYPE" => "STRING",
        "DEFAULT" => "quantity",
    );
    $arComponentParameters['PARAMETERS']["PRODUCT_PROPS_VARIABLE"] = array(
        "PARENT" => "URL_TEMPLATES",
        "NAME" => GetMessage("CP_BCS_PRODUCT_PROPS_VARIABLE"),
        "TYPE" => "STRING",
        "DEFAULT" => "prop",
    );
    $arComponentParameters['PARAMETERS']["SECTION_ID_VARIABLE"] = array(
        "PARENT" => "URL_TEMPLATES",
        "NAME" => GetMessage("IBLOCK_SECTION_ID_VARIABLE"),
        "TYPE" => "STRING",
        "DEFAULT" => "SECTION_ID",
    );
}

if ($arCurrentValues['PAGE_ELEMENT_COUNT'])
    CIBlockParameters::AddPagerSettings($arComponentParameters, GetMessage("T_IBLOCK_DESC_PAGER_CATALOG"), true, true);

if (CModule::IncludeModule('catalog') && CModule::IncludeModule('currency')) {
    $arComponentParameters["PARAMETERS"]['CONVERT_CURRENCY'] = array(
        'PARENT' => 'PRICES',
        'NAME' => GetMessage('CP_BCS_CONVERT_CURRENCY'),
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'N',
        'REFRESH' => 'Y',
    );

    if (isset($arCurrentValues['CONVERT_CURRENCY']) && 'Y' == $arCurrentValues['CONVERT_CURRENCY']) {
        $arCurrencyList = array();
        $rsCurrencies = CCurrency::GetList(($by = 'SORT'), ($order = 'ASC'));
        while ($arCurrency = $rsCurrencies->Fetch()) {
            $arCurrencyList[$arCurrency['CURRENCY']] = $arCurrency['CURRENCY'];
        }
        $arComponentParameters['PARAMETERS']['CURRENCY_ID'] = array(
            'PARENT' => 'PRICES',
            'NAME' => GetMessage('CP_BCS_CURRENCY_ID'),
            'TYPE' => 'LIST',
            'VALUES' => $arCurrencyList,
            'DEFAULT' => CCurrency::GetBaseCurrency(),
        );
    }
}

if (!$OFFERS_IBLOCK_ID) {
    unset($arComponentParameters["PARAMETERS"]["OFFERS_FIELD_CODE"]);
    unset($arComponentParameters["PARAMETERS"]["OFFERS_PROPERTY_CODE"]);
    unset($arComponentParameters["PARAMETERS"]["OFFERS_SORT_FIELD"]);
    unset($arComponentParameters["PARAMETERS"]["OFFERS_SORT_ORDER"]);
} else {
    unset($arComponentParameters["PARAMETERS"]["PRODUCT_PROPERTIES"]);
    $arComponentParameters["PARAMETERS"]["OFFERS_CART_PROPERTIES"] = array(
        "PARENT" => "PRICES",
        "NAME" => GetMessage("CP_BCS_OFFERS_CART_PROPERTIES"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $arProperty_Offers,
    );
}
?>