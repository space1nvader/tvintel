<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require($_SERVER["DOCUMENT_ROOT"]."/helpers.php");

$APPLICATION->SetPageProperty("keywords_inner", "Пользователь");
$APPLICATION->SetPageProperty("title", "Пользователь");
$APPLICATION->SetPageProperty("keywords", "Пользователь");
$APPLICATION->SetPageProperty("description", "Пользователь");
$APPLICATION->SetTitle("Пользователь");


$connectionStatuses = [
    'Включена' => 1,
    'Приостановлена' => 2,
    'Отключена' => 3
];

//$tarifs = [
//
//];

global $USER;

    if ($USER->IsAuthorized()){

        $userid = $USER->GetID();


        $rsUser = CUser::GetByID($userid);
        $arUser = $rsUser->Fetch();
    
        $rsUser = CUser::GetByID($USER->GetParam('USER_ID'));
        $arUser = $rsUser->Fetch();

        $userInfoUrl = 'https://helpdesk.tvintel.info/nstaff/ext/user_info';
        
        $data = array(
            'uid' => $arUser["UF_UID"],
         );
        
        $info = curlRequest($userInfoUrl, ['uid' => $arUser["UF_UID"]]);
        
        if($info['internet']) {
            $payments = $info['internet'][0]['payment'];
            $address = $info['internet'][0]['address'];
            $tariff = $info['internet'][0]['tarif'];
            $speed = $info['internet'][0]['speed'];
            $abonplata = $info['internet'][0]['abonplata'];
        }
        
        
        if($info['ctv']){
            $payments = $info['ctv'][0]['payment'];
            $address = $info['ctv'][0]['address'];
            $tariff = $info['ctv'][0]['tarif'];
            $speed = $info['ctv'][0]['speed'];
            $abonplata = $info['ctv'][0]['abonplata'];
        }
?>
     <section class="acc" id="acc">
        <div class="acc--in">
          <h2 class="acc__ttl">Здравствуйте, <span class="acc__ttl__name"><?=$arUser["NAME"];?></span></h2>
          <div class="acc__flex">
            <div class="acc__col acc__col--1">
              <div class="acc__box acc__box--inf">
                <p class="acc__inf__num">№ договора: <span class="acc__inf__num__val"><?


                                echo $arUser["LOGIN"];
                                ?></span></p>
                <p class="acc__inf__name"><?=$arUser["NAME"]; ?></p>
                <p class="acc__inf__adr"><?=$address?></p>
              </div>

                <div class="acc__box acc__box--trf">
                    <ul class="acc__trf__its" <?=(!$info['internet']) ? 'style="padding-bottom: 20px;"': ''?>>
                        <li class="acc__trf__it">
                            <p class="acc__trf__label">Текущий тариф</p>
                            <p class="acc__trf__ttl"><?=$tariff?></p>
                        </li>
                        <?php if($info['internet']): ?>
                        <li class="acc__trf__it">
                            <p class="acc__trf__label">Скорость до</p>
                            <p class="acc__trf__speed"><span class="acc__trf__speed__val"><?=$speed?></span><span class="acc__trf__speed__unit">Мбит</span></p>
                        </li>
                        <?php endif; ?>
                        <li class="acc__trf__it">
                            <p class="acc__trf__label">Стоимость</p>
                            <p class="acc__trf__price"><span class="acc__trf__price__val"><?=$abonplata?></span><span class="acc__trf__price__unit"> <span class="icon-rub"></span>/мес.</span></p>
                        </li>
                    </ul>
                    <?php if($info['internet']): ?>
                    <div class="acc__trf__change">
                        <p class="acc__trf__change__bt"><span class="acc__trf__change__bt__txt">Сменить тариф</span><span class="acc__trf__change__bt__ic"></span></p>
                        <div class="acc__trf__change__cntw">
                            <div class="acc__trf__change__cnt">
                                <ul class="acc__trf__change__its">
                                    <li class="acc__trf__change__it acc__trf__change__it--head">
                                        <div class="acc__trf__change__it__flex">
                                            <p class="acc__trf__change__it__col acc__trf__change__it__col--1">Тариф</p>
                                            <p class="acc__trf__change__it__col acc__trf__change__it__col--2">Скорость</p>
                                            <p class="acc__trf__change__it__col acc__trf__change__it__col--3">Стоимость</p>
                                        </div>
                                    </li>
                                    <li class="acc__trf__change__it">
                                        <div class="acc__trf__change__it__flex">
                                            <p class="acc__trf__change__it__col acc__trf__change__it__col--1">Мир 30</p>
                                            <p class="acc__trf__change__it__col acc__trf__change__it__col--2">30 Мбит</p>
                                            <p class="acc__trf__change__it__col acc__trf__change__it__col--3">350 <span class="icon-rub"></span>/мес</p>
                                        </div>
                                        <p class="acc__trf__change__it__bt">Подключить</p>
<!--                                        <span class="prc__it__bt openModal" data-modal="bid" ttl="МИР-60" speed="до 60 Мбит/сек." price="450 руб/мес" tid="13">-->
<!--                                              Подключить-->
<!--                                          </span>-->
                                    </li>
                                    <li class="acc__trf__change__it">
                                        <div class="acc__trf__change__it__flex">
                                            <p class="acc__trf__change__it__col acc__trf__change__it__col--1">Мир 60</p>
                                            <p class="acc__trf__change__it__col acc__trf__change__it__col--2">60 Мбит</p>
                                            <p class="acc__trf__change__it__col acc__trf__change__it__col--3">550 <span class="icon-rub"></span>/мес</p>
                                        </div>
                                        <p class="acc__trf__change__it__bt">Подключить</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                
              <div class="acc__box acc__box--log">
                <p class="acc__log__ttl">История платежей</p>
                <ul class="acc__log__its">
                    <?php foreach($payments as $payment): ?>
                      <li class="acc__log__it">
                        <p class="acc__log__date"><?=date('d.m.Y h:i', strtotime($payment['pay_date']))?></p>
<!--                        <p class="acc__log__method"><span class="acc__log__method__ic acc__log__method__ic--1"></span></p>-->
                        <p class="acc__log__valw"><span class="acc__log__val"><?=$payment['amount'];?> </span><span class="icon-rub"></span></p>
                      </li>
                    <?php endforeach; ?>
                </ul>
              </div>
            </div>
            <div class="acc__col acc__col--2">
              <div class="acc__box acc__box--status">
                <p class="acc__status acc__status--<?=$connectionStatuses[$arUser["UF_STATUS"]]?>"><?= $arUser["UF_STATUS"]; ?></p>
<!--                <p class="acc__status acc__status--2">Приостановлено</p>-->
<!--                <p class="acc__status acc__status--3">Отключено</p>-->
                <p class="acc__blnc"><span class="acc__blnc__val"><?= $arUser["UF_MONEY"]; ?></span> <span class="icon-rub"></span></p>
                <div class="acc__btw"><span class="acc__bt acc__bt--pay">Пополнить баланс</span><span class="acc__bt acc__bt--promise">Обещанный платеж</span></div>
              </div>
            </div>
          </div>
        </div>
      </section>

    <?}else{?>
     <section class="acc" id="acc">
        <div class="acc--in">
<!--                        --><?//$APPLICATION->IncludeComponent("bitrix:system.auth.form","login",Array(
//                                "REGISTER_URL" => "register.php",
//                                "FORGOT_PASSWORD_URL" => "",
//                                "PROFILE_URL" => "profile.php",
//                                "SHOW_ERRORS" => "Y"
//
//                            )
//                        );?>
        </div>
      </section>
                    <?};
                    ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>