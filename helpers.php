<?php


function curlRequest($url, $dataParams)
{
    $payload = json_encode($dataParams);
    
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
    );
    
    $result = curl_exec($ch);
    
    curl_close($ch);
    
    return json_decode($result, true);
}

function printJsonAndReturn($data) {
    print json_encode($data);
    exit;
}
